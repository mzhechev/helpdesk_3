﻿using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Identity;
using Microsoft.AspNetCore.Authorization;
using HelpDesk_3.Application.Features.Identity.Commands.LoginUser;
using Microsoft.AspNetCore.Mvc;
using HelpDesk_3.Web.Common;
using HelpDesk_3.Application.Features.Identity.Commands.CreateUser;

namespace HelpDesk_3.Web.Features
{
    [ApiController]
    [Route("[controller]")]
    public class IdentityController : ApiController
    {
        [HttpPost]
        [Route(nameof(Register))]
        public async Task<ActionResult> Register(CreateUserCommand command)
            => await this.Mediator.Send(command).ToActionResult();

        [HttpPost]
        [Route(nameof(Login))]
        public async Task<ActionResult<LoginOutputModel>> Login(LoginUserCommand command)
            => await this.Mediator.Send(command).ToActionResult();
    }
}
