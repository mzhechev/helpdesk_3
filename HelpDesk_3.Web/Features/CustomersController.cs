﻿using HelpDesk_3.Application.Features.Customers.Command.Update;
using HelpDesk_3.Application.Features.Customers.Commands.Create;
using HelpDesk_3.Application.Features.Customers.Commands.Delete;
using HelpDesk_3.Application.Features.Customers.Queries.Search;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HelpDesk_3.Web.Features
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ApiController
    {
        [HttpGet]
        [Authorize]
        [Route(nameof(Search))]
        public async Task<ActionResult<CustomerOutputModel>> Search(
            [FromQuery] SearchCustomerQuery query)
            => await this.Send(query);

        [HttpPost]
        [Authorize]
        [Route(nameof(Create))]
        public async Task<ActionResult<CreateCustomerOutputModel>> Create(
            CreateCustomerCommand command)
            => await this.Send(command);

        [HttpPut]
        [Authorize]
        [Route(nameof(Update))]
        public async Task<ActionResult> Update(
            UpdateCustomerCommand command)
            => await this.Send(command);

        [HttpDelete]
        [Authorize]
        [Route(nameof(Delete))]
        public async Task<ActionResult> Delete(
            DeleteCustomerCommand command)
            => await this.Send(command);
    }
}
