﻿using HelpDesk_3.Application.Features.Requests.Commands.Complaint.Create;
using HelpDesk_3.Application.Features.Requests.Commands.Complaint.Delete;
using HelpDesk_3.Application.Features.Requests.Commands.Complaint.Update;
using HelpDesk_3.Application.Features.Requests.Queries.Search;
using HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HelpDesk_3.Web.Features.Requests
{
    [ApiController]
    [Route("[controller]")]
    public class ComplaintRequestsController : ApiController
    {
        [HttpGet]
        [Authorize]
        [Route(nameof(Search))]
        public async Task<ActionResult<RequestOutputModel>> Search(
            [FromQuery] SearchRequestQuery query)
            => await this.Send(query);

        [HttpPost]
        [Authorize]
        [Route(nameof(Create))]
        public async Task<ActionResult<CreateComplaintOutputModel>> Create(
            CreateComplaintCommand command)
            => await this.Send(command);

        [HttpPut]
        [Authorize]
        [Route(nameof(Update))]
        public async Task<ActionResult> Update(
            UpdateComplaintCommand command)
            => await this.Send(command);

        [HttpDelete]
        [Authorize]
        [Route(nameof(Delete))]
        public async Task<ActionResult> Delete(
            DeleteComplaintCommand command)
            => await this.Send(command);
    }
}
