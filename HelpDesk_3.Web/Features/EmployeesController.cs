﻿using HelpDesk_3.Application.Features.Employees.Commands.Delete;
using HelpDesk_3.Application.Features.Employees.Commands.Update;
using HelpDesk_3.Application.Features.Employees.Queries.Search;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HelpDesk_3.Web.Features
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : ApiController
    {
        [HttpGet]
        [Authorize]
        [Route(nameof(Search))]
        public async Task<ActionResult<EmployeeOutputModel>> Search(
            [FromQuery] SearchEmployeeQuery query)
            => await this.Send(query);

        [HttpPut]
        [Authorize]
        [Route(nameof(Update))]
        public async Task<ActionResult> Update(
            UpdateEmployeeCommand command)
            => await this.Send(command);

        [HttpDelete]
        [Authorize]
        [Route(nameof(Delete))]
        public async Task<ActionResult> Delete(
            DeleteEmployeeCommand command)
            => await this.Send(command);
    }
}
