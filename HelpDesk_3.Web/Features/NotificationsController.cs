﻿using HelpDesk_3.Application.Features.Notifications.Commands.Create;
using HelpDesk_3.Application.Features.Notifications.Commands.Delete;
using HelpDesk_3.Application.Features.Notifications.Queries.Search;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HelpDesk_3.Web.Features
{
    [ApiController]
    [Route("[controller]")]
    public class NotificationsController : ApiController
    {
        [HttpGet]
        [Authorize]
        [Route(nameof(Search))]
        public async Task<ActionResult<SearchEmailOutputModel>> Search(
            [FromQuery] SearchEmailQuery query)
                => await this.Send(query);

        [HttpPost]
        [Authorize]
        [Route(nameof(Create))]
        public async Task<ActionResult<CreateEmailOutputModel>> Create(
            CreateEmailCommand command)
                => await this.Send(command);

        [HttpDelete]
        [Authorize]
        [Route(nameof(Delete))]
        public async Task<ActionResult> Delete(
            DeleteEmailCommand command)
                => await this.Send(command);
    }
}
