﻿using HelpDesk_3.Application.Contracts;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace HelpDesk_3.Web.Services
{
    public class CurrentUserService : ICurrentUser
    {
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            var user = httpContextAccessor.HttpContext?.User;

            if (user == null)
            {
                throw new InvalidOperationException(
                    "This request does not have an authenticated user.");
            }

            var userIdentity = (user.Identity as ClaimsIdentity);

            if (userIdentity != null)
            {
                var roles = userIdentity.Claims
                    .Where(c => c.Type == ClaimTypes.Role)
                    .Select(c => c.Value)
                    .ToList();

                this.UserRoles = roles;
            }

            this.UserId = user.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public string UserId { get; }

        public List<string> UserRoles { get; } = default!;
    }
}
