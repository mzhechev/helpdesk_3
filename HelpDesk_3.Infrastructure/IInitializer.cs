﻿namespace HelpDesk_3.Infrastructure
{
    public interface IInitializer
    {
        void Initialize();
    }
}
