﻿using AutoMapper;
using HelpDesk_3.Application.Features.Notifications;
using HelpDesk_3.Application.Features.Notifications.Queries.Search;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Notifications;
using HelpDesk_3.Infrastructure.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;

namespace HelpDesk_3.Infrastructure.Persistance.Repositories
{
    internal class NotificationRepository : DataRepository<Email>, INotificationRepository
    {
        private readonly IMapper mapper;

        public NotificationRepository(HelpDesk_3DbContext db, IMapper mapper)
            : base(db)
            => this.mapper = mapper;

        public async Task<bool> Delete(Guid id, CancellationToken cancellationToken = default)
        {
            var email = await this.Data.Emails.FirstOrDefaultAsync(i => i.Id == id, cancellationToken);

            if (email == null)
            {
                return false;
            }

            this.Data.Emails.Remove(email);

            await this.Data.SaveChangesAsync(cancellationToken);

            return true;
        }

        public async Task<SearchEmailOutputModel> GetEmailById(Guid id, CancellationToken cancellationToken = default)
        {
            var query = await this.mapper
                .ProjectTo<SearchEmailOutputModel>(this
                    .Data
                    .Emails
                    .Where(e => e.Id == id))
                .FirstOrDefaultAsync(cancellationToken);

            if (query == null)
            {
                throw new InvalidEmailException("No email found with this Id.");
            }

            return query;
        }
    }
}
