﻿using AutoMapper;
using HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels;
using HelpDesk_3.Application.Features.Requests;
using HelpDesk_3.Domain.Models.Requests.Request.Complaint;
using HelpDesk_3.Infrastructure.Persistence.Repositories;
using HelpDesk_3.Domain.Models.Requests;
using Microsoft.EntityFrameworkCore;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels.Complaint;
using HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels.NewEmployee;
using HelpDesk_3.Application.Exceptions;
using HelpDesk_3.Domain.Models.Requests.RequestLogistic;
using HelpDesk_3.Domain.Models.Enums;

namespace HelpDesk_3.Infrastructure.Persistance.Repositories
{
    internal class RequestRepository : DataRepository<RequestData>, IRequestRepository
    {
        private readonly IMapper mapper;

        public RequestRepository(HelpDesk_3DbContext db, IMapper mapper) 
            : base(db)
        {
            this.mapper = mapper;
        }

        public async Task<RequestData> Find(Guid id, string category, CancellationToken cancellationToken)
        {
            RequestData? query;
            switch (category)
            {
                case "Complaint":
                    query = await this.Data.ComplaintRequests.FindAsync(id);
                    break;
                case "NewEmployee":
                    query = await this.Data.NewEmployeeRequests.FindAsync(id);
                    break;
                default:
                    query = default!;
                    break;
            }
            
            if (query == null)
            {
                throw new NotFoundException(category + "Request", id);
            }

            return query;
        }
        

        public async Task<RequestOutputModel> SearchById(Guid id, string category, CancellationToken cancellationToken)
        {
            RequestOutputModel? query;
            switch (category)
            {
                case "Complaint":
                    query = await this.mapper
                                .ProjectTo<ComplaintRequestOutputModel>(this
                                    .Data
                                    .ComplaintRequests
                                    .Where(e => e.Id == id))
                                .FirstOrDefaultAsync(cancellationToken);
                    break;
                case "NewEmployee":
                    query = await this.mapper
                                .ProjectTo<NewEmployeeRequestOutputModel>(this
                                    .Data
                                    .NewEmployeeRequests
                                    .Where(e => e.Id == id))
                                .FirstOrDefaultAsync(cancellationToken);
                    break;
                default:
                    query = default!;
                    break;
            }

            if (query == null)
            {
                throw new NotFoundException(category + "Request", id);
            }

            return query;
        }

        public async Task<Step> SearchForStep(string category, Status currentStatus, CancellationToken cancellationToken)
        {
            var step = await this
                  .Data
                  .Steps
                  .Where(s => s.Category == category)
                  .Where(s => s.CurrentStatus == currentStatus)
                  .FirstOrDefaultAsync(cancellationToken);

            if (step == null)
            {
                //maybe better explained?
                throw new NotFoundException("Step", currentStatus);
            }

            return step;
        }
    }
}
