﻿using AutoMapper;
using HelpDesk_3.Application.Features.Employees;
using HelpDesk_3.Application.Features.Employees.Queries.Search;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Employees;
using HelpDesk_3.Infrastructure.Persistance;
using Microsoft.EntityFrameworkCore;

namespace HelpDesk_3.Infrastructure.Persistence.Repositories
{
    internal class EmployeeRepository : DataRepository<Employee>, IEmployeeRepository
    {
        private readonly IMapper mapper;

        public EmployeeRepository(HelpDesk_3DbContext db, IMapper mapper)
            : base(db)
            => this.mapper = mapper;

        public async Task<Employee> GetEmployee(int id, CancellationToken cancellationToken = default)
        {
            var employee = await this
                .Data
                .Employees
                .FirstOrDefaultAsync(x => x.Id == id);

            if (employee == null)
            {
                throw new InvalidEmployeeException("No employee found with this Id.");
            }

            return employee;
        }

        public async Task<EmployeeOutputModel> GetEmployee(string? identificationNumber, CancellationToken cancellationToken = default)
        {
            var query = await this.mapper
                .ProjectTo<EmployeeOutputModel>(this
                    .Data
                    .Employees
                    .Where(e => e.PersonalInformation.IdentificationNumber == identificationNumber))
                .SingleOrDefaultAsync(cancellationToken);

            if (query == null)
            {
                throw new InvalidEmployeeException("No employee found with this Identification number.");
            }

            return query;
        }

        public async Task<EmployeeOutputModel> GetEmployee(int? id, CancellationToken cancellationToken = default)
        {
            var query = await this.mapper
                .ProjectTo<EmployeeOutputModel>(this
                    .Data
                    .Employees
                    .Where(e => e.Id == id))
                .SingleOrDefaultAsync(cancellationToken);

            if (query == null)
            {
                throw new InvalidEmployeeException("No employee found with this Id.");
            }

            return query;
        }

        public async Task<bool> Delete(int id, CancellationToken cancellationToken = default)
        {
            var employee = await this.Data.Employees.FindAsync(id);

            if (employee == null)
            {
                return false;
            }

            var user = await this.Data.Users
                .FirstOrDefaultAsync(i => i.Employee.Id == id);

            if (user != null)
            {
                this.Data.Users.Remove(user);
            }

            this.Data.Employees.Remove(employee);

            await this.Data.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
