﻿using AutoMapper;
using HelpDesk_3.Application.Features.Customers;
using HelpDesk_3.Application.Features.Customers.Queries.Search;
using HelpDesk_3.Application.Features.Requests;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Customers;
using HelpDesk_3.Infrastructure.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;

namespace HelpDesk_3.Infrastructure.Persistance.Repositories
{
    internal class CustomerRepository : DataRepository<Customer>, ICustomerRepository
    {
        private readonly IMapper mapper;
        private readonly IRequestRepository requestRepository;

        public CustomerRepository(
            HelpDesk_3DbContext db,
            IMapper mapper,
            IRequestRepository requestRepository)
            : base(db)
        {
            this.mapper = mapper;
            this.requestRepository = requestRepository;
        }

        public async Task<bool> Delete(Guid id, CancellationToken cancellationToken)
        {
            var customer = await this.Data.Customers.FindAsync(id);

            if (customer == null)
            {
                return false;
            }

            this.Data.Customers.Remove(customer);

            await this.Data.SaveChangesAsync(cancellationToken);

            return true;
        }

        public async Task<Customer> Find(Guid id, CancellationToken cancellationToken)
        {
            var customer = await this.Data.Customers.FindAsync(id);

            if (customer == null)
            {
                throw new InvalidCustomerException("No customer found with this Id.");
            }

            return customer;
        }

        public async Task<CustomerOutputModel> GetCustomerById(Guid? id, CancellationToken cancellationToken = default)
        {
            var customer = await this.mapper
                .ProjectTo<CustomerOutputModel>(this
                    .Data
                    .Customers
                    .Where(e => e.Id == id))
                .SingleOrDefaultAsync(cancellationToken);

            if (customer == null)
            {
                throw new InvalidCustomerException("No customer found with this Id.");
            }

            return customer;
        }

        public async Task<CustomerOutputModel> GetCustomerByIdentification(string? identificationNumber, CancellationToken cancellationToken = default)
        {
            var customer = await this.mapper
                .ProjectTo<CustomerOutputModel>(this
                    .Data
                    .Customers
                    .Where(e => e.PersonalInformation.IdentificationNumber == identificationNumber))
                .SingleOrDefaultAsync(cancellationToken);

            if (customer == null)
            {
                throw new InvalidCustomerException("No customer found with this Id.");
            }

            return customer;
        }

        public async Task<CustomerOutputModel> GetCustomerByRequestId(Guid? requestId, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}
