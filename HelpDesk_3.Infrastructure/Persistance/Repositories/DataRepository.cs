﻿using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Domain.Common;
using HelpDesk_3.Infrastructure.Persistance;

namespace HelpDesk_3.Infrastructure.Persistence.Repositories
{
    internal abstract class DataRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IAggregateRoot
    {
        protected DataRepository(HelpDesk_3DbContext db) => this.Data = db;

        protected HelpDesk_3DbContext Data { get; }

        protected IQueryable<TEntity> All() => this.Data.Set<TEntity>();

        public async Task Save(
            TEntity entity,
            CancellationToken cancellationToken = default)
        {
            this.Data.Update(entity);

            await this.Data.SaveChangesAsync(cancellationToken);
        }

    }
}
