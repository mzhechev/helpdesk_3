﻿using HelpDesk_3.Domain.Models.Customers;
using HelpDesk_3.Domain.Models.Employees;
using HelpDesk_3.Domain.Models.Notifications;
using HelpDesk_3.Domain.Models.Requests.Request.Complaint;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;
using HelpDesk_3.Domain.Models.Requests.RequestLogistic;
using HelpDesk_3.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace HelpDesk_3.Infrastructure.Persistance
{
    internal class HelpDesk_3DbContext : IdentityDbContext<User>
    {
        public HelpDesk_3DbContext(DbContextOptions<HelpDesk_3DbContext> options)
            : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; } = default!;

        public DbSet<Employee> Employees { get; set; } = default!;

        public DbSet<Email> Emails { get; set; } = default!;

        public DbSet<NewEmployeeRequest> NewEmployeeRequests { get; set; } = default!;

        public DbSet<ComplaintRequest> ComplaintRequests { get; set; } = default!;

        public DbSet<Comment> Comments { get; set; } = default!;

        public DbSet<Step> Steps { get; set; } = default!;

        public DbSet<Attachment> Attachments { get; set; } = default!;


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
