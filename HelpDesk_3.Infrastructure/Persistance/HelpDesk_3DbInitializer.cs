﻿using HelpDesk_3.Infrastructure.Persistance;
using Microsoft.EntityFrameworkCore;

namespace HelpDesk_3.Infrastructure.Persistence
{
    internal class HelpDesk_3DbInitializer : IInitializer
    {
        private readonly HelpDesk_3DbContext db;

        public HelpDesk_3DbInitializer(HelpDesk_3DbContext db) => this.db = db;

        public void Initialize() => this.db.Database.Migrate();
    }
}
