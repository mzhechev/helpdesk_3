﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HelpDesk_3.Infrastructure.Persistance.Migrations
{
    public partial class ComplaintRequestResponseDaysToInt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ResponseDays",
                table: "ComplaintRequests",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ResponseDays",
                table: "ComplaintRequests");
        }
    }
}
