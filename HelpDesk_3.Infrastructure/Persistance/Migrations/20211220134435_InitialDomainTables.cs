﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HelpDesk_3.Infrastructure.Persistance.Migrations
{
    public partial class InitialDomainTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PersonalInformation_FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_MiddleName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_IdentificationNumber = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Emails",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Reciever = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    TimeSent = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PersonalInformation_FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_MiddleName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_IdentificationNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AvailableLeave = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NewEmployeeRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RequestData_Category = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RequestData_CategoryGroup = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RequestData_Status = table.Column<int>(type: "int", nullable: false),
                    PersonalInformation_FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_MiddleName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_IdentificationNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContactInformation_PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContactInformation_Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EmployeeType = table.Column<int>(type: "int", nullable: false),
                    PlaceOfWork = table.Column<int>(type: "int", nullable: false),
                    ProbationPeriod = table.Column<int>(type: "int", nullable: false),
                    NoticePeriod = table.Column<int>(type: "int", nullable: false),
                    OrgAnnouncement = table.Column<bool>(type: "bit", nullable: false),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EmploymentHistory = table.Column<int>(type: "int", nullable: false),
                    Address_Country = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Address_City = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Address_Street = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Address_StreetNumber = table.Column<int>(type: "int", nullable: false),
                    Address_Appartment = table.Column<int>(type: "int", nullable: false),
                    Address_Unit = table.Column<int>(type: "int", nullable: false),
                    Address_Floor = table.Column<int>(type: "int", nullable: false),
                    StartDocuments_ReportingLevel = table.Column<int>(type: "int", nullable: false),
                    StartDocuments_ProbationPeriodValidation = table.Column<bool>(type: "bit", nullable: false),
                    StartDocuments_DeclarationGDPR = table.Column<bool>(type: "bit", nullable: false),
                    StartDocuments_LabourBook = table.Column<bool>(type: "bit", nullable: false),
                    StartDocuments_MedicalCertificate = table.Column<bool>(type: "bit", nullable: false),
                    StartDocuments_Diploma = table.Column<bool>(type: "bit", nullable: false),
                    StartDocuments_SignedJobOffer = table.Column<bool>(type: "bit", nullable: false),
                    StartDocuments_JobDescription = table.Column<bool>(type: "bit", nullable: false),
                    OrganizationData_Division = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OrganizationData_Department = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OrganizationData_Unit = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OrganizationData_Location = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OrganizationData_CostProfitArea = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OrganizationData_CostCenter = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OrganizationData_OfficialPosition = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OrganizationData_InternalPosition = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationData_DirectManagerAdminReporting = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    OrganizationData_DirectManagerFunctionalReporting = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationData_Bonus = table.Column<int>(type: "int", nullable: false),
                    OrganizationData_AdditionalComments = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    OrganizationData_Photo = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    OrganizationData_CiriumVitae = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    FraudCheck_CriminalRecord = table.Column<bool>(type: "bit", nullable: false),
                    FraudCheck_CentralCreditRecord = table.Column<int>(type: "int", nullable: false),
                    FraudCheck_BlackList = table.Column<bool>(type: "bit", nullable: false),
                    FraudCheck_ValidateInformation = table.Column<bool>(type: "bit", nullable: false),
                    FraudCheck_SmartInfo = table.Column<string>(type: "nvarchar(64)", maxLength: 64, nullable: false),
                    OpenSourcesCheck_Posts = table.Column<bool>(type: "bit", nullable: false),
                    OpenSourcesCheck_PictureSearch = table.Column<bool>(type: "bit", nullable: false),
                    OpenSourcesCheck_GoogleCheck = table.Column<bool>(type: "bit", nullable: false),
                    OpenSourcesCheck_HateGroups = table.Column<bool>(type: "bit", nullable: false),
                    OpenSourcesCheck_AccessCard = table.Column<bool>(type: "bit", nullable: false),
                    OpenSourcesCheck_InternalSecurity = table.Column<bool>(type: "bit", nullable: false),
                    InternalSecurityComment = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    EquipmentAndAccess_WorkingPlace = table.Column<int>(type: "int", nullable: false),
                    EquipmentAndAccess_StationaryPhone = table.Column<bool>(type: "bit", nullable: false),
                    EquipmentAndAccess_WorkingPlaceLocation = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EquipmentAndAccess_LeavingEmployee = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    EquipmentAndAccess_MobilePhone = table.Column<int>(type: "int", nullable: false),
                    EquipmentAndAccess_Uniform = table.Column<bool>(type: "bit", nullable: false),
                    EquipmentAndAccess_CompanyCar = table.Column<bool>(type: "bit", nullable: false),
                    EquipmentAndAccess_PrefferedEquipment = table.Column<int>(type: "int", nullable: false),
                    EquipmentAndAccess_VPN = table.Column<bool>(type: "bit", nullable: false),
                    EquipmentAndAccess_AdditionalEquipment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RequestAccessToSystems_OperationalRisk = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_SmartInfo = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_SmartInfoRoles = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RequestAccessToSystems_SCard = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_SCardRoles = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RequestAccessToSystems_FOS = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_FOSRoles = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RequestAccessToSystems_CeGate = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_ICollect = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_ICollectRoles = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RequestAccessToSystems_Syron = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_SyronRoles = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RequestAccessToSystems_AccessLikeUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    RequestAccessToSystems_VCS = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_VCSRoles = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RequestAccessToSystems_PhoneRecievingVCS = table.Column<int>(type: "int", nullable: false),
                    RequestAccessToSystems_CustomerView = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_AccessToOtherSystems = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    RequestAccessToSystems_AccessToEmailByMobile = table.Column<bool>(type: "bit", nullable: false),
                    RequestAccessToSystems_EmailGroups = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    RequestAccessToSystems_SharedFolders = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false),
                    AccessToSystemsData_ADUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_ADPass = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_VCSUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_VCSPass = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_SmartInfoUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_SmartInfoPass = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_SCardUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_SCardPass = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_FOSUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_FOSPass = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_SyronUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_SyronPass = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_ICollectUser = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_ICollectPass = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    AccessToSystemsData_UserEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    AccessToSystemsData_CustomerView = table.Column<bool>(type: "bit", nullable: false),
                    AccessToSystemsData_EquipmentOnFirstDay = table.Column<int>(type: "int", nullable: false),
                    AccessToSystemsData_AdditionalEquipmentApproved = table.Column<bool>(type: "bit", nullable: false),
                    AccessToSystemsData_AccessToSharedFolders = table.Column<bool>(type: "bit", nullable: false),
                    AccessToSystemsData_AddedToGroupEmails = table.Column<bool>(type: "bit", nullable: false),
                    AccessToSystemsData__UpdateAdditionalAccessesRequested = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    IBAN = table.Column<string>(type: "nvarchar(22)", maxLength: 22, nullable: false),
                    AllPaymentDocumentsRecieved = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_SignedJobDescription = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_SignedLabourContract = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_PCCCheck = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_PCCCheckDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StartDocumentsSigned_InstructionNoteHS = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_NRA = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_VideoSurveillance = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_GDPR = table.Column<bool>(type: "bit", nullable: false),
                    StartDocumentsSigned_GDPRPhotosVideo = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewEmployeeRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ComplaintRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RequestData_Category = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RequestData_CategoryGroup = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RequestData_Status = table.Column<int>(type: "int", nullable: false),
                    PersonalInformation_FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_MiddleName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PersonalInformation_IdentificationNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContractNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CustomerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ContactInformation_PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContactInformation_Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContractIssuingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ContractStatus = table.Column<int>(type: "int", nullable: false),
                    TypeOfLoan = table.Column<int>(type: "int", nullable: false),
                    Merchant = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    NotificationType = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Content = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false),
                    ComplaintDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ResponseDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ComplaintInformation_SourceOfComplaint = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ComplaintInformation_MethodOfReceivingComplaint = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ComplaintInformation_SubjectOfComplaint = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ComplaintInformation_DetailsOfComplaint = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CommunicationHistory = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    NumberOfClientContracts = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    NameOfClientContracts = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    ReturnToPreviousClaim = table.Column<bool>(type: "bit", nullable: false),
                    SolvedInFavor = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    PVCC = table.Column<bool>(type: "bit", nullable: false),
                    AmountPVCCRON = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    RegistrationNumber = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    ResponseDays = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComplaintRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComplaintRequests_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LeaveRequest",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EmployeeId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeaveRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeaveRequest_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintRequests_CustomerId",
                table: "ComplaintRequests",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_LeaveRequest_EmployeeId",
                table: "LeaveRequest",
                column: "EmployeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComplaintRequests");

            migrationBuilder.DropTable(
                name: "Emails");

            migrationBuilder.DropTable(
                name: "LeaveRequest");

            migrationBuilder.DropTable(
                name: "NewEmployeeRequests");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Employees");
        }
    }
}
