﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HelpDesk_3.Infrastructure.Persistance.Migrations
{
    public partial class StepDropRequestIdColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequestId",
                table: "Step");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RequestId",
                table: "Step",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
