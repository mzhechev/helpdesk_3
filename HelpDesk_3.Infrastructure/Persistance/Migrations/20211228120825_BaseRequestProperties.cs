﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HelpDesk_3.Infrastructure.Persistance.Migrations
{
    public partial class BaseRequestProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RequestData_Status",
                table: "NewEmployeeRequests",
                newName: "Status");

            migrationBuilder.RenameColumn(
                name: "RequestData_CategoryGroup",
                table: "NewEmployeeRequests",
                newName: "CategoryGroup");

            migrationBuilder.RenameColumn(
                name: "RequestData_Category",
                table: "NewEmployeeRequests",
                newName: "Category");

            migrationBuilder.RenameColumn(
                name: "RequestData_Status",
                table: "ComplaintRequests",
                newName: "Status");

            migrationBuilder.RenameColumn(
                name: "RequestData_CategoryGroup",
                table: "ComplaintRequests",
                newName: "CategoryGroup");

            migrationBuilder.RenameColumn(
                name: "RequestData_Category",
                table: "ComplaintRequests",
                newName: "Category");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryGroup",
                table: "NewEmployeeRequests",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "NewEmployeeRequests",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryGroup",
                table: "ComplaintRequests",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "ComplaintRequests",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Status",
                table: "NewEmployeeRequests",
                newName: "RequestData_Status");

            migrationBuilder.RenameColumn(
                name: "CategoryGroup",
                table: "NewEmployeeRequests",
                newName: "RequestData_CategoryGroup");

            migrationBuilder.RenameColumn(
                name: "Category",
                table: "NewEmployeeRequests",
                newName: "RequestData_Category");

            migrationBuilder.RenameColumn(
                name: "Status",
                table: "ComplaintRequests",
                newName: "RequestData_Status");

            migrationBuilder.RenameColumn(
                name: "CategoryGroup",
                table: "ComplaintRequests",
                newName: "RequestData_CategoryGroup");

            migrationBuilder.RenameColumn(
                name: "Category",
                table: "ComplaintRequests",
                newName: "RequestData_Category");

            migrationBuilder.AlterColumn<string>(
                name: "RequestData_CategoryGroup",
                table: "NewEmployeeRequests",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "RequestData_Category",
                table: "NewEmployeeRequests",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "RequestData_CategoryGroup",
                table: "ComplaintRequests",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "RequestData_Category",
                table: "ComplaintRequests",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);
        }
    }
}
