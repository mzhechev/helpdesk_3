﻿using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class NewEmployeeRequestConfiguration : IEntityTypeConfiguration<NewEmployeeRequest>
    {
        public void Configure(EntityTypeBuilder<NewEmployeeRequest> builder)
        {
            builder
                .HasKey(ner => ner.Id);

            builder
                .Property(ner => ner.Category)
                .HasMaxLength(50);

            builder
                .Property(ner => ner.CategoryGroup)
                .HasMaxLength(50);

            builder
                .Property(ner => ner.Status);

            builder
                .OwnsOne(ner => ner.PersonalInformation, pi =>
                {
                    pi.WithOwner();

                    pi.Property(pi => pi.FirstName);
                    pi.Property(pi => pi.MiddleName);
                    pi.Property(pi => pi.LastName);
                    pi.Property(pi => pi.IdentificationNumber);
                });

            builder
                .OwnsOne(ner => ner.ContactInformation, ci =>
                {
                    ci.WithOwner();

                    ci.Property(ci => ci.PhoneNumber);
                    ci.Property(ci => ci.Email);
                });

            builder.Property(ner => ner.EmployeeType);

            builder.Property(ner => ner.PlaceOfWork);

            builder.Property(ner => ner.ProbationPeriod);

            builder.Property(ner => ner.NoticePeriod);

            builder.Property(ner => ner.OrgAnnouncement);

            builder.Property(ner => ner.Gender);

            builder.Property(ner => ner.DateOfBirth);

            builder.Property(ner => ner.EmploymentHistory);

            builder
                .OwnsOne(ner => ner.Address, ad =>
                {
                    ad.WithOwner();

                    ad
                        .Property(ad => ad.Country)
                        .HasMaxLength(128);

                    ad
                        .Property(ad => ad.City)
                        .HasMaxLength(128);

                    ad
                        .Property(ad => ad.Street)
                        .HasMaxLength(128);

                    ad.Property(ad => ad.StreetNumber);
                    ad.Property(ad => ad.Appartment);
                    ad.Property(ad => ad.Unit);
                    ad.Property(ad => ad.Floor);
                });


            builder
                .OwnsOne(ner => ner.StartDocuments, sd =>
                {
                    sd.WithOwner();

                    sd.Property(sd => sd.ReportingLevel);
                    sd.Property(sd => sd.ProbationPeriodValidation);
                    sd.Property(sd => sd.DeclarationGDPR);
                    sd.Property(sd => sd.LabourBook);
                    sd.Property(sd => sd.MedicalCertificate);
                    sd.Property(sd => sd.Diploma);
                    sd.Property(sd => sd.SignedJobOffer);
                    sd.Property(sd => sd.JobDescription);
                });

            builder
                .OwnsOne(ner => ner.OrganizationData, od =>
                {
                    od.WithOwner();

                    od
                        .Property(od => od.Division)
                        .HasMaxLength(64);
                    od
                        .Property(od => od.Department)
                        .HasMaxLength(64);
                    od
                        .Property(od => od.Unit)
                        .HasMaxLength(64);
                    od
                        .Property(od => od.Location)
                        .HasMaxLength(64);
                    od
                        .Property(od => od.CostProfitArea)
                        .HasMaxLength(64);
                    od
                        .Property(od => od.CostCenter)
                        .HasMaxLength(64);
                    od
                        .Property(od => od.OfficialPosition)
                        .HasMaxLength(64);
                    od
                        .Property(od => od.DirectManagerAdminReporting)
                        .HasMaxLength(128);
                    od
                        .Property(od => od.DirectManagerAdminReporting)
                        .HasMaxLength(128);
                    od.Property(od => od.Bonus);
                    od
                        .Property(od => od.AdditionalComments)
                        .HasMaxLength(512);
                    od
                        .Property(od => od.Photo)
                        .HasMaxLength(512);
                    od
                        .Property(od => od.CiriumVitae)
                        .HasMaxLength(512);
                });

            builder
                .OwnsOne(ner => ner.FraudCheck, fc =>
                {
                    fc.WithOwner();

                    fc.Property(fc => fc.CriminalRecord);
                    fc.Property(fc => fc.CentralCreditRecord);
                    fc.Property(fc => fc.BlackList);
                    fc.Property(fc => fc.ValidateInformation);
                    fc
                        .Property(fc => fc.SmartInfo)
                        .HasMaxLength(64);
                });

            builder
                .OwnsOne(ner => ner.OpenSourcesCheck, osc =>
                {
                    osc.WithOwner();

                    osc.Property(osc => osc.Posts);
                    osc.Property(osc => osc.PictureSearch);
                    osc.Property(osc => osc.GoogleCheck);
                    osc.Property(osc => osc.HateGroups);
                    osc.Property(osc => osc.AccessCard);
                    osc.Property(osc => osc.InternalSecurity);
                });

            builder
                .Property(ner => ner.InternalSecurityComment)
                .HasMaxLength(256);

            builder
                .OwnsOne(ner => ner.EquipmentAndAccess, ea =>
                {
                    ea.WithOwner();

                    ea.Property(ea => ea.MobilePhone);
                    ea.Property(ea => ea.Uniform);
                    ea.Property(ea => ea.CompanyCar);
                    ea.Property(ea => ea.PrefferedEquipment);
                    ea.Property(ea => ea.VPN);
                    ea.Property(ea => ea.AdditionalEquipment);
                    ea.Property(ea => ea.WorkingPlace);
                    ea.Property(ea => ea.StationaryPhone);
                    ea.Property(ea => ea.WorkingPlaceLocation);
                    ea.Property(ea => ea.LeavingEmployee);
                });

            builder
                .OwnsOne(ner => ner.RequestAccessToSystems, ras =>
                {
                    ras.WithOwner();

                    ras.Property(ras => ras.OperationalRisk);
                    ras.Property(ras => ras.SmartInfo);
                    ras
                        .Property(ras => ras.SmartInfoRoles)
                        .HasMaxLength(256);

                    ras.Property(ras => ras.SCard);
                    ras
                        .Property(ras => ras.SCardRoles)
                        .HasMaxLength(256);
                    ras.Property(ras => ras.FOS);
                    ras
                        .Property(ras => ras.FOSRoles)
                        .HasMaxLength(256);
                    ras.Property(ras => ras.CeGate);
                    ras.Property(ras => ras.ICollect);
                    ras
                        .Property(ras => ras.ICollectRoles)
                        .HasMaxLength(256);
                    ras.Property(ras => ras.Syron);
                    ras
                        .Property(ras => ras.SyronRoles)
                        .HasMaxLength(256);
                    ras
                        .Property(ras => ras.AccessLikeUser)
                        .HasMaxLength(128);
                    ras.Property(ras => ras.VCS);
                    ras
                        .Property(ras => ras.VCSRoles)
                        .HasMaxLength(256);
                    ras.Property(ras => ras.PhoneRecievingVCS);
                    ras.Property(ras => ras.CustomerView);
                    ras
                        .Property(ras => ras.AccessToOtherSystems)
                        .HasMaxLength(256);
                    ras.Property(ras => ras.AccessToEmailByMobile);
                    ras
                        .Property(ras => ras.EmailGroups)
                        .HasMaxLength(512);
                    ras
                        .Property(ras => ras.SharedFolders)
                        .HasMaxLength(1024);
                });

            builder
                .OwnsOne(ner => ner.AccessToSystemsData, asd =>
                {
                    asd.WithOwner();

                    asd
                        .Property(asd => asd.ADUser)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.ADPass)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.VCSUser)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.VCSPass)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.SmartInfoUser)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.SmartInfoPass)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.SCardUser)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.SCardPass)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.FOSUser)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.FOSPass)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.SyronUser)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.SyronPass)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.ICollectUser)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.ICollectPass)
                        .HasMaxLength(128);
                    asd
                        .Property(asd => asd.UserEmail)
                        .HasMaxLength(256);
                    asd.Property(asd => asd.EquipmentOnFirstDay);
                    asd.Property(asd => asd.AdditionalEquipmentApproved);
                    asd.Property(asd => asd.AddedToGroupEmails);

                    asd
                        .Property(asd => asd._UpdateAdditionalAccessesRequested)
                        .HasMaxLength(256);
                });

            builder
                .Property(ner => ner.IBAN)
                .HasMaxLength(22);

            builder.Property(ner => ner.AllPaymentDocumentsRecieved);

            builder
                .OwnsOne(ner => ner.StartDocumentsSigned, sds =>
                {
                    sds.WithOwner();

                    sds.Property(sds => sds.SignedJobDescription);
                    sds.Property(sds => sds.SignedLabourContract);
                    sds.Property(sds => sds.PCCCheck);
                    sds.Property(sds => sds.PCCCheckDate);
                    sds.Property(sds => sds.InstructionNoteHS);
                    sds.Property(sds => sds.NRA);
                    sds.Property(sds => sds.VideoSurveillance);
                    sds.Property(sds => sds.GDPR);
                    sds.Property(sds => sds.GDPRPhotosVideo);
                });
        }
    }
}
