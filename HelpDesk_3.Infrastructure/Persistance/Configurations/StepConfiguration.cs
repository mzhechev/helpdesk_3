﻿using HelpDesk_3.Domain.Models.Requests.RequestLogistic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class StepConfiguration : IEntityTypeConfiguration<Step>
    {
        public void Configure(EntityTypeBuilder<Step> builder)
        {
            builder
                .HasKey(s => s.Id);

            builder
                .Property(s => s.CurrentRole);

            builder
                .Property(s => s.NextRole);

            builder
                .Property(s => s.CanStart);

            builder
                .Property(s => s.CanFinish);

            builder
                .Property(s => s.CanEdit);

            builder
                .Property(s => s.CurrentStatus);

            builder
                .Property(s => s.NextStatus);

            builder
                .Property(s => s.Button);

            builder
                .Property(s => s.ButtonVisible);
        }
    }
}
