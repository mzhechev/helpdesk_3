﻿using HelpDesk_3.Domain.Models.Requests.RequestLogistic;
using HelpDesk_3.Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasKey(c => c.Id);

            builder
                .Property(c => c.RequestId);

            builder
                .Property(c => c.Category);

            builder
                .HasOne(c => c.CreatedBy)
                .WithOne()
                .HasForeignKey<Comment>("EmployeeId")
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .Property(c => c.Content);

            builder
                .Property(c => c.Status);

            builder
                .HasOne(c => c.Attachment)
                .WithOne()
                .HasForeignKey<Attachment>("AttachmentId")
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
