﻿using HelpDesk_3.Domain.Models.Employees;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.AvailableLeave);

            builder
                .OwnsOne(c => c.PersonalInformation, p =>
                {
                    p.WithOwner();

                    p.Property(op => op.FirstName);
                    p.Property(op => op.MiddleName);
                    p.Property(op => op.LastName);
                    p.Property(op => op.IdentificationNumber);
                });

            builder
                .HasMany(lr => lr.LeaveRequests)
                .WithOne().Metadata.PrincipalToDependent
                .SetField("leaveRequests");

            //builder
            //    .OwnsMany(e => e.LeaveRequests, lr =>
            //    {
            //        lr.WithOwner();

            //        lr
            //            .Property(lr => lr.StartDate)
            //            .IsRequired();
            //        lr
            //            .Property(lr => lr.EndDate)
            //            .IsRequired();
            //    });
        }
    }
}
