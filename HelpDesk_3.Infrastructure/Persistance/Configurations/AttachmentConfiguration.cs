﻿using HelpDesk_3.Domain.Models.Requests.RequestLogistic;
using HelpDesk_3.Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class AttachmentConfiguration : IEntityTypeConfiguration<Attachment>
    {
        public void Configure(EntityTypeBuilder<Attachment> builder)
        {
            builder
                .HasKey(a => a.Id);

            builder
                .Property(a => a.Path);

            builder
                .Property(a => a.CreatedOn);

            builder
                .HasOne(a => a.CreatedBy)
                .WithOne()
                .HasForeignKey<Attachment>("EmployeeId")
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .Property(a => a.EditedOn);
        }
    }
}
