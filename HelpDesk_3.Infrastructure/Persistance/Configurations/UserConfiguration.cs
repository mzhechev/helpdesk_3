﻿using HelpDesk_3.Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasOne(u => u.Employee)
                .WithOne()
                .HasForeignKey<User>("EmployeeId")
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
