﻿using HelpDesk_3.Domain.Models.Customers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder
                .HasKey(c => c.Id);

            builder
                .OwnsOne(c => c.PersonalInformation, p =>
                {
                    p.WithOwner();

                    p.Property(op => op.FirstName);
                    p.Property(op => op.MiddleName);
                    p.Property(op => op.LastName);
                    p.Property(op => op.IdentificationNumber);
                });
        }
    }
}
