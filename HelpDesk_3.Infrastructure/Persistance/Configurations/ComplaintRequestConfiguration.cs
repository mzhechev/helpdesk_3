﻿using HelpDesk_3.Domain.Models.Requests.Request.Complaint;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class ComplaintRequestConfiguration : IEntityTypeConfiguration<ComplaintRequest>
    {
        public void Configure(EntityTypeBuilder<ComplaintRequest> builder)
        {
            builder
                .HasKey(cr => cr.Id);

            builder
                .Property(cr => cr.Category)
                .HasMaxLength(50);

            builder
                .Property(cr => cr.CategoryGroup)
                .HasMaxLength(50);

            builder
                .Property(cr => cr.Status);

            builder
                .OwnsOne(cr => cr.PersonalInformation, pi =>
                {
                    pi.WithOwner();

                    pi.Property(pi => pi.FirstName);
                    pi.Property(pi => pi.MiddleName);
                    pi.Property(pi => pi.LastName);
                    pi.Property(pi => pi.IdentificationNumber);
                });

            builder
                .Property(cr => cr.ContractNumber);

            builder
                .HasOne(cr => cr.Customer)
                .WithMany()
                .HasForeignKey("CustomerId")
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .OwnsOne(cr => cr.ContactInformation, ci =>
                {
                    ci.WithOwner();

                    ci.Property(ci => ci.PhoneNumber);
                    ci.Property(ci => ci.Email);
                });

            builder.Property(cr => cr.ContractIssuingDate);

            builder.Property(cr => cr.ContractStatus);

            builder.Property(cr => cr.TypeOfLoan);

            builder
                .Property(cr => cr.Merchant)
                .HasMaxLength(128);

            builder
                .Property(cr => cr.NotificationType)
                .HasMaxLength(128);

            builder
                .Property(cr => cr.Content)
                .HasMaxLength(512);

            builder.Property(cr => cr.ComplaintDate);

            builder.Property(cr => cr.ResponseDate);

            builder
                .OwnsOne(cr => cr.ComplaintInformation, ci =>
                {
                    ci.WithOwner();

                    ci.Property(ci => ci.SourceOfComplaint);
                    ci.Property(ci => ci.MethodOfReceivingComplaint);
                    ci.Property(ci => ci.SubjectOfComplaint);
                    ci.Property(ci => ci.DetailsOfComplaint);
                });

            builder
                .Property(cr => cr.CommunicationHistory)
                .HasMaxLength(128);

            builder
                .Property(cr => cr.NumberOfClientContracts)
                .HasMaxLength(128);

            builder
                .Property(cr => cr.NameOfClientContracts)
                .HasMaxLength(128);

            builder.Property(cr => cr.ReturnToPreviousClaim);

            builder
                .Property(cr => cr.SolvedInFavor)
                .HasMaxLength(128);

            builder.Property(cr => cr.PVCC);

            builder
                .Property(cr => cr.AmountPVCCRON)
                .HasMaxLength(128);

            builder
                .Property(cr => cr.RegistrationNumber)
                .HasMaxLength(128);

            builder.Property(cr => cr.ResponseDays);

            builder
                .Property(cr => cr.Remarks)
                .HasMaxLength(512);
        }
    }
}
