﻿using HelpDesk_3.Domain.Models.Notifications;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HelpDesk_3.Infrastructure.Persistance.Configurations
{
    internal class NotificationConfiguration : IEntityTypeConfiguration<Email>
    {
        public void Configure(EntityTypeBuilder<Email> builder)
        {
            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder
                .Property(e => e.Reciever)
                .IsRequired()
                .HasMaxLength(100);

            builder
                .Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(512);

            builder
                .Property(e => e.TimeSent)
                .IsRequired();
        }
    }
}
