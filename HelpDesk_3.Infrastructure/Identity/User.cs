﻿using HelpDesk_3.Application.Features.Identity;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Employees;
using Microsoft.AspNetCore.Identity;

namespace HelpDesk_3.Infrastructure.Identity
{
    public class User : IdentityUser, IUser
    {
        internal User(string username)
            : base(username)
            => this.UserName = username;

        private User()
        {
        }

        public Employee? Employee { get; private set; }

        public void BecomeEmployee(Employee employee)
        {
            if (this.Employee != null)
            {
                throw new InvalidEmployeeException($"User '{this.UserName}' is already an employee.");
            }

            this.Employee = employee;
        }
    }
}
