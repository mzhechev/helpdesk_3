﻿namespace HelpDesk_3.Infrastructure.Identity
{
    public interface IJwtTokenGenerator
    {
        string GenerateToken(User user, IList<string> roles);
    }
}
