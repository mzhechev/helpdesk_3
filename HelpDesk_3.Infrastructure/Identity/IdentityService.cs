﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Identity;
using HelpDesk_3.Application.Features.Identity.Commands.LoginUser;
using Microsoft.AspNetCore.Identity;

namespace HelpDesk_3.Infrastructure.Identity
{
    internal class IdentityService : IIdentity
    {
        private const string InvalidLoginErrorMessage = "Invalid credentials.";

        private readonly UserManager<User> userManager;
        private readonly IJwtTokenGenerator jwtTokenGenerator;

        public IdentityService(UserManager<User> userManager, IJwtTokenGenerator jwtTokenGenerator)
        {
            this.userManager = userManager;
            this.jwtTokenGenerator = jwtTokenGenerator;
        }

        public async Task<Result<IUser>> Register(UserInputModel userInput)
        {
            var user = new User(userInput.Email);

            var identityResult = await this.userManager.CreateAsync(user, userInput.Password);

            var errors = identityResult.Errors.Select(e => e.Description);

            return identityResult.Succeeded
                ? Result<IUser>.SuccessWith(user)
                : Result<IUser>.Failure(errors);
        }

        public async Task<Result<LoginOutputModel>> Login(UserInputModel userInput)
        {
            var user = await this.userManager.FindByNameAsync(userInput.Email);//FindByEmailAsync(userInput.Email);
            if (user == null)
            {
                return InvalidLoginErrorMessage;
            }

            var roles = await this.userManager.GetRolesAsync(user);

            var passwordValid = await this.userManager.CheckPasswordAsync(user, userInput.Password);
            if (!passwordValid)
            {
                return InvalidLoginErrorMessage;
            }

            var token = this.jwtTokenGenerator.GenerateToken(user, roles);

            return new LoginOutputModel(token);
        }
    }
}
