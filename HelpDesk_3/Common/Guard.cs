﻿namespace HelpDesk_3.Domain.Common
{
    using System;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using Exceptions;
    using Models;

    public static class Guard
    {
        public static void AgainstEmptyString<TException>(string value, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (!string.IsNullOrEmpty(value))
            {
                return;
            }

            ThrowException<TException>($"{name} cannot be null ot empty.");
        }

        public static void AgainstNegativeNumber<TException>(decimal value, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (value >= 0)
            {
                return;
            }

            ThrowException<TException>($"{name} cannot be a negative value.");
        }

        public static void ForStringLength<TException>(string value, int minLength, int maxLength, string name = "Value")
            where TException : BaseDomainException, new()
        {
            //TODO JUST SO IT WORKS :)
            //AgainstEmptyString<TException>(value, name);

            if (minLength <= value.Length && value.Length <= maxLength)
            {
                return;
            }

            ThrowException<TException>($"{name} must have between {minLength} and {maxLength} symbols.");
        }

        public static void AgainstOutOfRange<TException>(int number, int min, int max, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (min <= number && number <= max)
            {
                return;
            }

            ThrowException<TException>($"{name} must be between {min} and {max}.");
        }

        public static void AgainstOutOfRange<TException>(decimal number, decimal min, decimal max, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (min <= number && number <= max)
            {
                return;
            }

            ThrowException<TException>($"{name} must be between {min} and {max}.");
        }

        public static void ForValidUrl<TException>(string url, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (url.Length <= ModelConstants.Common.MaxUrlLength && 
                Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                return;
            }

            ThrowException<TException>($"{name} must be a valid URL.");
        }

        public static void Against<TException>(object actualValue, object unexpectedValue, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (!actualValue.Equals(unexpectedValue))
            {
                return;
            }

            ThrowException<TException>($"{name} must not be {unexpectedValue}.");
        }

        public static void AgainstNull<TException>(object actualValue, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (actualValue != null)
            {
                return;
            }

            ThrowException<TException>($"{name} must not be null.");
        }

        public static void ForFutureDate<TException>(DateTime date, string name = "Value")
            where TException : BaseDomainException, new()
        {
            if (date.Date >= DateTime.Now.Date)
            {
                return;
            }

            ThrowException<TException>($"{name} must be after {DateTime.Now}.");
        }

        public static void ForExactStringLength<TException>(string value, int length, string name = "Value")
            where TException : BaseDomainException, new()
        {
            AgainstEmptyString<TException>(value, name);
            
            if (value.Length == length)
            {
                return;
            }

            ThrowException<TException>($"{name} must be exactly {length} characters long.");
        }

        public static void ForCorrectEmail<TException>(string value, string name = "Value")
            where TException : BaseDomainException, new()
        {
            AgainstEmptyString<TException>(value, name);

            try
            {
                MailAddress m = new(value);

                return;
            }
            catch (FormatException)
            {
                ThrowException<TException>($"{name} is not a valid email.");
            }
        }

        public static void ForCorrectPhone<TException>(string value, string name = "Value")
            where TException : BaseDomainException, new()
        {
            AgainstEmptyString<TException>(value, name);

            if (Regex.Match(value, @"^(\+[0-9]{12})$").Success ||
                Regex.Match(value, @"^([0-9]{10})$").Success)
            {
                return;
            }

            ThrowException<TException>($"{name} is not a valid phone.");
        }

        public static void ForCorrectUri<TException>(string value, string name = "Value")
            where TException : BaseDomainException, new()
        {
            AgainstEmptyString<TException>(value, name);

            if (Uri.IsWellFormedUriString(value, UriKind.Absolute))
            {
                return;
            }

            ThrowException<TException>($"{name} is not a valid Uri.");
        }

        public static void ForCorrectPassword<TException>(string value)
            where TException : BaseDomainException, new()
        {
            AgainstEmptyString<TException>(value);

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinChars = new Regex(@".{6}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(value))
            {
                ThrowException<TException>("Password should contain at least one lower case letter.");
            } 
            else if (!hasUpperChar.IsMatch(value))
            {
                ThrowException<TException>("Password should contain at least one upper case letter.");
            }
            else if (!hasMinChars.IsMatch(value))
            {
                ThrowException<TException>("Password should be at least 6 letters long.");
            }
            else if (!hasNumber.IsMatch(value))
            {
                ThrowException<TException>("Password should contain at least one numeric value.");
            }
            else if (!hasSymbols.IsMatch(value))
            {
                ThrowException<TException>("Password should contain at least one special case character.");
            }
            else
            {
                return;
            }
        }

        public static void ForCorrectEGN<TException>(string value, string name = "Value")
            where TException : BaseDomainException, new()
        {
            AgainstEmptyString<TException>(value);

            var charIN = value.ToCharArray();
            var multipliers = new int[9] { 2, 4, 8, 5, 10, 9, 7, 3, 6 };
            var sum = 0;

            if (charIN[0] == 0 && charIN[1] == 0)
            {
                charIN[2] = Convert.ToChar(charIN[2] - '4');
            }

            for (int i = 0; i < multipliers.Length; i++)
            {
                sum += int.Parse(charIN[i].ToString()) * multipliers[i];
            }

            var controlNumber = sum / 11;

            if (controlNumber == 10)
            {
                controlNumber = 0;
            }

            charIN[9] = Convert.ToChar(controlNumber);

            //ThrowException<TException>($"{name} is not a valid EGN");
        }

        private static void ThrowException<TException>(string message)
            where TException : BaseDomainException, new()
        {
            var exception = new TException
            {
                Error = message
            };

            throw exception;
        }
    }
}
