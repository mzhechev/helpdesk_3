﻿using System;
using System.Collections.Generic;

namespace HelpDesk_3.Domain.Common
{
    public interface IInitialData
    {
        Type EntityType { get; }

        IEnumerable<object> GetData();
    }
}
