﻿using HelpDesk_3.Domain.Models.Customers;
using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests;
using HelpDesk_3.Domain.Models.Requests.Request.Complaint;

namespace HelpDesk_3.Domain.Factories.Requests.Complaint
{
    internal class ComplaintRequestFactory : IComplaintRequestFactory
    {
        private string Category = default!;
        private string CategoryGroup = default!;
        private Status Status;
        private PersonalInformation PersonalInformation = default!;
        private Customer Customer = default!;
        private ContactInformation ContactInformation = default!;
        private string Content = default!;
        private DateTime ComplaintDate = default!;

        public IComplaintRequestFactory WithRequestData(string category, string categoryGroup, Status status)
        {
            this.Category = category;
            this.CategoryGroup = categoryGroup;
            this.Status = status;
        
            return this;
        }

        public IComplaintRequestFactory WithPersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber)
        {
            this.PersonalInformation = new PersonalInformation(
                firstName,
                middleName,
                lastName,
                identificationNumber);

            return this;
        }

        public IComplaintRequestFactory WithCustomer(Customer customer)
        {
            this.Customer = customer;
            return this;
        }

        public IComplaintRequestFactory WithContactInformation(string phoneNumber, string email)
        {
            this.ContactInformation = new ContactInformation(
                phoneNumber,
                email);
            return this;
        }
        
        public IComplaintRequestFactory WithComplaintDate(DateTime complaintDate)
        {
            this.ComplaintDate = complaintDate;
            return this;
        }
        
        public IComplaintRequestFactory WithContent(string content)
        {
            this.Content = content;
            return this;
        }

        public ComplaintRequest Build()
        {
            return new ComplaintRequest(
                 this.Category,
                 this.CategoryGroup,
                 this.Status,
                 this.PersonalInformation,
                 this.Customer,
                 this.ContactInformation,
                 this.Content,
                 this.ComplaintDate);
        }
    }
}
