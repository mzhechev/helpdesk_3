﻿using HelpDesk_3.Domain.Models.Customers;
using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests.Request.Complaint;

namespace HelpDesk_3.Domain.Factories.Requests.Complaint
{
    public interface IComplaintRequestFactory : IFactory<ComplaintRequest>
    {
        IComplaintRequestFactory WithRequestData(string category, string categoryGroup, Status status);
        IComplaintRequestFactory WithPersonalInformation(string firstName,
                string middleName,
                string lastName,
                string identificationNumber);
        IComplaintRequestFactory WithCustomer(Customer customer);
        IComplaintRequestFactory WithContactInformation(
            string phoneNumber,
            string email);
        IComplaintRequestFactory WithContent(string content);
        IComplaintRequestFactory WithComplaintDate(DateTime complaintDate);
    }
}
