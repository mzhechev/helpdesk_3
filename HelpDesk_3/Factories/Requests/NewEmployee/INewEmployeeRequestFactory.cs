﻿using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Domain.Factories.Requests.NewEmployee
{
    public interface INewEmployeeRequestFactory : IFactory<NewEmployeeRequest>
    {
        INewEmployeeRequestFactory WithRequestData(string category, string categoryGroup, Status status);

        INewEmployeeRequestFactory WithEmployeeType(EmployeeType employeeType);
        
        INewEmployeeRequestFactory WithPlaceOfWork(PlaceOfWork placeOfWork);

        INewEmployeeRequestFactory WithPersonalInformation(string firstName);

        INewEmployeeRequestFactory WithContractInformation(string phoneNumber, string email);
    }
}