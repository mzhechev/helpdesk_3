﻿using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Domain.Factories.Requests.NewEmployee
{
    internal class NewEmployeeRequestFactory : INewEmployeeRequestFactory
    {
        private string Category = default!;
        private string CategoryGroup = default!;
        private Status Status;
        private ContactInformation ContactInformation = default!;
        private PersonalInformation PersonalInformation = default!;
        private EmployeeType EmployeeType = default!;
        private PlaceOfWork PlaceOfWork = default!;

        public INewEmployeeRequestFactory WithRequestData(string category, string categoryGroup, Status status)
        {
            this.Category = category;
            this.CategoryGroup = categoryGroup;
            this.Status = status;

            return this;
        }

        public INewEmployeeRequestFactory WithPersonalInformation(string firstName)
        {
            this.PersonalInformation = new PersonalInformation(firstName);
            return this;
        }

        public INewEmployeeRequestFactory WithEmployeeType(EmployeeType employeeType)
        {
            this.EmployeeType = employeeType;
            return this;
        }

        public INewEmployeeRequestFactory WithPlaceOfWork(PlaceOfWork placeOfWork)
        {
            this.PlaceOfWork = placeOfWork;
            return this;
        }

        public INewEmployeeRequestFactory WithContractInformation(string phoneNumber, string email)
        {
            this.ContactInformation = new ContactInformation(phoneNumber, email);
            return this;
        }

        public NewEmployeeRequest Build()
        {
            return new NewEmployeeRequest(
                this.Category,
                this.CategoryGroup,
                this.Status,
                this.PersonalInformation,
                this.ContactInformation,
                this.EmployeeType,
                this.PlaceOfWork);
        }

    }
}
