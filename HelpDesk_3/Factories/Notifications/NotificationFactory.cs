﻿using HelpDesk_3.Domain.Models.Notifications;

namespace HelpDesk_3.Domain.Factories.Notifications
{
    internal class NotificationFactory : INotificationFactory
    {
        private string name = default!;
        private string reciever = default!;
        private string description = default!;
        private DateTime timeSent = default!;

        public INotificationFactory WithDescription(string description)
        {
            this.description = description;

            return this;
        }

        public INotificationFactory WithReciever(string reciever)
        {
            this.reciever = reciever;

            return this;
        }

        public INotificationFactory WithName(string name)
        {
            this.name = name;

            return this;
        }

        public INotificationFactory WithTimeSent(DateTime timeSent)
        {
            this.timeSent = timeSent;

            return this;
        }

        public Email Build()
        {
            return new Email(
                name, 
                reciever, 
                description, 
                timeSent);
        }
    }
}
