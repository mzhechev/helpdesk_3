﻿using HelpDesk_3.Domain.Models.Notifications;

namespace HelpDesk_3.Domain.Factories.Notifications
{
    public interface INotificationFactory : IFactory<Email>
    {
        INotificationFactory WithName(string name);

        INotificationFactory WithReciever(string reciever);

        INotificationFactory WithDescription(string description);

        INotificationFactory WithTimeSent(DateTime timeSent);
    }
}
