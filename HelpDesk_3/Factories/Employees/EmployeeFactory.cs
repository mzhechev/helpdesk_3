﻿using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Employees;
using HelpDesk_3.Domain.Models.Requests;

namespace HelpDesk_3.Domain.Factories.Employees
{
    internal class EmployeeFactory : IEmployeeFactory
    {
        private PersonalInformation personalInformation = default!;

        private bool personalInformationSet = false;

        public IEmployeeFactory WithPersonalInformation(
            string firstName, 
            string middleName, 
            string lastName, 
            string identificationNumber)
        {
            this.personalInformation = new PersonalInformation(
                firstName,
                middleName,
                lastName,
                identificationNumber);
            personalInformationSet = true;

            return this;
        }

        public Employee Build()
        {
            if (!personalInformationSet)
            {
                throw new InvalidEmployeeException("Employee, must have personal information");
            }

            return new Employee(
                this.personalInformation);
        }
    }
}
