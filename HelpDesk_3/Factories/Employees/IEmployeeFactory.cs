﻿using HelpDesk_3.Domain.Models.Employees;

namespace HelpDesk_3.Domain.Factories.Employees
{
    public interface IEmployeeFactory : IFactory<Employee>
    {
        IEmployeeFactory WithPersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber);
    }
}
