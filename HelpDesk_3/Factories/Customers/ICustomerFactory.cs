﻿using HelpDesk_3.Domain.Models.Customers;
using HelpDesk_3.Domain.Models.Requests;

namespace HelpDesk_3.Domain.Factories.Customers
{
    public interface ICustomerFactory : IFactory<Customer>
    {
        ICustomerFactory WithPersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber);
    }
}
