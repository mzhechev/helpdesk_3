﻿using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Customers;
using HelpDesk_3.Domain.Models.Requests;

namespace HelpDesk_3.Domain.Factories.Customers
{
    internal class CustomerFactory : ICustomerFactory
    {
        private PersonalInformation personalInformation = default!;

        private bool personalInformationSet = false;

        public ICustomerFactory WithPersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber)
        {
            this.personalInformation = new PersonalInformation(
                firstName, 
                middleName, 
                lastName, 
                identificationNumber);  
            personalInformationSet = true;

            return this;
        }

        public Customer Build()
        {
            if (!personalInformationSet)
            {
                throw new InvalidCustomerException("Customer, must have personal information");
            }

            return new Customer(
                this.personalInformation);
        }
    }
}
