﻿using HelpDesk_3.Domain.Common;

namespace HelpDesk_3.Domain.Factories
{
    public interface IFactory<out TEntity>
        where TEntity : IAggregateRoot
    {
        TEntity Build();
    }
}
