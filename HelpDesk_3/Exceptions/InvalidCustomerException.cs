﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidCustomerException : BaseDomainException
    {
        public InvalidCustomerException()
        {

        }

        public InvalidCustomerException(string error) => this.Error = error;
    }
}
