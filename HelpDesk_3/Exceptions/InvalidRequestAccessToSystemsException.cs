﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidRequestAccessToSystemsException : BaseDomainException
    {
        public InvalidRequestAccessToSystemsException()
        {

        }

        public InvalidRequestAccessToSystemsException(string error) => this.Error = error;
    }
}
