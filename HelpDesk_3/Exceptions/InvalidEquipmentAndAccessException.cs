﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidEquipmentAndAccessException : BaseDomainException
    {
        public InvalidEquipmentAndAccessException()
        {

        }

        public InvalidEquipmentAndAccessException(string error) => this.Error = error;
    }
}
