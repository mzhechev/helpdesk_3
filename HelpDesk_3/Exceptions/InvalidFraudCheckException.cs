﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidFraudCheckException : BaseDomainException
    {
        public InvalidFraudCheckException()
        {

        }

        public InvalidFraudCheckException(string error) => this.Error = error;
    }
}

