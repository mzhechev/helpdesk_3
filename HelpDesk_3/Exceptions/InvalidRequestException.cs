﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidRequestException : BaseDomainException
    {
        public InvalidRequestException()
        {

        }

        public InvalidRequestException(string error) => this.Error = error;
    }
}
