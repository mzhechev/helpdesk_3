﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidComplaintInformationException : BaseDomainException
    {
        public InvalidComplaintInformationException()
        {

        }

        public InvalidComplaintInformationException(string error) => this.Error = error;
    }
}
