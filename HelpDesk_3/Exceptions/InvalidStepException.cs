﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidStepException : BaseDomainException
    {
        public InvalidStepException()
        {

        }

        public InvalidStepException(string error) => this.Error = error;
    }
}
