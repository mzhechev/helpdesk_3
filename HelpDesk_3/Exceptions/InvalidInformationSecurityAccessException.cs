﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidInformationSecurityAccessException : BaseDomainException
    {
        public InvalidInformationSecurityAccessException()
        {

        }

        public InvalidInformationSecurityAccessException(string error) => this.Error = error;
    }
}
