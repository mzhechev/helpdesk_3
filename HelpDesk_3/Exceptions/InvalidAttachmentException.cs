﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidAttachmentException : BaseDomainException
    {
        public InvalidAttachmentException()
        {

        }

        public InvalidAttachmentException(string error) => this.Error = error;
    }
}
