﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidEmployeeException : BaseDomainException
    {
        public InvalidEmployeeException()
        {

        }

        public InvalidEmployeeException(string error) => this.Error = error;
    }
}
