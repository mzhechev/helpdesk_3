﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidStartDocumentsSignedException : BaseDomainException
    {
        public InvalidStartDocumentsSignedException()
        {

        }

        public InvalidStartDocumentsSignedException(string error) => this.Error = error;
    }
}
