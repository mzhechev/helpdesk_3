﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidLeaveRequestException : BaseDomainException
    {
        public InvalidLeaveRequestException()
        {

        }

        public InvalidLeaveRequestException(string error) => this.Error = error;
    }
}
