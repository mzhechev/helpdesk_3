﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidAccessToSystemsDataException : BaseDomainException
    {
        public InvalidAccessToSystemsDataException()
        {

        }

        public InvalidAccessToSystemsDataException(string error) => this.Error = error;
    }
}
