﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidOrganizationDataException : BaseDomainException
    {
        public InvalidOrganizationDataException()
        {

        }

        public InvalidOrganizationDataException(string error) => this.Error = error;
    }
}
