﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidCommentException : BaseDomainException
    {
        public InvalidCommentException()
        {

        }

        public InvalidCommentException(string error) => this.Error = error;
    }
}
