﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidEmailException : BaseDomainException
    {
        public InvalidEmailException()
        {

        }

        public InvalidEmailException(string error) => this.Error = error;
    }
}
