﻿namespace HelpDesk_3.Domain.Exceptions
{
    public class InvalidContactInformationException : BaseDomainException
    {
        public InvalidContactInformationException()
        {

        }

        public InvalidContactInformationException(string error) => this.Error = error;
    }
}
