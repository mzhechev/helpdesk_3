﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;

namespace HelpDesk_3.Domain.Models.Employees
{
    public class LeaveRequest : Entity<int>
    {
        internal LeaveRequest(
            DateTime startDate, 
            DateTime endDate)
        {
            Validate(startDate, endDate);

            this.StartDate = startDate;
            this.EndDate = endDate;
        }

        public DateTime StartDate { get; private set; }

        public DateTime EndDate { get; private set; }

        public LeaveRequest UpdateStartDate(DateTime startDate)
        {
            if (startDate != DateTime.MinValue.AddDays(1)) 
            {
                this.ValidateStartDate(startDate);
                this.StartDate = startDate;
            }

            return this;
        }

        public LeaveRequest UpdateEndDate(DateTime endDate)
        {
            if (endDate != DateTime.MinValue.AddDays(1))
            {
                this.ValidateStartDate(endDate);
                this.EndDate = endDate;
            }

            return this;
        }

        private void Validate(DateTime startDate, DateTime endDate)
        {
            ValidateStartDate(startDate);
            ValidateEndDate(endDate);
            
            if (startDate > endDate)
            {
                throw new InvalidLeaveRequestException("Leave request START date should be before END date.");
            }
        }

        private void ValidateStartDate(DateTime startDate)
            => Guard.ForFutureDate<InvalidLeaveRequestException>(
                startDate,
                nameof(this.StartDate));

        private void ValidateEndDate(DateTime endDate)
            => Guard.ForFutureDate<InvalidLeaveRequestException>(
                endDate,
                nameof(this.EndDate));
    }
}
