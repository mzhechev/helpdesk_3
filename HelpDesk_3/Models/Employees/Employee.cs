﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Requests;

namespace HelpDesk_3.Domain.Models.Employees
{
    public class Employee : Entity<int>, IAggregateRoot
    {
        private readonly HashSet<LeaveRequest> leaveRequests;

        internal Employee(
            PersonalInformation personalInformation)
        {
            this.PersonalInformation = personalInformation;
            this.leaveRequests = new HashSet<LeaveRequest>();
        }
        private Employee()
        {
            this.leaveRequests = new HashSet<LeaveRequest>();
        }

        public PersonalInformation PersonalInformation { get; private set; } = default!;

        public int AvailableLeave { get; private set; }

        public Employee UpdatePersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber)
        {
            this.PersonalInformation
                .UpdateFirstName(firstName)
                .UpdateMiddleName(middleName)
                .UpdateLastName(lastName)
                .UpdateIdentificationNumber(identificationNumber);

            return this;
        }

        public Employee UpdateAvailableLeave(int availableLeave)
        {
            this.ValidateAvailableLeave(availableLeave);
            this.AvailableLeave = availableLeave;

            return this;
        } 

        public void AddLeaveRequest(LeaveRequest leaveRequest) 
            => leaveRequests.Add(leaveRequest);

        public IReadOnlyCollection<LeaveRequest> LeaveRequests
            => this.leaveRequests.ToList().AsReadOnly();

        private void ValidateAvailableLeave(int availableLeave)
            => Guard.AgainstNegativeNumber<InvalidEmployeeException>(
                availableLeave,
                nameof(this.AvailableLeave));
    }
}
