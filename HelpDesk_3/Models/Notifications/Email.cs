﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;

using static HelpDesk_3.Domain.Models.ModelConstants.Email;

namespace HelpDesk_3.Domain.Models.Notifications
{
    public class Email : Entity<Guid>, IAggregateRoot
    {
        //add: Sent From
        internal Email(
            string name,
            string reciever,
            string descripton,
            DateTime timeSent)
        {
            Validate(name, reciever, descripton, timeSent);

            this.Name = name;
            this.Reciever = reciever;
            this.Description = descripton;
            this.TimeSent = timeSent;
        }

        private Email(
            string name,
            string reciever,
            string description)
        {
            this.Name = name;
            this.Reciever = reciever;
            this.Description = description;
        }

        public string Name { get; private set; }

        public string Reciever { get; private set; }

        public string Description { get; private set; }

        public DateTime TimeSent { get; private set; }

        private void Validate(string name, string reciever, string descripton, DateTime timeSent) 
        {
            ValidateName(name);
            ValidateReciever(reciever);
            ValidateDescription(descripton);
            ValidateTimeSent(timeSent);
        }

        private void ValidateName(string name)
            => Guard.ForStringLength<InvalidEmailException>(
                name,
                MinEmailNameLength,
                MaxEmailNameLength,
                nameof(this.Name));

        private void ValidateReciever(string reciever)
            => Guard.ForStringLength<InvalidEmailException>(
                reciever,
                MinRecieverLength,
                MaxRecieverLength,
                nameof(this.Reciever));

        private void ValidateDescription(string description)
            => Guard.ForStringLength<InvalidEmailException>(
                description,
                MinDescriptionLength,
                MaxDescriptionLength,
                nameof(this.Description));

        private void ValidateTimeSent(DateTime timeSent)
            => Guard.ForFutureDate<InvalidEmailException>(
                timeSent,
                nameof(this.Description));
    }
}
