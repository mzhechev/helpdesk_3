﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Models.Requests;

namespace HelpDesk_3.Domain.Models.Customers
{
    public class Customer : Entity<Guid>, IAggregateRoot
    {
        internal Customer(
            PersonalInformation personalInformation)
        {
            this.PersonalInformation = personalInformation;
        }
        private Customer(){}

        public PersonalInformation PersonalInformation { get; private set; } = default!;

        public Customer UpdatePersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber)
        {
            this.PersonalInformation
                .UpdateFirstName(firstName)
                .UpdateMiddleName(middleName)
                .UpdateLastName(lastName)
                .UpdateIdentificationNumber(identificationNumber);

            return this;
        }
    }
}
