﻿namespace HelpDesk_3.Domain.Models
{
    public class ModelConstants
    {
        public class Common
        {
            public const int MinNameLength = 2;
            public const int MaxNameLength = 20;

            public const int MinEmailLength = 3;
            public const int MaxEmailLength = 50;

            public const int IdentificationNumberLength = 10;

            public const int MaxUrlLength = 2048;

            public const int Zero = 0;

            public const int MinDescriptionLength = 0;
            public const int MaxDescriptionLength = 500;

            public const int MinCommentLength = 200;
            public const int MaxCommentLength = 0;

            public const int MinUsernameLength = 6;
            public const int MaxUsernameLength = 20;

            public const int MinPasswordLength = 6;
            public const int MaxPasswordLength = 20;

            public const int EGNLength = 10;

            public const int IBANLength = 34;

            public const int PhoneLength = 10;
        }

        public class Categories
        {
            public const int MinCategoriesLength = 0;
            public const int MaxCategoriesLength = 50;
        }

        public class Address
        {
            public const int MinCountryLength = 0;
            public const int MaxCountryLength = 56;

            public const int MinCityLength = 0;
            public const int MaxCityLength = 50;
            
            public const int MinStreetLength = 0;
            public const int MaxStreetLength = 150;

            public const int MinStreetNumberLength = 0;
            public const int MaxStreetNumberLength = 999;

            public const int MinAppartmentLength = 0;
            public const int MaxAppartmentLength = 1160;

            public const int MinUnitLength = 0;
            public const int MaxUnitLength = 200;

            public const int MinFloorLength = 0;
            public const int MaxFloorLength = 163;
        }

        public class Email
        {
            public const int MinEmailNameLength = 0;
            public const int MaxEmailNameLength = 100;

            public const int MinRecieverLength = 0;
            public const int MaxRecieverLength = 100;

            public const int MinDescriptionLength = 0;
            public const int MaxDescriptionLength = 300;
        }

        public class ContactInformation
        {
            public const int MinPhoneNumberLength = 0;
            public const int MaxPhoneNumberLengthLength = 10;

            public const int MinEmailLength = 0;
            public const int MaxEmailLength = 50;
        }

        public class ComplaintInformation
        {
            public const int MinComplaintInformationLength = 2;
            public const int MaxComplaintInformationLength = 100;
        }

        public class AccessToSystemsData
        {
            public const int MinUpdateAdditionalAccessesRequestedLength = 2;
            public const int MaxUpdateAdditionalAccessesRequestedLength = 50; //?
        }

        public class ComplaintRequest
        {
            public const int ContractNumberLength = 10;

            public const int MinMerchantNameLength = 2;
            public const int MaxMerchantNameLength = 50; //?

            public const int MinNotificationTypeLength = 1;
            public const int MaxNotificationTypeLength = 10;

            public const int MinContentLength = 1;
            public const int MaxContentLength = 20; //?

            public const int MinCommunicationHistoryLength = 1;
            public const int MaxCommunicationHistoryLength = 100; //?

            public const int MinNumberOfClientContractsLength = 1;
            public const int MaxNumberOfClientContractsLength = 3;

            public const int MinNameOfClientContractsLength = 1;
            public const int MaxNameOfClientContractsLength = 100;

            public const int MinSolvedInFavorLength = 1;
            public const int MaxSolvedInFavorLength = 10;

            public const int MinAmountPVCCRONLength = 1;
            public const int MaxAmountPVCCRONLength = 15;

            public const int RegistrationNumberLength = 15;

            public const int MinResponseDays = 1;
            public const int MaxResponseDays = 999;

            public const int AmountPVCCRONLength = 10;
        }

        public class EquipmentAndAccess
        {
            public const int MinAdditionalEquipmentLength = 1;
            public const int MaxAdditionalEquipmentLength = 100;

            public const int MinWorkingPlaceLocationLength = 1;
            public const int MaxWorkingPlaceLocationLength = 30;

            public const int MinLeavingEmployeeLength = 1;
            public const int MaxLeavingEmployeeLength = 20;
        }

        public class FraudCheck
        {
            public const int MinSmartInfoLength = 1;
            public const int MaxSmartInfoLength = 20; //?
        }

        public class OrganizationData
        {
            public const int MinDivisionLength = 1;
            public const int MaxDivisionLength = 20;

            public const int MinDepartmentLength = 1;
            public const int MaxDepartmentLength = 20;

            public const int MinUnitLength = 1;
            public const int MaxUnitLength = 20;

            public const int MinLocationLength = 1;
            public const int MaxLocationLength = 20;

            public const int MinCostProfitAreaLength = 1;
            public const int MaxCostProfitAreaLength = 20; //?

            public const int MinCostCenterLength = 1;
            public const int MaxCostCenterLength = 20; //?

            public const int MinOfficialPositionLength = 1;
            public const int MaxOfficialPositionLength = 50;

            public const int MinInternalPositionLength = 1;
            public const int MaxInternalPositionLength = 50;

            public const int MinDirectManagerAdminReportingLength = 1;
            public const int MaxDirectManagerAdminReportingLength = 50;

            public const int MinDirectManagerFunctionalReportingLength = 1;
            public const int MaxDirectManagerFunctionalReportingLength = 50;
        }

        public class RequestAccessToSystems
        {

            public const int MinRolesLength = 1;
            public const int MaxRolesLength = 100;

            public const int MinAccessLikeUserLength = 1;
            public const int MaxAccessLikeUserLength = 50;

            public const int MinAccessToOtherSystemsLength = 1;
            public const int MaxAccessToOtherSystemsLength = 100;

            public const int MinEmailGroupsLength = 1;
            public const int MaxEmailGroupsLength = 200;

            public const int MinSharedFoldersLength = 1;
            public const int MaxSharedFoldersLength = 200;
        }
    }
}
