﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using System.Net.Mail;
using static HelpDesk_3.Domain.Models.ModelConstants.ContactInformation;

namespace HelpDesk_3.Domain.Models.Requests
{
    public class ContactInformation : ValueObject
    {
        internal ContactInformation(
            string phoneNumber,
            string email)
        {
            Validate(phoneNumber, email);

            this.PhoneNumber = phoneNumber;
            this.Email = email;
        }

        public string? PhoneNumber { get; private set; }

        public string? Email { get; private set; }

        public ContactInformation UpdatePhoneNumber(string phoneNumber)
        {
            if (phoneNumber != null)
            {
                this.ValidatePhoneNumber(phoneNumber);
                this.PhoneNumber = phoneNumber;
            }

            return this;
        }

        public ContactInformation UpdateEmail(string email)
        {
            if (email != null)
            {
                this.ValidateEmail(email);
                this.Email = email;
            }

            return this;
        }

        private void Validate(string phoneNumber, string email)
        {
            ValidatePhoneNumber(phoneNumber);
            ValidateEmail(email);
        }

        private void ValidatePhoneNumber(string phoneNumber)
            => Guard.ForCorrectPhone<InvalidContactInformationException>(
                phoneNumber,
                nameof(this.PhoneNumber));

        private void ValidateEmail(string email)
            => Guard.ForCorrectEmail<InvalidContactInformationException>(
                email,
                nameof(this.Email));
    }
}
