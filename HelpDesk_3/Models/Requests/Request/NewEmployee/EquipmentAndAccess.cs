﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.EquipmentAndAccess;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class EquipmentAndAccess : ValueObject
    {
        internal EquipmentAndAccess() { }

        public WorkingPlace? WorkingPlace { get; private set; } = default!;

        public bool? StationaryPhone { get; private set; } = default!;

        public string? WorkingPlaceLocation { get; private set; } = default!;

        public string? LeavingEmployee { get; private set; } = default!;

        public MobilePhone? MobilePhone { get; private set; } = default!;

        public bool? Uniform { get; private set; } = default!;

        public bool? CompanyCar { get; private set; } = default!;

        public PrefferedEquipment? PrefferedEquipment { get; private set; } = default!;

        public bool? VPN { get; private set; } = default!;

        public string? AdditionalEquipment { get; private set; } = default!;

        public EquipmentAndAccess UpdateMobilePhone(MobilePhone mobilePhone)
        {
            this.MobilePhone = mobilePhone;
            return this;
        }

        public EquipmentAndAccess UpdateUniform(bool uniform)
        {
            this.Uniform = uniform;
            return this;
        }

        public EquipmentAndAccess UpdateCompanyCar(bool companyCar)
        {
            this.CompanyCar = companyCar;
            return this;
        }

        public EquipmentAndAccess UpdatePrefferedEquipment(PrefferedEquipment prefferedEquipment)
        {
            this.PrefferedEquipment = prefferedEquipment;
            return this;
        }

        public EquipmentAndAccess UpdateVPN(bool vPN)
        {
            this.VPN = vPN;
            return this;
        }

        public EquipmentAndAccess UpdateAdditionalEquipment(string additionalEquipment)
        {
            if (additionalEquipment != null)
            {
                this.ValidateAdditionalEquipment(additionalEquipment);
                this.AdditionalEquipment = additionalEquipment;
            }

            return this;
        }

        public EquipmentAndAccess UpdateWorkingPlace(WorkingPlace workingPlace)
        {
            this.WorkingPlace = workingPlace;
            return this;
        }

        public EquipmentAndAccess UpdateStationaryPhone(bool stationaryPhone)
        {
            this.StationaryPhone = stationaryPhone;
            return this;
        }

        public EquipmentAndAccess UpdateWorkingPlaceLocation(string workingPlaceLocation)
        {
            if (workingPlaceLocation != null)
            {
                this.ValidateWorkingPlaceLocation(workingPlaceLocation);
                this.WorkingPlaceLocation = workingPlaceLocation;
            }

            return this;
        }

        public EquipmentAndAccess UpdateLeavingEmployee(string leavingEmployee)
        {
            if (leavingEmployee != null)
            {
                this.ValidateLeavingEmployee(leavingEmployee);
                this.LeavingEmployee = leavingEmployee;
            }

            return this;
        }

        private void Validate(
            string additionalEquipment, 
            string workingPlaceLocation,
            string leavingEmployee)
        {
            this.ValidateAdditionalEquipment(additionalEquipment);
            this.ValidateWorkingPlaceLocation(workingPlaceLocation);
            this.ValidateLeavingEmployee(leavingEmployee);
        }

        private void ValidateAdditionalEquipment(string additionalEquipment)
            => Guard.ForStringLength<InvalidEquipmentAndAccessException>(
                additionalEquipment,
                MinAdditionalEquipmentLength, 
                MaxAdditionalEquipmentLength, 
                nameof(this.AdditionalEquipment));

        private void ValidateWorkingPlaceLocation(string workingPlaceLocation)
            => Guard.ForStringLength<InvalidEquipmentAndAccessException>(
                workingPlaceLocation, 
                MinWorkingPlaceLocationLength, 
                MaxWorkingPlaceLocationLength, 
                nameof(this.WorkingPlaceLocation));
        private void ValidateLeavingEmployee(string leavingEmployee)
            => Guard.ForStringLength<InvalidEquipmentAndAccessException>(
                leavingEmployee, 
                MinLeavingEmployeeLength, 
                MaxLeavingEmployeeLength, 
                nameof(this.LeavingEmployee));
    }
}
