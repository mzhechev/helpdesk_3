﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class InformationSecurityAccess : ValueObject
    {
        internal InformationSecurityAccess() { }

        public bool? VPNApproved { get; private set; } = default!;

        public string? VPNComment { get; private set; } = default!;

        public bool? MobileDeviceApproved { get; private set; } = default!;

        public string? MobileDeviceComment { get; private set; } = default!;

        public bool? AccessVerificationApproved { get; private set; } = default!;

        public string? AccessVerificationComment { get; private set; } = default!;

        public InformationSecurityAccess UpdateVPN(bool vpnApproved)
        {
            this.VPNApproved = vpnApproved;
            return this;
        }

        public InformationSecurityAccess UpdateVPNComment(string vpnComment)
        {
            if (vpnComment != null)
            {
                this.ValidateVPNComment(vpnComment);
                this.VPNComment = vpnComment;
            }

            return this;
        }

        public InformationSecurityAccess UpdateMobileDeviceApproved(bool mobileDeviceApproved)
        {
            this.MobileDeviceApproved = mobileDeviceApproved;
            return this;
        }

        public InformationSecurityAccess UpdateMobileDeviceApproved(string mobileDeviceComment)
        {
            if (mobileDeviceComment != null)
            {
                this.ValidateMobileDeviceComment(mobileDeviceComment);
                this.MobileDeviceComment = mobileDeviceComment;
            }

            return this;
        }

        public InformationSecurityAccess UpdateAccessVerificationApproved(bool accessVerificationApproved)
        {
            this.AccessVerificationApproved = accessVerificationApproved;
            return this;
        }

        public InformationSecurityAccess UpdateAccessVerificationComment(string accessVerificationComment)
        {
            if (accessVerificationComment != null)
            {
                this.ValidateAccessVerificationComment(accessVerificationComment);
                this.AccessVerificationComment = accessVerificationComment;
            }

            return this;
        }

        private void Validate(string vpnComment, string mobileDeviceComment, string accessVerificationComment)
        {
            this.ValidateVPNComment(vpnComment);
            this.ValidateMobileDeviceComment(mobileDeviceComment);
            this.ValidateAccessVerificationComment(accessVerificationComment);
        }

        private void ValidateVPNComment(string vpnComment)
            => Guard.ForStringLength<InvalidInformationSecurityAccessException>(
                vpnComment, 
                MinCommentLength, 
                MaxCommentLength, 
                nameof(vpnComment));

        private void ValidateMobileDeviceComment(string mobileDeviceComment)
            => Guard.ForStringLength<InvalidInformationSecurityAccessException>(
                mobileDeviceComment,
                MinCommentLength,
                MaxCommentLength,
                nameof(mobileDeviceComment));

        private void ValidateAccessVerificationComment(string accessVerificationComment)
            => Guard.ForStringLength<InvalidInformationSecurityAccessException>(
                accessVerificationComment,
                MinCommentLength,
                MaxCommentLength,
                nameof(accessVerificationComment));
    }
}
