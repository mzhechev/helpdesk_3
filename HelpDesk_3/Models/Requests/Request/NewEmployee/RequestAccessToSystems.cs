﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.RequestAccessToSystems;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class RequestAccessToSystems : ValueObject
    {
        internal RequestAccessToSystems() { }

        public bool? OperationalRisk { get; private set; }
        public bool? SmartInfo { get; private set; }
        public string? SmartInfoRoles { get; private set; } = default!;
        public bool? SCard { get; private set; }
        public string? SCardRoles { get; private set; } = default!;
        public bool? FOS { get; private set; }
        public string? FOSRoles { get; private set; } = default!;
        public bool? CeGate { get; private set; }
        public bool? ICollect { get; private set; }
        public string? ICollectRoles { get; private set; } = default!;
        public bool? Syron { get; private set; }
        public string? SyronRoles { get; private set; } = default!;
        public string? AccessLikeUser { get; private set; } = default!;
        public bool? VCS { get; private set; }
        public string? VCSRoles { get; private set; } = default!;
        public PhoneRecieving? PhoneRecievingVCS { get; private set; }
        public bool? CustomerView { get; private set; }
        public string? AccessToOtherSystems { get; private set; } = default!;
        public bool? AccessToEmailByMobile { get; private set; }
        public string? EmailGroups { get; private set; } = default!;
        public string? SharedFolders { get; private set; } = default!;

        public RequestAccessToSystems UpdateOperationalRisk(bool operationalRisk)
        {
            this.OperationalRisk = operationalRisk;
            return this;
        }
        
        public RequestAccessToSystems UpdateSmartInfo(bool smartInfo)
        {
            this.SmartInfo = smartInfo;
            return this;
        }

        public RequestAccessToSystems UpdateSmartInfoRoles(string smartInfoRoles)
        {
            if (smartInfoRoles != null)
            {
                this.ValidateSmartInfoRoles(smartInfoRoles);
                this.SmartInfoRoles = smartInfoRoles;
            }

            return this;
        }

        public RequestAccessToSystems UpdateSCard(bool sCard)
        {
            this.SCard = sCard;
            return this;
        }

        public RequestAccessToSystems UpdateSCardRoles(string sCardRoles)
        {
            if (sCardRoles != null)
            {
                this.ValidateSCardRoles(sCardRoles);
                this.SCardRoles = sCardRoles;
            }

            return this;
        }

        public RequestAccessToSystems UpdateFOS(bool FOS)
        {
            this.FOS = FOS;
            return this;
        }

        public RequestAccessToSystems UpdateFOSRoles(string FOSRoles)
        {
            if (FOSRoles != null)
            {
                this.ValidateFOSRoles(FOSRoles);
                this.FOSRoles = FOSRoles;
            }

            return this;
        }

        public RequestAccessToSystems UpdateICollect(bool iCollect)
        {
            this.ICollect = iCollect;
            return this;
        }

        public RequestAccessToSystems UpdateICollectRoles(string iCollectRoles)
        {
            if (iCollectRoles != null)
            {
                this.ValidateICollectRoles(iCollectRoles);
                this.ICollectRoles = iCollectRoles;
            }

            return this;
        }

        public RequestAccessToSystems UpdateSyron(bool syron)
        {
            this.Syron = syron;
            return this;
        }

        public RequestAccessToSystems UpdateSyronRoles(string syronRoles)
        {
            if (syronRoles != null)
            {
                this.ValidateSyronRoles(syronRoles);
                this.SyronRoles = syronRoles;
            }

            return this;
        }

        public RequestAccessToSystems UpdateAccessLikeUser(string accessLikeUser)
        {
            if (accessLikeUser != null)
            {
                this.ValidateAccessLikeUser(accessLikeUser);
                this.AccessLikeUser = accessLikeUser;
            }

            return this;
        }

        public RequestAccessToSystems UpdateVCS(bool vcs)
        {
            this.VCS = vcs;
            return this;
        }

        public RequestAccessToSystems UpdateVCSRoles(string vcsRoles)
        {
            if (vcsRoles != null)
            {
                this.ValidateVCSRoles(vcsRoles);
                this.VCSRoles = vcsRoles;
            }

            return this;
        }

        public RequestAccessToSystems UpdatePhoneRecievingVCS(PhoneRecieving phoneRecieving)
        {
            this.PhoneRecievingVCS = phoneRecieving;
            return this;
        }

        public RequestAccessToSystems UpdateCustomerView(bool customerView)
        {
            this.CustomerView = customerView;
            return this;
        }

        public RequestAccessToSystems UpdateAccessToOtherSystems(string accessToOtherSystems)
        {
            if (accessToOtherSystems != null)
            {
                this.ValidateAccessToOtherSystems(accessToOtherSystems);
                this.AccessToOtherSystems = accessToOtherSystems;
            }

            return this;
        }

        public RequestAccessToSystems UpdateAccessToEmailByMobile(bool accessToEmailByMobile)
        {
            this.AccessToEmailByMobile = accessToEmailByMobile;
            return this;
        }

        public RequestAccessToSystems UpdateEmailGroups(string emailGroups)
        {
            if (emailGroups != null)
            {
                this.ValidateEmailGroups(emailGroups);
                this.EmailGroups = emailGroups;
            }

            return this;
        }

        public RequestAccessToSystems UpdateSharedFolders(string sharedFolders)
        {
            if (sharedFolders != null)
            {
                this.ValidateSharedFolders(sharedFolders);
                this.SharedFolders = sharedFolders;
            }

            return this;
        }

        private void Validate(
            string smartInfoRoles, 
            string sCardRoles, 
            string fosRoles, 
            string iCollectRoles, 
            string syronRoles, 
            string accessLikeUser, 
            string vcsRoles, 
            string accessToOtherSystems, 
            string emailGroups, 
            string sharedFolders)
        {
            ValidateSmartInfoRoles(smartInfoRoles);
            ValidateSCardRoles(sCardRoles);
            ValidateFOSRoles(fosRoles);
            ValidateICollectRoles(iCollectRoles);
            ValidateSyronRoles(syronRoles);
            ValidateAccessLikeUser(accessLikeUser);
            ValidateVCSRoles(vcsRoles);
            ValidateAccessToOtherSystems(accessToOtherSystems);
            ValidateEmailGroups(emailGroups);
            ValidateSharedFolders(sharedFolders);
        }

        private void ValidateSmartInfoRoles(string smartInfoRoles)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                smartInfoRoles,
                MinRolesLength,
                MaxRolesLength, 
                nameof(this.SmartInfoRoles));

        private void ValidateSCardRoles(string sCardRoles)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                sCardRoles, 
                MinRolesLength, 
                MaxRolesLength, 
                nameof(this.SCardRoles));

        private void ValidateFOSRoles(string fosRoles)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                fosRoles, 
                MinRolesLength, 
                MaxRolesLength, 
                nameof(this.FOSRoles));

        private void ValidateICollectRoles(string iCollectRoles)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                iCollectRoles, 
                MinRolesLength, 
                MaxRolesLength, 
                nameof(this.ICollectRoles));

        private void ValidateSyronRoles(string syronRoles)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                syronRoles, 
                MinRolesLength, 
                MaxRolesLength, 
                nameof(this.SyronRoles));

        private void ValidateAccessLikeUser(string accessLikeUser)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                accessLikeUser, 
                MinAccessLikeUserLength, 
                MaxAccessLikeUserLength, 
                nameof(this.AccessLikeUser));

        private void ValidateVCSRoles(string vcsRoles)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                vcsRoles, 
                MinRolesLength, 
                MaxRolesLength, 
                nameof(this.VCSRoles));

        private void ValidateAccessToOtherSystems(string accessToOtherSystems)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                accessToOtherSystems, 
                MinAccessToOtherSystemsLength, 
                MaxAccessToOtherSystemsLength, 
                nameof(this.AccessToOtherSystems));

        private void ValidateEmailGroups(string emailGroups)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                emailGroups, 
                MinEmailGroupsLength, 
                MaxEmailGroupsLength, 
                nameof(this.EmailGroups));

        private void ValidateSharedFolders(string sharedFolders)
            => Guard.ForStringLength<InvalidRequestAccessToSystemsException>(
                sharedFolders, 
                MinSharedFoldersLength, 
                MaxSharedFoldersLength, 
                nameof(this.SharedFolders));
    }
}
