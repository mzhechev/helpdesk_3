﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class StartDocumentsSigned : ValueObject
    {
        internal StartDocumentsSigned() { }

        public bool? SignedJobDescription { get; private set; }

        public bool? SignedLabourContract { get; private set; }

        public bool? PCCCheck { get; private set; }

        public DateTime? PCCCheckDate { get; private set; }

        public bool? InstructionNoteHS { get; private set; }

        public bool? NRA { get; private set; }

        public bool? VideoSurveillance { get; private set; }

        public bool? GDPR { get; private set; }

        public bool? GDPRPhotosVideo { get; private set; }

        public StartDocumentsSigned UpdateSignedJobDescription(bool signedJobDescription)
        {
            this.SignedJobDescription = signedJobDescription;
            return this;
        }

        public StartDocumentsSigned UpdateSignedLabourContract(bool signedLabourContract)
        {
            this.SignedLabourContract = signedLabourContract;
            return this;
        }

        public StartDocumentsSigned UpdatePCCCheck(bool pccCheck)
        {
            this.PCCCheck = pccCheck;
            return this;
        }

        public StartDocumentsSigned UpdatePCCCheckDate(DateTime pccCheckDate)
        {
            if (pccCheckDate != DateTime.MinValue)
            {
                this.ValidatePCCCheckDate(pccCheckDate);
                this.PCCCheckDate = pccCheckDate;
            }

            return this;
        }

        public StartDocumentsSigned UpdateInstructionNoteHS(bool instructionNoteHS)
        {
            this.InstructionNoteHS = instructionNoteHS;
            return this;
        }

        public StartDocumentsSigned UpdateNRA(bool nra)
        {
            this.NRA = nra;
            return this;
        }

        public StartDocumentsSigned UpdateVideoSurveillance(bool videoSurveillance)
        {
            this.VideoSurveillance = videoSurveillance;
            return this;
        }

        public StartDocumentsSigned UpdateGDPR(bool gdpr)
        {
            this.GDPR = gdpr;
            return this;
        }

        public StartDocumentsSigned UpdateGDPRPhotosVideo(bool gdprPhotosVideo)
        {
            this.GDPRPhotosVideo = gdprPhotosVideo;
            return this;
        }

        private void ValidatePCCCheckDate(DateTime pccCheckDate)
            => Guard.ForFutureDate<InvalidStartDocumentsSignedException>(pccCheckDate, nameof(this.PCCCheckDate));
    }
}
