﻿using HelpDesk_3.Domain.Common;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class OpenSourcesCheck : ValueObject
    {
        internal OpenSourcesCheck() { }

        public bool? Posts { get; private set; }

        public bool? PictureSearch { get; private set; }

        public bool? GoogleCheck { get; private set; }

        public bool? HateGroups { get; private set; }

        public bool? AccessCard { get; private set; }

        public bool? InternalSecurity { get; private set; }

        public OpenSourcesCheck UpdatePosts(bool posts)
        {
            this.Posts = posts;
            return this;
        }

        public OpenSourcesCheck UpdatePictureSearch(bool pictureSearch)
        {
            this.PictureSearch = pictureSearch;
            return this;
        }

        public OpenSourcesCheck UpdateGoogleCheck(bool googleCheck)
        {
            this.GoogleCheck = googleCheck;
            return this;
        }

        public OpenSourcesCheck UpdateHateGroups(bool hateGroups)
        {
            this.HateGroups = hateGroups;
            return this;
        }

        public OpenSourcesCheck UpdateAccessCard(bool accessCard)
        {
            this.AccessCard = accessCard;
            return this;
        }

        public OpenSourcesCheck UpdateInternalSecurity(bool internalSecurity)
        {
            this.InternalSecurity = internalSecurity;
            return this;
        }
    }
}
