﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.OrganizationData;
using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class OrganizationData : ValueObject
    {
        internal OrganizationData() { }

        public string? Division { get; private set; } = default!;
        
        public string? Department { get; private set; } = default!;
        
        public string? Unit { get; private set; } = default!;
        
        public string? Location { get; private set; } = default!;
        
        public string? CostProfitArea { get; private set; } = default!;
        
        public string? CostCenter { get; private set; } = default!;
        
        public string? OfficialPosition { get; private set; } = default!;
        
        public string? InternalPosition { get; private set; } = default!;
        
        public string? DirectManagerAdminReporting { get; private set; } = default!;
        
        public string? DirectManagerFunctionalReporting { get; private set; } = default!;
        
        public BonusEligibility? Bonus { get; private set; }
        
        public string? AdditionalComments { get; private set; } = default!;
        
        public string? Photo { get; private set; } = default!;
        
        public string? CiriumVitae { get; private set; } = default!;

        public OrganizationData UpdateDivision(string division)
        {
            if (division != null)
            {
                this.ValidateDivision(division);
                this.Division = division;
            }

            return this;
        }

        public OrganizationData UpdateDepartment(string department)
        {
            if (department != null)
            {
                this.ValidateDepartment(department);
                this.Department = department;
            }

            return this;
        }

        public OrganizationData UpdateUnit(string unit)
        {
            if (unit != null)
            {
                this.ValidateUnit(unit);
                this.Unit = unit;
            }

            return this;
        }

        public OrganizationData UpdateLocation(string location)
        {
            if (location != null)
            {
                this.ValidateLocation(location);
                this.Location = location;
            }

            return this;
        }

        public OrganizationData UpdateCostProfitArea(string costProfitArea)
        {
            if (costProfitArea != null)
            {
                this.ValidateCostProfitArea(costProfitArea);
                this.CostProfitArea = costProfitArea;
            }

            return this;
        }

        public OrganizationData UpdateCostCenter(string costCenter)
        {
            if (costCenter != null)
            {
                this.ValidateCostCenter(costCenter);
                this.CostCenter = costCenter;
            }

            return this;
        }

        public OrganizationData UpdateOfficialPosition(string officialPosition)
        {
            if (officialPosition != null)
            {
                this.ValidateOfficialPosition(officialPosition);
                this.OfficialPosition = officialPosition;
            }

            return this;
        }

        public OrganizationData UpdateInternalPosition(string internalPosition)
        {
            if (internalPosition != null)
            {
                this.ValidateInternalPosition(internalPosition);
                this.InternalPosition = internalPosition;
            }

            return this;
        }

        public OrganizationData UpdateDirectManagerAdminReporting(string directManagerAdminReporting)
        {
            if (directManagerAdminReporting != null)
            {
                this.ValidateDirectManagerAdminReporting(directManagerAdminReporting);
                this.DirectManagerAdminReporting = directManagerAdminReporting;
            }

            return this;
        }

        public OrganizationData UpdateDirectManagerFunctionalReporting(string directManagerFunctionalReporting)
        {
            if (directManagerFunctionalReporting != null)
            {
                this.ValidateDirectManagerFunctionalReporting(directManagerFunctionalReporting);
                this.DirectManagerFunctionalReporting = directManagerFunctionalReporting;
            }

            return this;
        }

        public OrganizationData UpdateBonus(BonusEligibility bonus)
        {
            this.Bonus = bonus;
            return this;
        }

        public OrganizationData UpdateAdditionalComments(string additionalComments)
        {
            if (additionalComments != null)
            {
                this.ValidateAdditionalComments(additionalComments);
                this.AdditionalComments = additionalComments;
            }

            return this;
        }

        public OrganizationData UpdatePhoto(string photo)
        {
            if (photo != null)
            {
                this.ValidatePhoto(photo);
                this.Photo = photo;
            }

            return this;
        }

        public OrganizationData UpdateCiriumVitae(string ciriumVitae)
        {
            if (ciriumVitae != null)
            {
                this.ValidateCiriumVitae(ciriumVitae);
                this.CiriumVitae = ciriumVitae;
            }

            return this;
        }

        private void Validate(
            string division, 
            string department, 
            string unit, 
            string location, 
            string costProfitArea, 
            string costCenter, 
            string officialPosition, 
            string internalPosition, 
            string directManagerAdminReporting, 
            string directManagerFunctionalReporting, 
            string additionalComments, 
            string photo,
            string ciriumVitae)
        {
            ValidateDivision(division);
            ValidateDepartment(department);
            ValidateUnit(unit);
            ValidateLocation(location);
            ValidateCostProfitArea(costProfitArea);
            ValidateCostCenter(costCenter);
            ValidateOfficialPosition(officialPosition);
            ValidateInternalPosition(internalPosition);
            ValidateDirectManagerAdminReporting(directManagerAdminReporting);
            ValidateDirectManagerFunctionalReporting(directManagerFunctionalReporting);
            ValidateAdditionalComments(additionalComments);
            ValidatePhoto(photo);
            ValidateCiriumVitae(ciriumVitae);
        }

        private void ValidateDivision(string division)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                division,
                MinDivisionLength,
                MaxDivisionLength, 
                nameof(this.Division));

        private void ValidateDepartment(string department)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                department, 
                MinDepartmentLength, 
                MaxDepartmentLength, 
                nameof(this.Department));

        private void ValidateUnit(string unit)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                unit, 
                MinUnitLength, 
                MaxUnitLength, 
                nameof(this.Unit));

        private void ValidateLocation(string location)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                location, 
                MaxLocationLength, 
                MinLocationLength, 
                nameof(this.Location));

        private void ValidateCostProfitArea(string costProfitArea)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                costProfitArea, 
                MinCostProfitAreaLength, 
                MaxCostProfitAreaLength, 
                nameof(this.CostProfitArea));

        private void ValidateCostCenter(string costCenter)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                costCenter, 
                MinCostCenterLength, 
                MaxCostCenterLength, 
                nameof(this.CostCenter));

        private void ValidateOfficialPosition(string officialPosition)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                officialPosition, 
                MinOfficialPositionLength, 
                MaxOfficialPositionLength, 
                nameof(this.OfficialPosition));

        private void ValidateInternalPosition(string internalPosition)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                internalPosition, 
                MinInternalPositionLength, 
                MaxInternalPositionLength, 
                nameof(this.InternalPosition));

        private void ValidateDirectManagerAdminReporting(string directManagerAdminReporting)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                directManagerAdminReporting, 
                MinDirectManagerAdminReportingLength, 
                MaxDirectManagerAdminReportingLength, 
                nameof(this.DirectManagerAdminReporting));

        private void ValidateDirectManagerFunctionalReporting(string directManagerFunctionalReporting)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                directManagerFunctionalReporting, 
                MinDirectManagerFunctionalReportingLength, 
                MaxDirectManagerFunctionalReportingLength, 
                nameof(this.DirectManagerFunctionalReporting));

        private void ValidateAdditionalComments(string additionalComments)
            => Guard.ForStringLength<InvalidOrganizationDataException>(
                additionalComments, 
                MinCommentLength, 
                MaxCommentLength, 
                nameof(this.AdditionalComments));

        private void ValidatePhoto(string photo)
            => Guard.ForCorrectUri<InvalidOrganizationDataException>(
                photo, 
                nameof(this.Photo));

        private void ValidateCiriumVitae(string ciriumVitae)
            => Guard.ForCorrectUri<InvalidOrganizationDataException>(
                ciriumVitae, 
                nameof(this.CiriumVitae));
    }
}
