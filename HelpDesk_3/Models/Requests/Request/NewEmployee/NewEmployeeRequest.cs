﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class NewEmployeeRequest : RequestData
    {
        internal NewEmployeeRequest(
            string category,
            string categoryGroup,
            Status status,
            PersonalInformation personalInformation,
            ContactInformation contactInformation,
            EmployeeType employeeType,
            PlaceOfWork placeOfWork)
            : base (category, categoryGroup, status)
        {
            this.PersonalInformation = personalInformation;
            this.EmployeeType = employeeType;
            this.PlaceOfWork = placeOfWork;
            this.ContactInformation = contactInformation;

            this.Address = new Address();
            this.StartDocuments = new StartDocuments();
            this.OrganizationData = new OrganizationData();
            this.FraudCheck = new FraudCheck();
            this.OpenSourcesCheck = new OpenSourcesCheck();
            this.EquipmentAndAccess = new EquipmentAndAccess();
            this.RequestAccessToSystems = new RequestAccessToSystems();
            this.AccessToSystemsData = new AccessToSystemsData();
            this.StartDocumentsSigned = new StartDocumentsSigned();
        }

        private NewEmployeeRequest(
            string category,
            string categoryGroup,
            Status status)
            : base(category, categoryGroup, status)
        {

        }

        private NewEmployeeRequest(
            string category,
            string categoryGroup,
            Status status,
            EmployeeType employeeType,
            PlaceOfWork placeOfWork,
            int probationPeriod,
            int noticePeriod,
            bool orgAnnouncement,
            Gender gender,
            DateTime dateOfBirth,
            EmploymentHistory employmentHistory,
            string internalSecurityComment,
            string iBAN,
            bool allPaymentDocumentsRecieved)
            : base(category, categoryGroup, status)
        {
            this.EmployeeType = employeeType;
            this.PlaceOfWork = placeOfWork;
            this.ProbationPeriod = probationPeriod;
            this.NoticePeriod = noticePeriod;
            this.OrgAnnouncement = orgAnnouncement;
            this.Gender = gender;
            this.DateOfBirth = dateOfBirth;
            this.EmploymentHistory = employmentHistory;
            this.InternalSecurityComment = internalSecurityComment;
            this.IBAN = iBAN;
            this.AllPaymentDocumentsRecieved = allPaymentDocumentsRecieved;

            this.PersonalInformation = null!;
            this.ContactInformation = null!;
            this.Address = null!;
            this.StartDocuments = null!;
            this.OrganizationData = null!;
            this.FraudCheck = null!;
            this.OpenSourcesCheck = null!;
            this.EquipmentAndAccess = null!;
            this.RequestAccessToSystems = null!;
            this.AccessToSystemsData = null!;
            this.StartDocumentsSigned = null!;
        }

        public PersonalInformation PersonalInformation { get; private set; } = default!;
        public ContactInformation ContactInformation { get; private set; } = default!;
        public EmployeeType EmployeeType { get; private set; } = default!;
        public PlaceOfWork PlaceOfWork { get; private set; } = default!;
        public int? ProbationPeriod { get; private set; }
        public int? NoticePeriod { get; private set; }
        public bool? OrgAnnouncement { get; private set; }
        public Gender? Gender { get; private set; }
        public DateTime? DateOfBirth { get; private set; }
        public EmploymentHistory? EmploymentHistory { get; private set; }
        public Address Address { get; private set; } = default!;
        public StartDocuments StartDocuments { get; private set; } = default!;
        public OrganizationData OrganizationData { get; private set; } = default!;
        public FraudCheck FraudCheck { get; private set; } = default!;
        public OpenSourcesCheck OpenSourcesCheck { get; private set; } = default!;
        public string? InternalSecurityComment { get; private set; } = default!;
        public EquipmentAndAccess EquipmentAndAccess { get; private set; } = default!;
        public RequestAccessToSystems RequestAccessToSystems { get; private set; } = default!;
        public AccessToSystemsData AccessToSystemsData { get; private set; } = default!;
        public string? IBAN { get; private set; } = default!;
        public bool? AllPaymentDocumentsRecieved { get; private set; }
        public StartDocumentsSigned StartDocumentsSigned { get; private set; } = default!;

        public NewEmployeeRequest UpdatePersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber)
        {
            this.PersonalInformation
                .UpdateFirstName(firstName)
                .UpdateMiddleName(middleName)
                .UpdateLastName(lastName)
                .UpdateIdentificationNumber(identificationNumber);

            return this;
        }

        public NewEmployeeRequest UpdateContactInformation(
            string phoneNumber,
            string email)
        {
            this.ContactInformation
                .UpdatePhoneNumber(phoneNumber)
                .UpdateEmail(email);

            return this;
        }

        public NewEmployeeRequest UpdateEmployeeType(EmployeeType employeeType)
        {
            this.EmployeeType = employeeType;
            return this;
        }

        public NewEmployeeRequest UpdatePlaceOfWork(PlaceOfWork placeOfWork)
        {
            this.PlaceOfWork = placeOfWork;
            return this;
        }

        public NewEmployeeRequest UpdateProbationPeriod(int probationPeriod) 
        {
            this.ValidateProbationPeriod(probationPeriod);
            this.ProbationPeriod = probationPeriod;
            return this;
        }

        public NewEmployeeRequest UpdateNoticePeriod(int noticePeriod)
        {
            this.ValidateNoticePeriod(noticePeriod);
            this.NoticePeriod = noticePeriod;
            return this;
        }

        public NewEmployeeRequest UpdateOrgAnnouncement(bool orgAnnouncement)
        {
            this.OrgAnnouncement = orgAnnouncement;
            return this;
        }

        public NewEmployeeRequest UpdateGender(Gender gender)
        {
            this.Gender = gender;
            return this;
        }

        public NewEmployeeRequest UpdateDateOfBirth(DateTime dateOfBirth)
        {
            if (dateOfBirth != DateTime.MinValue.AddDays(1))
            {
                this.ValidateDateOfBirth(dateOfBirth);
                this.DateOfBirth = dateOfBirth;
            }

            return this;
        }

        public NewEmployeeRequest UpdateEmploymentHistory(EmploymentHistory employmentHistory)
        {
            this.EmploymentHistory = employmentHistory;
            return this;
        }

        public NewEmployeeRequest UpdateAddress(
            string country,
            string city,
            string street,
            int streetNumber,
            int appartment,
            int unit,
            int floor)
        {
            this.Address
                .UpdateCountry(country)
                .UpdateCity(city)
                .UpdateStreet(street)
                .UpdateStreetNumber(streetNumber)
                .UpdateAppartment(appartment)
                .UpdateUnit(unit)
                .UpdateFloor(floor);

            return this;
        }

        public NewEmployeeRequest UpdateStartDocuments(
            ReportingLevel reportingLevel,
            bool probationPeriodValidation,
            bool declarationGDPR,
            bool labourBook,
            bool medicalCertificate,
            bool diploma,
            bool signedJobOffer,
            bool jobDescription)
        {
            this.StartDocuments
                .UpdateReportingLevel(reportingLevel)
                .UpdateProbationPeriodValidation(probationPeriodValidation)
                .UpdateDeclarationGDPR(declarationGDPR)
                .UpdateLabourBook(labourBook)
                .UpdateMedicalCertificate(medicalCertificate)
                .UpdateDiploma(diploma)
                .UpdateSignedJobOffer(signedJobOffer)
                .UpdateJobDescription(jobDescription);

            return this;
        }

        public NewEmployeeRequest UpdateOrganizationData(
            string division,
            string department,
            string unit,
            string location,
            string costProfitArea,
            string costCenter,
            string officialPosition,
            string internalPosition,
            string directManagerAdminReporting,
            string directManagerFunctionalReporting,
            BonusEligibility bonus,
            string additionalComments,
            string photo,
            string ciriumVitae)
        {
            this.OrganizationData
                .UpdateDivision(division)
                .UpdateDepartment(department)
                .UpdateUnit(unit)
                .UpdateLocation(location)
                .UpdateCostProfitArea(costProfitArea)
                .UpdateCostCenter(costCenter)
                .UpdateOfficialPosition(officialPosition)
                .UpdateInternalPosition(internalPosition)
                .UpdateDirectManagerAdminReporting(directManagerAdminReporting)
                .UpdateDirectManagerFunctionalReporting(directManagerFunctionalReporting)
                .UpdateBonus(bonus)
                .UpdateAdditionalComments(additionalComments)
                .UpdateAdditionalComments(additionalComments)
                .UpdatePhoto(photo)
                .UpdateCiriumVitae(ciriumVitae);

            return this;
        }

        public NewEmployeeRequest UpdateFraudCheck(
            bool criminalRecord,
            CentralCreditRecord centralCreditRecord,
            bool blackList,
            bool validateInformation,
            string smartInfo)
        {
            this.FraudCheck
                .UpdateCriminalRecord(criminalRecord)
                .UpdateCriminalRecord(centralCreditRecord)
                .UpdateBlackList(blackList)
                .UpdateValidateInformation(validateInformation)
                .UpdateSmartInfo(smartInfo);

            return this;
        }

        public NewEmployeeRequest UpdateOpenSourcesCheck(
            bool posts,
            bool pictureSearch,
            bool googleCheck,
            bool hateGroups,
            bool accessCard,
            bool internalSecurity)
        {
            this.OpenSourcesCheck
                .UpdatePosts(posts)
                .UpdatePictureSearch(pictureSearch)
                .UpdateGoogleCheck(googleCheck)
                .UpdateHateGroups(hateGroups)
                .UpdateAccessCard(accessCard)
                .UpdateInternalSecurity(internalSecurity);


            return this;
        }

        public NewEmployeeRequest UpdateInternalSecurityComment(string internalSecurityComment)
        {
            if (internalSecurityComment != null)
            {
                this.ValidateInternalSecurityComment(internalSecurityComment);
                this.InternalSecurityComment = internalSecurityComment;
            }

            return this;
        }

        public NewEmployeeRequest UpdateEquipmentAndAccess(
            MobilePhone mobilePhone,
            bool uniform,
            bool companyCar,
            PrefferedEquipment prefferedEquipment,
            bool vPN,
            string additionalEquipment,
            WorkingPlace workingPlace,
            bool stationaryPhone,
            string workingPlaceLocation,
            string leavingEmployee)
        {
            this.EquipmentAndAccess
                .UpdateMobilePhone(mobilePhone)
                .UpdateUniform(uniform)
                .UpdateCompanyCar(companyCar)
                .UpdatePrefferedEquipment(prefferedEquipment)
                .UpdateVPN(vPN)
                .UpdateAdditionalEquipment(additionalEquipment)
                .UpdateWorkingPlace(workingPlace)
                .UpdateStationaryPhone(stationaryPhone)
                .UpdateWorkingPlaceLocation(workingPlaceLocation)
                .UpdateLeavingEmployee(leavingEmployee);

            return this;
        }

        public NewEmployeeRequest UpdateRequestAccessToSystems(
            bool operationalRisk,
            bool smartInfo,
            string smartInfoRoles,
            bool sCard,
            string sCardRoles,
            bool fos,
            string fosRoles,
            bool ceGate,
            bool iCollect,
            string iCollectRoles,
            bool syron,
            string syronRoles,
            string accessLikeUser,
            bool vcs,
            string vcsRoles,
            PhoneRecieving phoneRecievingVCS,
            bool customerView,
            string accessToOtherSystems,
            bool accessToEmailByMobile,
            string emailGroups,
            string sharedFolders)
        {
            this.RequestAccessToSystems
                .UpdateOperationalRisk(operationalRisk)
                .UpdateSmartInfo(smartInfo)
                .UpdateSmartInfoRoles(smartInfoRoles)
                .UpdateSCard(sCard)
                .UpdateSCardRoles(sCardRoles)
                .UpdateFOS(fos)
                .UpdateFOSRoles(fosRoles)
                .UpdateICollect(iCollect)
                .UpdateICollectRoles(iCollectRoles)
                .UpdateSyron(syron)
                .UpdateSyronRoles(syronRoles)
                .UpdateAccessLikeUser(accessLikeUser)
                .UpdateVCS(vcs)
                .UpdateVCSRoles(vcsRoles)
                .UpdatePhoneRecievingVCS(phoneRecievingVCS)
                .UpdateCustomerView(customerView)
                .UpdateAccessToOtherSystems(accessToOtherSystems)
                .UpdateAccessToEmailByMobile(accessToEmailByMobile)
                .UpdateEmailGroups(emailGroups)
                .UpdateSharedFolders(sharedFolders);

            return this;
        }

        public NewEmployeeRequest UpdateAccessToSystemsData(
            string adUser,
            string adPass,
            string vcsUser,
            string vcsPass,
            string smartInfoUser,
            string smartInfoPass,
            string sCardUser,
            string sCardPass,
            string fosUser,
            string fosPass,
            string syronUser,
            string syronPass,
            string iCollectUser,
            string iCollectPass,
            string userEmail,
            bool customerView,
            YesNoNotApproved equipmentOnFirstDay,
            bool additionalEquipmentApproved,
            bool accessToSharedFolders,
            bool addedToGroupEmails,
            string updateAdditionalAccessesRequested)
        {
            this.AccessToSystemsData
                .UpdateADUser(adUser)
                .UpdateADPass(adPass)
                .UpdateVCSUser(vcsUser)
                .UpdateVCSPass(vcsPass)
                .UpdateSmartInfoUser(smartInfoUser)
                .UpdateSmartInfoPass(smartInfoPass)
                .UpdateSCardUser(sCardUser)
                .UpdateSCardPass(sCardPass)
                .UpdateFOSUser(fosUser)
                .UpdateFOSPass(fosPass)
                .UpdateSyronUser(syronUser)
                .UpdateSyronPass(syronPass)
                .UpdateICollectUser(iCollectUser)
                .UpdateICollectPass(iCollectPass)
                .UpdateUserEmail(userEmail)
                .UpdateCustomerView(customerView)
                .UpdateEquipmentOnFirstDay(equipmentOnFirstDay)
                .UpdateAdditionalEquipmentApproved(additionalEquipmentApproved)
                .UpdateAccessToSharedFolders(accessToSharedFolders)
                .UpdateAddedToGroupEmails(addedToGroupEmails)
                .UpdateAdditionalAccessesRequested(updateAdditionalAccessesRequested);
                
            return this;
        }

        public NewEmployeeRequest UpdateIBAN(string iban)
        {
            if (iban != null) 
            { 
                this.ValidateIBAN(iban);
                this.IBAN = iban;
            }

            return this;
        }

        public NewEmployeeRequest UpdateAllPaymentDocumentsRecieved(bool allPaymentDocumentsRecieved)
        {
            this.AllPaymentDocumentsRecieved = allPaymentDocumentsRecieved;
            return this;
        }

        public NewEmployeeRequest UpdateStartDocumentsSigned(
            bool signedJobDescription,
            bool signedLabourContract,
            bool pccCheck,
            DateTime pccCheckDate,
            bool instructionNoteHS,
            bool nra,
            bool videoSurveillance,
            bool gdpr,
            bool gdprPhotosVideo) 
        {
            this.StartDocumentsSigned
                .UpdateSignedJobDescription(signedJobDescription)
                .UpdateSignedLabourContract(signedLabourContract)
                .UpdatePCCCheck(pccCheck)
                .UpdatePCCCheckDate(pccCheckDate)
                .UpdateInstructionNoteHS(instructionNoteHS)
                .UpdateNRA(nra)
                .UpdateVideoSurveillance(videoSurveillance)
                .UpdateGDPR(gdpr)
                .UpdateGDPRPhotosVideo(gdprPhotosVideo);

            return this;
        }

        private void ValidateProbationPeriod(int probationPeriod)
            => Guard.AgainstNegativeNumber<InvalidRequestException>(
                probationPeriod, 
                nameof(this.ProbationPeriod));

        private void ValidateNoticePeriod(int noticePeriod)
            => Guard.AgainstNegativeNumber<InvalidRequestException>(
                noticePeriod, 
                nameof(this.NoticePeriod));

        private void ValidateDateOfBirth(DateTime dateOfBirth)
        {
            if (dateOfBirth.AddYears(18) >= DateTime.Today)
            {
                throw new InvalidRequestException("Employee must be above 18 years of age.");
            }
        }

        private void ValidateInternalSecurityComment(string internalSecurityComment)
            => Guard.ForStringLength<InvalidRequestException>(
                internalSecurityComment, 
                MinCommentLength, 
                MaxCommentLength, 
                nameof(this.InternalSecurityComment));

        private void ValidateIBAN(string iban)
            => Guard.ForExactStringLength<InvalidRequestException>(
                iban,
                IBANLength,
                nameof(this.IBAN));
    }
}
