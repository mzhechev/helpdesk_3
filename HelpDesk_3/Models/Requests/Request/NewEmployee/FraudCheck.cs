﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.FraudCheck;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class FraudCheck : ValueObject
    {
        internal FraudCheck() { }

        public bool? CriminalRecord { get; private set; } = default!;

        public CentralCreditRecord? CentralCreditRecord { get; private set; } = default!;

        public bool? BlackList { get; private set; } = default!;

        public bool? ValidateInformation { get; private set; } = default!;

        public string? SmartInfo { get; private set; } = default!;

        public FraudCheck UpdateCriminalRecord(bool criminalRecord)
        {
            this.CriminalRecord = criminalRecord;
            return this;
        }

        public FraudCheck UpdateCriminalRecord(CentralCreditRecord centralCreditRecord)
        {
            this.CentralCreditRecord = centralCreditRecord;
            return this;
        }

        public FraudCheck UpdateBlackList(bool blackList)
        {
            this.BlackList = blackList;
            return this;
        }

        public FraudCheck UpdateValidateInformation(bool validateInformation)
        {
            this.ValidateInformation = validateInformation;
            return this;
        }

        public FraudCheck UpdateSmartInfo(string smartInfo)
        {
            if (SmartInfo != null)
            {
                this.ValidateSmartInfo(smartInfo);
                this.SmartInfo = smartInfo;
            }

            return this;
        }

        private void ValidateSmartInfo(string smartInfo)
            => Guard.ForStringLength<InvalidFraudCheckException>(
                smartInfo, 
                MinSmartInfoLength, 
                MaxSmartInfoLength, 
                nameof(this.SmartInfo));
    }
}
