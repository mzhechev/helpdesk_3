﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Models.Enums;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class StartDocuments : ValueObject
    {
        internal StartDocuments() { }

        public ReportingLevel? ReportingLevel { get; set; }

        public bool? ProbationPeriodValidation { get; private set; }

        public bool? DeclarationGDPR { get; private set; }

        public bool? LabourBook { get; private set; }

        public bool? MedicalCertificate { get; private set; }

        public bool? Diploma { get; private set; }

        public bool? SignedJobOffer { get; private set; }

        public bool? JobDescription { get; private set; }

        public StartDocuments UpdateReportingLevel(ReportingLevel reportingLevel)
        {
            this.ReportingLevel = reportingLevel;
            return this;
        }

        public StartDocuments UpdateProbationPeriodValidation(bool probationPeriodValidation) 
        {
            this.ProbationPeriodValidation = probationPeriodValidation;
            return this;
        }

        public StartDocuments UpdateDeclarationGDPR(bool declarationGDPR)
        {
            this.DeclarationGDPR = declarationGDPR;
            return this;
        }

        public StartDocuments UpdateLabourBook(bool labourBook)
        {
            this.LabourBook = labourBook;
            return this;
        }

        public StartDocuments UpdateMedicalCertificate(bool medicalCertificate)
        {
            this.MedicalCertificate = medicalCertificate;
            return this;
        }

        public StartDocuments UpdateDiploma(bool diploma)
        {
            this.Diploma = diploma;
            return this;
        }

        public StartDocuments UpdateSignedJobOffer(bool signedJobOffer)
        {
            this.SignedJobOffer = signedJobOffer;
            return this;
        }

        public StartDocuments UpdateJobDescription(bool jobDescription)
        {
            this.JobDescription = jobDescription;
            return this;
        }
    }
}
