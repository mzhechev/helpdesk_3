﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;
using static HelpDesk_3.Domain.Models.ModelConstants.AccessToSystemsData;

namespace HelpDesk_3.Domain.Models.Requests.Request.NewEmployee
{
    public class AccessToSystemsData : ValueObject
    {
        internal AccessToSystemsData() { }

        public string? ADUser { get; private set; } = default!;
        public string? ADPass { get; private set; } = default!;
        public string? VCSUser { get; private set; } = default!;
        public string? VCSPass { get; private set; } = default!;
        public string? SmartInfoUser { get; private set; } = default!;
        public string? SmartInfoPass { get; private set; } = default!;
        public string? SCardUser { get; private set; } = default!;
        public string? SCardPass { get; private set; } = default!;
        public string? FOSUser { get; private set; } = default!;
        public string? FOSPass { get; private set; } = default!;
        public string? SyronUser { get; private set; } = default!;
        public string? SyronPass { get; private set; } = default!;
        public string? ICollectUser { get; private set; } = default!;
        public string? ICollectPass { get; private set; } = default!;
        public string? UserEmail { get; private set; } = default!;
        public bool? CustomerView { get; private set; } = default!;
        public YesNoNotApproved? EquipmentOnFirstDay { get; private set; } = default!;
        public bool? AdditionalEquipmentApproved { get; private set; } = default!;
        public bool? AccessToSharedFolders { get; private set; } = default!;
        public bool? AddedToGroupEmails { get; private set; } = default!;
        public string? _UpdateAdditionalAccessesRequested { get; private set; } = default!;

        public AccessToSystemsData UpdateADUser(string adUser)
        {
            if (adUser != null)
            {
                this.ValidateUser(adUser);
                this.ADUser = adUser;
            }

            return this;
        }

        public AccessToSystemsData UpdateADPass(string adPass)
        {
            if (adPass != null)
            {
                this.ValidatePass(adPass);
                this.ADPass = adPass;
            }

            return this;
        }

        public AccessToSystemsData UpdateVCSUser(string vcsUser)
        {
            if (vcsUser != null)
            {
                this.ValidateUser(vcsUser);
                this.VCSUser = vcsUser;
            }

            return this;
        }

        public AccessToSystemsData UpdateVCSPass(string vcsPass)
        {
            if (vcsPass != null)
            {
                this.ValidatePass(vcsPass);
                this.VCSPass = vcsPass;
            }

            return this;
        }

        public AccessToSystemsData UpdateSmartInfoUser(string smartInfoUser)
        {
            if (smartInfoUser != null)
            {
                this.ValidateUser(smartInfoUser);
                this.SmartInfoUser = smartInfoUser;
            }

            return this;
        }

        public AccessToSystemsData UpdateSmartInfoPass(string smartInfoPass)
        {
            if (smartInfoPass != null)
            {
                this.ValidatePass(smartInfoPass);
                this.SmartInfoPass = smartInfoPass;
            }

            return this;
        }

        public AccessToSystemsData UpdateSCardUser(string sCardUser)
        {
            if (sCardUser != null)
            {
                this.ValidateUser(sCardUser);
                this.SCardUser = sCardUser;
            }

            return this;
        }

        public AccessToSystemsData UpdateSCardPass(string sCardPass)
        {
            if (sCardPass != null)
            {
                this.ValidatePass(sCardPass);
                this.SCardPass = sCardPass;
            }

            return this;
        }

        public AccessToSystemsData UpdateFOSUser(string fosUser)
        {
            if (fosUser != null)
            {
                this.ValidateUser(fosUser);
                this.FOSUser = fosUser;
            }

            return this;
        }

        public AccessToSystemsData UpdateFOSPass(string fosPass)
        {
            if (fosPass != null)
            {
                this.ValidatePass(fosPass);
                this.FOSPass = fosPass;
            }

            return this;
        }

        public AccessToSystemsData UpdateSyronUser(string syronUser)
        {
            if (syronUser != null)
            {
                this.ValidateUser(syronUser);
                this.SyronUser = syronUser;
            }

            return this;
        }

        public AccessToSystemsData UpdateSyronPass(string syronPass)
        {
            if (syronPass != null)
            {
                this.ValidatePass(syronPass);
                this.SyronPass = syronPass;
            }

            return this;
        }

        public AccessToSystemsData UpdateICollectUser(string iCollectUser)
        {
            if (iCollectUser != null)
            {
                this.ValidateUser(iCollectUser);
                this.ICollectUser = iCollectUser;
            }
            return this;
        }

        public AccessToSystemsData UpdateICollectPass(string iCollectPass)
        {
            if (iCollectPass != null)
            {
                this.ValidatePass(iCollectPass);
                this.ICollectPass = iCollectPass;
            }
            return this;
        }

        public AccessToSystemsData UpdateUserEmail(string userEmail)
        {
            if (userEmail != null)
            {
                this.ValidateUserEmail(userEmail);
                this.UserEmail = userEmail;
            }

            return this;
        }

        public AccessToSystemsData UpdateCustomerView(bool customerView)
        {
            this.CustomerView = customerView;
            return this;
        }

        public AccessToSystemsData UpdateEquipmentOnFirstDay(YesNoNotApproved equipmentOnFirstDay)
        {
            this.EquipmentOnFirstDay = equipmentOnFirstDay;
            return this;
        }

        public AccessToSystemsData UpdateAdditionalEquipmentApproved(bool additionalEquipmentApproved)
        {
            this.AdditionalEquipmentApproved = additionalEquipmentApproved;
            return this;
        }
        
        public AccessToSystemsData UpdateAccessToSharedFolders(bool accessToSharedFolders)
        {
            this.AccessToSharedFolders = accessToSharedFolders;
            return this;
        }

        public AccessToSystemsData UpdateAddedToGroupEmails(bool addedToGroupEmails)
        {
            this.AddedToGroupEmails = addedToGroupEmails;
            return this;
        }

        public AccessToSystemsData UpdateAdditionalAccessesRequested(string updateAdditionalAccessesRequested)
        {
            if (updateAdditionalAccessesRequested != null)
            {
                this.ValidateUpdateAdditionalAcessesRequested(updateAdditionalAccessesRequested);
                this._UpdateAdditionalAccessesRequested = updateAdditionalAccessesRequested;
            }

            return this;
        }

        private void Validate(
            string userEmail,
            string updateAdditionalAccessesRequested)
        {
            this.ValidateUserEmail(userEmail);
            this.ValidateUpdateAdditionalAcessesRequested(updateAdditionalAccessesRequested);
        }

        private void ValidatePass(string password)
            => Guard.ForCorrectPassword<InvalidAccessToSystemsDataException>(
                password);

        private void ValidateUser(string username)
            => Guard.ForStringLength<InvalidAccessToSystemsDataException>(
                username,
                MinUsernameLength,
                MaxUsernameLength,
                "Username");

        private void ValidateUserEmail(string userEmail)
            => Guard.ForCorrectEmail<InvalidEmailException>(
                userEmail, 
                nameof(this.UserEmail));

        private void ValidateUpdateAdditionalAcessesRequested(string updateAdditionalAccessesRequested)
            => Guard.ForStringLength<InvalidRequestException>(
                updateAdditionalAccessesRequested, 
                MinUpdateAdditionalAccessesRequestedLength, 
                MaxUpdateAdditionalAccessesRequestedLength, 
                nameof(this.UpdateAdditionalAccessesRequested));
    }
}
