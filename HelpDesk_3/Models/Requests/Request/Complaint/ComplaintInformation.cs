﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;
using static HelpDesk_3.Domain.Models.ModelConstants.ComplaintInformation;

namespace HelpDesk_3.Domain.Models.Requests
{
    public class ComplaintInformation : ValueObject
    {
        internal ComplaintInformation() {}

        public string? SourceOfComplaint { get; private set; } = default!;

        public string? MethodOfReceivingComplaint { get; private set; } = default!;

        public string? SubjectOfComplaint { get; private set; } = default!;

        public string? DetailsOfComplaint { get; private set; } = default!;

        public ComplaintInformation UpdateSourceOfComplaint(string sourceOfComplaint)
        {
            if (sourceOfComplaint != null)
            {
                this.ValidateSourceOfComplaint(sourceOfComplaint);
                this.SourceOfComplaint = sourceOfComplaint;
            }

            return this;
        }

        public ComplaintInformation UpdateMethodOfReceivingComplaint(string methodOfRecievingComplaint)
        {
            if (methodOfRecievingComplaint != null)
            {
                this.ValidateMethodOfReceivingComplaint(methodOfRecievingComplaint);
                this.MethodOfReceivingComplaint = methodOfRecievingComplaint;
            }

            return this;
        }

        public ComplaintInformation UpdateSubjectOfComplaint(string subjectOfComplaint)
        {
            if (subjectOfComplaint != null)
            {
                this.ValidateSubjectOfComplaint(subjectOfComplaint);
                this.SubjectOfComplaint = subjectOfComplaint;
            }

            return this;
        }

        public ComplaintInformation UpdateDetailsOfComplaint(string detailsOfComplaint)
        {
            if (detailsOfComplaint != null)
            {
                this.ValidateDetailsOfComplaint(detailsOfComplaint);
                this.DetailsOfComplaint = detailsOfComplaint;
            }

            return this;
        }

        private void Validate(
            string sourceOfComplaint, 
            string methodOfReceivingComplaint, 
            string subjectOfComplaint, 
            string detailsOfComplaint)
        {
            ValidateSourceOfComplaint(sourceOfComplaint);
            ValidateMethodOfReceivingComplaint(methodOfReceivingComplaint);
            ValidateSubjectOfComplaint(subjectOfComplaint);
            ValidateDetailsOfComplaint(detailsOfComplaint);
        }

        private void ValidateSourceOfComplaint(string sourceOfComplaint)
            => Guard.ForStringLength<InvalidComplaintInformationException>(
                sourceOfComplaint,
                MinComplaintInformationLength,
                MaxComplaintInformationLength,
                nameof(this.SourceOfComplaint));

        private void ValidateMethodOfReceivingComplaint(string methodOfReceivingComplaint)
            => Guard.ForStringLength<InvalidComplaintInformationException>(
                methodOfReceivingComplaint,
                MinComplaintInformationLength,
                MaxComplaintInformationLength,
                nameof(this.MethodOfReceivingComplaint));

        private void ValidateSubjectOfComplaint(string subjectOfComplaint)
            => Guard.ForStringLength<InvalidComplaintInformationException>(
                subjectOfComplaint,
                MinComplaintInformationLength,
                MaxComplaintInformationLength,
                nameof(this.SubjectOfComplaint));

        private void ValidateDetailsOfComplaint(string detailsOfComplaint)
            => Guard.ForStringLength<InvalidComplaintInformationException>(
                detailsOfComplaint,
                MinDescriptionLength,
                MaxDescriptionLength,
                nameof(this.DetailsOfComplaint));
    }
}
