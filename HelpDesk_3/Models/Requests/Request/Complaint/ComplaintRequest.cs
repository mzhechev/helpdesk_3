﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Customers;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;
using static HelpDesk_3.Domain.Models.ModelConstants.ComplaintRequest;

namespace HelpDesk_3.Domain.Models.Requests.Request.Complaint
{
    public class ComplaintRequest : RequestData
    {
        internal ComplaintRequest(
            string category,
            string categoryGroup,
            Status status,
            PersonalInformation personalInformation,
            Customer customer,
            ContactInformation contactInformation,
            string content,
            DateTime complaintDate)
            :base (category, categoryGroup, status)
            {
                Validate(complaintDate, content);

                this.PersonalInformation = personalInformation;
                this.Customer = customer;
                this.ContactInformation = contactInformation;
                this.Content = content;
                this.ComplaintDate = complaintDate;

                this.ComplaintInformation = new ComplaintInformation();
            }
        
        private ComplaintRequest(
            string category,
            string categoryGroup,
            Status status)
            :base (category, categoryGroup, status)
        {

        }

        private ComplaintRequest(
            string category,
            string categoryGroup,
            Status status,
            string contractNumber,
            DateTime contractIssuingDate,
            ContractStatus contractStatus,
            TypeOfLoan typeOfLoan,
            string merchant,
            string notificationType,
            string content,
            DateTime complaintDate,
            DateTime responseDate,
            string communicationHistory,
            string numberOfClientContracts,
            string nameOfClientContracts,
            bool returnToPreviousClaim,
            string solvedInFavor,
            bool pVCC,
            string amountPVCCRON,
            string registrationNumber,
            int responseDays,
            string remarks)
            : base(category, categoryGroup, status)
        {
            this.ContractNumber = contractNumber;
            this.ContractIssuingDate = contractIssuingDate;
            this.ContractStatus = contractStatus;
            this.TypeOfLoan = typeOfLoan;
            this.Merchant = merchant;
            this.NotificationType = notificationType;
            this.Content = content;
            this.ComplaintDate = complaintDate;
            this.ResponseDate = responseDate;
            this.CommunicationHistory = communicationHistory;
            this.NumberOfClientContracts = numberOfClientContracts;
            this.NameOfClientContracts = nameOfClientContracts;
            this.ReturnToPreviousClaim = returnToPreviousClaim;
            this.SolvedInFavor = solvedInFavor;
            this.PVCC = pVCC;
            this.AmountPVCCRON = amountPVCCRON;
            this.RegistrationNumber = registrationNumber;
            this.ResponseDays = responseDays;
            this.Remarks = remarks;

            this.PersonalInformation = null!;
            this.Customer = null!;
            this.ContactInformation = null!;
            this.ComplaintInformation = null!;
        }

        public PersonalInformation PersonalInformation { get; set; } = default!;
        public string? ContractNumber { get; private set; }
        public Customer Customer { get; private set; } = default!;
        public ContactInformation ContactInformation { get; private set; } = default!;
        public DateTime? ContractIssuingDate { get; private set; }
        public ContractStatus? ContractStatus { get; private set; }
        public TypeOfLoan? TypeOfLoan { get; private set; }
        public string? Merchant { get; private set; }
        public string? NotificationType { get; private set; } //TODO should be enum?
        public string Content { get; set; } = default!;
        public DateTime ComplaintDate { get; private set; } = default!;
        public DateTime? ResponseDate { get; private set; } = default!;
        public ComplaintInformation ComplaintInformation { get; private set; } = default!;
        public string? CommunicationHistory { get; private set; }
        public string? NumberOfClientContracts { get; private set; }
        public string? NameOfClientContracts { get; private set; }
        public bool? ReturnToPreviousClaim { get; private set; }
        public string? SolvedInFavor { get; private set; }
        public bool? PVCC { get; private set; }
        public string? AmountPVCCRON { get; private set; }
        public string? RegistrationNumber { get; private set; }
        public int? ResponseDays { get; private set; }
        public string? Remarks { get; private set; }

        public ComplaintRequest UpdatePersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber)
        {
            this.PersonalInformation
                .UpdateFirstName(firstName)
                .UpdateMiddleName(middleName)
                .UpdateLastName(lastName)
                .UpdateIdentificationNumber(identificationNumber);

            return this;
        }

        public ComplaintRequest UpdateContractNumber(string contractNumber) 
        {
            if (contractNumber != null)
            {
                this.ValidateContractNumber(contractNumber);
                this.ContractNumber = contractNumber;
            }

            return this;
        }

        public ComplaintRequest UpdateCustomer(Customer customer)
        {
            if (customer != null)
            {
                this.Customer = customer;
            }

            return this;
        } 

        public ComplaintRequest UpdateContactInformation(
            string phoneNumber,
            string email)
        {
            this.ContactInformation
                .UpdatePhoneNumber(phoneNumber)
                .UpdateEmail(email);
            
            return this;
        } 

        public ComplaintRequest UpdateContractIssuingDate(DateTime contractIssuingDate)
        {
            this.ValidateContractIssuingDate(contractIssuingDate);
            this.ContractIssuingDate = contractIssuingDate;

            return this;
        } 

        public ComplaintRequest UpdateContractStatus(ContractStatus contractStatus)
        {
            this.ContractStatus = contractStatus;
            return this;
        } 

        public ComplaintRequest UpdateTypeOfLoan(TypeOfLoan typeOfLoan)
        {
            this.TypeOfLoan = typeOfLoan;
            return this;
        } 

        public ComplaintRequest UpdateMerchant(string merchant)
        {
            if (merchant != null)
            {
                this.ValidateMerchant(merchant);
                this.Merchant = merchant;
            }

            return this;
        }

        public ComplaintRequest UpdateNotificationType(string notificationType)
        {
            if (notificationType != null)
            {
                this.ValidateNotificationType(notificationType);
                this.NotificationType = notificationType;
            }

            return this;
        }

        public ComplaintRequest UpdateContent(string content)
        {
            if (content != null)
            {
                this.ValidateContent(content);
                this.Content = content;
            }

            return this;
        }
        
        public ComplaintRequest UpdateComplaintDate(DateTime complaintDate)
        {
            if (complaintDate != DateTime.MinValue.AddDays(1))
            {
                this.ValidateComplaintDate(complaintDate);
                this.ComplaintDate = complaintDate;
            }

            return this;
        } 

        public ComplaintRequest UpdateResponseDate(DateTime responseDate)
        {
            if (responseDate != DateTime.MinValue)
            {
                this.ValidateResponseDate(responseDate);
                this.ResponseDate = responseDate;
            }

            return this;
        } 

        public ComplaintRequest UpdateComplaintInformation(
            string sourceOfComplaint,
            string methodOfReceivingComplaint,
            string subjectOfComplaint,
            string detailsOfComplaint)
        {
            this.ComplaintInformation
                .UpdateDetailsOfComplaint(sourceOfComplaint)
                .UpdateMethodOfReceivingComplaint(methodOfReceivingComplaint)
                .UpdateSubjectOfComplaint(subjectOfComplaint)
                .UpdateDetailsOfComplaint(detailsOfComplaint);

            return this;
        } 

        public ComplaintRequest UpdateCommunicationHistory(string communicationHistory)
        {
            if (communicationHistory != null)
            {
                this.ValidateCommunicationHistory(communicationHistory);
                this.CommunicationHistory = communicationHistory;
            }

            return this;
        }

        public ComplaintRequest UpdateNumberOfClientContracts(string numberOfClientContracts)
        {
            if (numberOfClientContracts != null)
            {
                this.ValidateNumberOfClientContracts(numberOfClientContracts);
                this.NumberOfClientContracts = numberOfClientContracts;
            }
            return this;
        }

        public ComplaintRequest UpdateNameOfClientContracts(string nameOfClientContracts)
        {
            if (nameOfClientContracts != null)
            {
                this.ValidateNameOfClientContracts(nameOfClientContracts);
                this.NameOfClientContracts = nameOfClientContracts;
            }

            return this;
        }

        public ComplaintRequest UpdateReturnToPreviousClaim(bool returnToPreviousClaim)
        {
            this.ReturnToPreviousClaim = returnToPreviousClaim;
            return this;
        } 

        public ComplaintRequest UpdateSolvedInFavor(string solvedInFavor)
        {
            if (solvedInFavor != null)
            {
                this.ValidateSolvedInFavor(solvedInFavor);
                this.SolvedInFavor = solvedInFavor;
            }
            return this;
        } 

        public ComplaintRequest UpdatePVCC(bool pVCC)
        {
            this.PVCC = pVCC;
            return this;
        } 

        public ComplaintRequest UpdateAmountPVCCRON(string amountPVCCRON)
        {
            if (amountPVCCRON != null)
            {
                this.ValidateAmountPVCCRON(amountPVCCRON);
                this.AmountPVCCRON = amountPVCCRON;
            }
            return this;
        } 

        public ComplaintRequest UpdateRegistrationNumber(string registrationNumber)
        {
            if (registrationNumber != null)
            {
                this.ValidateRegistrationNumber(registrationNumber);
                this.RegistrationNumber = registrationNumber;
            }
            return this;
        } 

        public ComplaintRequest UpdateResponseDays(int responseDays)
        {
            this.ValidateResponseDays(responseDays);
            this.ResponseDays = responseDays;

            return this;
        } 

        public ComplaintRequest UpdateRemarks(string remarks)
        {
            if (remarks != null)
            {
                this.ValidateRemarks(remarks);
                this.Remarks = remarks;
            }
            return this;
        } 

        private void Validate(
            DateTime complaintDate, 
            string content)
        {
            ValidateComplaintDate(complaintDate);
            ValidateContent(content);
        }

        private void ValidateContractNumber(string contractNumber)
            => Guard.ForExactStringLength<InvalidRequestException>(
                contractNumber,
                ContractNumberLength,
                nameof(this.ContractNumber));

        private void ValidateContractIssuingDate(DateTime contractIssuingDate)
            => Guard.ForFutureDate<InvalidRequestException>(
                contractIssuingDate, 
                nameof(this.ContractIssuingDate));

        private void ValidateMerchant(string merchant)
            => Guard.ForStringLength<InvalidRequestException>(
                merchant,
                MinMerchantNameLength, 
                MaxMerchantNameLength, 
                nameof(this.Merchant));

        private void ValidateNotificationType(string notificationType)
            => Guard.ForStringLength<InvalidRequestException>(
                notificationType, 
                MinNotificationTypeLength, 
                MaxNotificationTypeLength, 
                nameof(this.NotificationType));

        private void ValidateContent(string contnent)
            => Guard.ForStringLength<InvalidRequestException>(
                contnent, 
                MinContentLength, 
                MaxContentLength, 
                nameof(this.Content));

        private void ValidateComplaintDate(DateTime complaintDate)
            => Guard.ForFutureDate<InvalidRequestException>(
                complaintDate, 
                nameof(this.ComplaintDate));
        
        private void ValidateResponseDate(DateTime responseDate)
            => Guard.ForFutureDate<InvalidRequestException>(
                responseDate, 
                nameof(this.ResponseDate));

        private void ValidateCommunicationHistory(string communicationHistory)
            => Guard.ForStringLength<InvalidRequestException>(
                communicationHistory, 
                MinCommunicationHistoryLength, 
                MaxCommunicationHistoryLength, 
                nameof(this.CommunicationHistory));

        private void ValidateNumberOfClientContracts(string numberOfClientContracts)
            => Guard.ForStringLength<InvalidRequestException>(
                numberOfClientContracts, 
                MinNumberOfClientContractsLength, 
                MaxNumberOfClientContractsLength, 
                nameof(this.NumberOfClientContracts));

        private void ValidateNameOfClientContracts(string nameOfClientContracts)
            => Guard.ForStringLength<InvalidRequestException>(
                nameOfClientContracts, 
                MinNameOfClientContractsLength, 
                MaxNumberOfClientContractsLength, 
                nameof(this.NameOfClientContracts));

        private void ValidateSolvedInFavor(string solvedInFavor)
            => Guard.ForStringLength<InvalidRequestException>(
                solvedInFavor, 
                MinSolvedInFavorLength, 
                MaxSolvedInFavorLength, 
                nameof(this.SolvedInFavor));

        private void ValidateAmountPVCCRON(string amountPVCCRON)
            => Guard.ForStringLength<InvalidRequestException>(
                amountPVCCRON, 
                MinAmountPVCCRONLength, 
                MaxAmountPVCCRONLength, 
                nameof(this.AmountPVCCRON));

        private void ValidateRegistrationNumber(string registrationNumber)
            => Guard.ForExactStringLength<InvalidRequestException>(
                registrationNumber,
                RegistrationNumberLength,
                nameof(this.RegistrationNumber));

        private void ValidateResponseDays(int responseDays)
            => Guard.AgainstOutOfRange<InvalidRequestException>(
                responseDays, 
                MinResponseDays, 
                MaxResponseDays, 
                nameof(responseDays));

        private void ValidateRemarks(string remarks)
            => Guard.ForStringLength<InvalidRequestException>(
                remarks, 
                MinDescriptionLength,
                MaxDescriptionLength, 
                nameof(this.Remarks));
    }
}
