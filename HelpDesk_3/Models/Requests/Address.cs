﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using static HelpDesk_3.Domain.Models.ModelConstants.Address;

namespace HelpDesk_3.Domain.Models.Requests
{
    public class Address : ValueObject
    {
        internal Address(
            string country,
            string city,
            string street,
            int streetNumber,
            int appartment,
            int unit,
            int floor)
        {
            Validate(
                country, 
                city, 
                street, 
                streetNumber, 
                appartment, 
                unit, 
                floor);

            this.Country = country;
            this.City = city;
            this.Street = street;
            this.StreetNumber = streetNumber;
            this.Appartment = appartment;
            this.Unit = unit;
            this.Floor = floor;
        }

        internal Address() { }

        public string? Country { get; private set; } = default!;
        
        public string? City { get; private set; } = default!;

        public string? Street { get; private set; } = default!;

        public int? StreetNumber { get; private set; }

        public int? Appartment { get; private set; }

        public int? Unit { get; private set; }

        public int? Floor { get; private set; }

        public Address UpdateCountry(string country)
        {
            if (country != null)
            {
                this.ValidateCountry(country);
                this.Country = country;
            }

            return this;
        }

        public Address UpdateCity(string city)
        {
            if (city != null)
            {
                this.ValidateCity(city);
                this.City = city;
            }

            return this;
        }

        public Address UpdateStreet(string street)
        {
            if (street != null)
            {
                this.ValidateStreet(street);
                this.Street = street;
            }

            return this;
        }

        public Address UpdateStreetNumber(int streetNumber)
        {
            this.ValidateStreetNumber(streetNumber);
            this.StreetNumber = streetNumber;

            return this;
        }

        public Address UpdateAppartment(int appartment)
        {
            this.ValidateAppartment(appartment);
            this.Appartment = appartment;

            return this;
        }

        public Address UpdateUnit(int unit)
        {
            this.ValidateUnit(unit);
            this.Unit = unit;

            return this;
        }

        public Address UpdateFloor(int floor)
        {
            this.ValidateFloor(floor);
            this.Floor = floor;

            return this;
        }

        private void Validate(
            string country, 
            string city, 
            string street, 
            int streetNumber, 
            int appartment, 
            int unit, 
            int floor)
        {
            ValidateCountry(country);
            ValidateCity(city);
            ValidateStreet(street);
            ValidateStreetNumber(streetNumber);
            ValidateAppartment(appartment);
            ValidateUnit(unit);
            ValidateFloor(floor);
        }

        private void ValidateCountry(string country)
            => Guard.ForStringLength<InvalidAddressException>(
                country,
                MinCountryLength,
                MaxCountryLength,
                nameof(this.Country));

        private void ValidateCity(string city)
            => Guard.ForStringLength<InvalidAddressException>(
                city,
                MinCityLength,
                MaxCityLength,
                nameof(this.City));

        private void ValidateStreet(string street)
            => Guard.ForStringLength<InvalidAddressException>(
                street,
                MinStreetLength,
                MaxStreetLength,
                nameof(this.Street));

        private void ValidateStreetNumber(int streetNumber)
            => Guard.AgainstOutOfRange<InvalidAddressException>(
                streetNumber,
                MinStreetNumberLength,
                MaxStreetNumberLength,
                nameof(this.StreetNumber));

        private void ValidateAppartment(int appartment)
            => Guard.AgainstOutOfRange<InvalidAddressException>(
                appartment,
                MinAppartmentLength,
                MaxAppartmentLength,
                nameof(this.Appartment));

        private void ValidateUnit(int unit)
            => Guard.AgainstOutOfRange<InvalidAddressException>(
                unit,
                MinUnitLength,
                MaxUnitLength,
                nameof(this.Unit));

        private void ValidateFloor(int floor)
            => Guard.AgainstOutOfRange<InvalidAddressException>(
                floor,
                MinFloorLength,
                MaxFloorLength,
                nameof(this.Floor));
    }
}
