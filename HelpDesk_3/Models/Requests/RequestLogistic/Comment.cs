﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Employees;

namespace HelpDesk_3.Domain.Models.Requests.RequestLogistic
{
    public class Comment : Entity<Guid>
    {
        public Comment() {}

        public string RequestId { get; private set; } = default!;

        public string Category { get; private set; } = default!;

        public Employee CreatedBy { get; private set; } = default!;

        public string Content { get; private set; } = default!;

        public string Status { get; private set; } = default!;

        public Attachment Attachment { get; private set; } = default!;

        private Comment UpdateRequestId(string requestId)
        {
            this.ValidateRequestId(requestId);
            this.RequestId = requestId;

            return this;
        }

        private Comment UpdateCategory(string category)
        {
            this.ValidateCategory(category);
            this.Category = category;

            return this;
        }

        private Comment UpdateCreatedBy(Employee createdBy)
        {
            this.ValidateCreatedBy(createdBy);
            this.CreatedBy = createdBy;

            return this;
        }

        private Comment UpdateContent(string content)
        {
            this.ValidateContent(content);
            this.Content = content;

            return this;
        }

        private Comment UpdateStatus(string status)
        {
            this.ValidateStatus(status);
            this.Status = status;

            return this;
        }

        private Comment UpdateAttachment(Attachment attachment)
        {
            this.ValidateAttachment(attachment);
            this.Attachment = attachment;

            return this;
        }

        private void ValidateRequestId(string requestId)
            => Guard.AgainstEmptyString<InvalidCommentException>(
                requestId,
                nameof(this.RequestId));

        private void ValidateCategory(string category)
            => Guard.AgainstEmptyString<InvalidCommentException>(
                category,
                nameof(this.Category));

        private void ValidateCreatedBy(Employee createdBy)
            => Guard.AgainstNull<InvalidCommentException>(
                createdBy,
                nameof(this.CreatedBy));

        private void ValidateContent(string content)
            => Guard.AgainstEmptyString<InvalidCommentException>(
                content,
                nameof(this.Content));

        private void ValidateStatus(string status)
            => Guard.AgainstEmptyString<InvalidCommentException>(
                status,
                nameof(this.Status));

        private void ValidateAttachment(Attachment attachment)
            => Guard.AgainstNull<InvalidCommentException>(
                attachment,
                nameof(this.Attachment));
    }
}
