﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

namespace HelpDesk_3.Domain.Models.Requests.RequestLogistic
{
    public class Step : Entity<int>
    {
        public Step() {}

        public string Category { get; private set; } = default!;

        public string CurrentRole { get; private set; } = default!;

        public string NextRole { get; private set; } = default!;

        public bool CanStart { get; set; }

        public bool CanFinish { get; set; }

        public bool CanEdit { get; set; }

        public Status CurrentStatus { get; set; }

        public Status NextStatus { get; set; }

        public Button Button { get; set; }

        public bool ButtonVisible { get; set; }

        public Step UpdateCategory(string category)
        {
            this.ValidateCategory(category);
            this.Category = category;

            return this;
        }

        public Step UpdateCurrentRole(string currentRole)
        {
            this.ValidateCurrentRole(currentRole);
            this.CurrentRole = currentRole;

            return this;
        }

        public Step UpdateNextRole(string nextRole)
        {
            this.ValidateNextRole(nextRole);
            this.NextRole = nextRole;

            return this;
        }

        public Step UpdateCanStart(bool canStart)
        {
            this.CanStart = canStart;

            return this;
        }

        public Step UpdateCanFinish(bool canFinish)
        {
            this.CanFinish = canFinish;

            return this;
        }

        public Step UpdateCanEdit(bool canEdit)
        {
            this.CanEdit = canEdit;

            return this;
        }

        public Step UpdateCurrentStatus(Status currentStatus)
        {
            this.CurrentStatus = currentStatus;

            return this;
        }

        public Step UpdateNextStatus(Status nextStatus)
        {
            this.NextStatus = nextStatus;

            return this;
        }

        public Step UpdateButton(Button button)
        {
            this.Button = button;

            return this;
        }

        public Step UpdateButtonVisible(bool buttonVisible)
        {
            this.ButtonVisible = buttonVisible;

            return this;
        }

        private void ValidateCategory(string category)
            => Guard.AgainstEmptyString<InvalidStepException>(
                category,
                nameof(this.Category));

        private void ValidateCurrentRole(string currentRole)
            => Guard.AgainstEmptyString<InvalidStepException>(
                currentRole,
                nameof(this.CurrentRole));

        private void ValidateNextRole(string nextRole)
            => Guard.AgainstEmptyString<InvalidStepException>(
                nextRole,
                nameof(this.NextRole));
    }
}
