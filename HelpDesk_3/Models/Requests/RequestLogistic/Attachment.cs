﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Employees;

namespace HelpDesk_3.Domain.Models.Requests.RequestLogistic
{
    public class Attachment : Entity<Guid>
    {
        public Attachment() {}

        public string Path { get; private set; } = default!;

        public DateTime CreatedOn { get; private set; } = default!;

        public Employee CreatedBy { get; private set; } = default!;

        public DateTime EditedOn { get; private set; } = default!;

        private Attachment UpdatePath(string path)
        {
            this.ValidatePath(path);
            this.Path = path;

            return this;
        }

        private Attachment UpdateCreatedOn(DateTime createdOn)
        {
            this.ValidateCreatedOn(createdOn);
            this.CreatedOn = createdOn;

            return this;
        }

        private Attachment UpdateCreatedBy(Employee createdBy)
        {
            this.ValidateCreatedBy(createdBy);
            this.CreatedBy = createdBy;

            return this;
        }

        private Attachment UpdateEditedOn(DateTime editedOn)
        {
            this.ValidateEditedOn(editedOn);
            this.EditedOn = editedOn;

            return this;
        }

        private void ValidatePath(string path)
            => Guard.ForCorrectUri<InvalidAttachmentException>(
                path,
                nameof(this.Path));

        private void ValidateCreatedOn(DateTime createdOn)
            => Guard.ForFutureDate<InvalidAttachmentException>(
                createdOn,
                nameof(this.CreatedOn));

        private void ValidateCreatedBy(Employee createdBy)
            => Guard.AgainstNull<InvalidAttachmentException>(
                createdBy,
                nameof(this.CreatedBy));

        private void ValidateEditedOn(DateTime editedOn)
            => Guard.ForFutureDate<InvalidAttachmentException>(
                editedOn,
                nameof(this.EditedOn));
    }
}
