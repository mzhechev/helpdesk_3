﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;

using static HelpDesk_3.Domain.Models.ModelConstants.Categories;

namespace HelpDesk_3.Domain.Models.Requests
{
    public abstract class RequestData : Entity<Guid>, IAggregateRoot
    {
        internal RequestData(
            string category,
            string categoryGroup,
            Status status)
        {
            this.Validate(category, categoryGroup);

            this.Category = category;
            this.CategoryGroup = categoryGroup;
            this.Status = status;
        }

        public string Category { get; private set; }

        public string CategoryGroup { get; private set; }

        public Status Status { get; private set; }

        public RequestData UpdateCategory(string category)
        {
            if (category != null)
            {
                this.ValidateCategory(category);
                this.Category = category;
            }

            return this;
        }

        public RequestData UpdateCategoryGroup(string categoryGroup)
        {
            if (categoryGroup != null)
            {
                this.ValidateCategory(categoryGroup);
                this.CategoryGroup = categoryGroup;
            }

            return this;
        }

        public RequestData UpdateStatus(Status status)
        {
            this.Status = status;

            return this;
        }

        private void Validate(string category, string categoryGroup)
        {
            this.ValidateCategory(category);
            this.ValidateCategoryGroup(categoryGroup);
        }

        private void ValidateCategory(string category)
            => Guard.ForStringLength<InvalidRequestException>(
                category,
                MinCategoriesLength,
                MaxCategoriesLength,
                nameof(this.Category));

        private void ValidateCategoryGroup(string categoryGroup)
            => Guard.ForStringLength<InvalidRequestException>(
                categoryGroup,
                MinCategoriesLength,
                MaxCategoriesLength,
                nameof(this.CategoryGroup));
    }
}
