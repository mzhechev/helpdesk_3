﻿using HelpDesk_3.Domain.Common;
using HelpDesk_3.Domain.Exceptions;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Domain.Models.Requests
{
    public class PersonalInformation : ValueObject
    {
        internal PersonalInformation(string firstName)
        {
            this.ValidateFirstName(firstName);

            this.FirstName = firstName;
        }

        internal PersonalInformation(
            string firstName,
            string lastName)
        {
            Validate(firstName, lastName);

            this.FirstName = firstName;
            this.LastName = lastName;
        }

        internal PersonalInformation(
            string firstName,
            string middleName,
            string lastName)
        {
            Validate(firstName, middleName, lastName);

            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.LastName = lastName;
        }

        internal PersonalInformation(
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber)
        {
            Validate(firstName, middleName, lastName, identificationNumber);

            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.LastName = lastName;
            this.IdentificationNumber = identificationNumber;
        }

        public string FirstName { get; private set; } = default!;

        public string? MiddleName { get; private set; } = default!;

        public string? LastName { get; private set; } = default!;

        public string? IdentificationNumber { get; private set; } = default!;

        private void Validate(string firstName, string middleName, string lastName, string identificationNumber)
        {
            ValidateFirstName(firstName);
            ValidateMiddleName(middleName);
            ValidateLastName(lastName);
            ValidateIdentificationNumber(identificationNumber);
        }

        private void Validate(string firstName, string middleName, string lastName)
        {
            ValidateFirstName(firstName);
            ValidateMiddleName(middleName);
            ValidateLastName(lastName);
        }

        private void Validate(string firstName, string lastName)
        {
            ValidateFirstName(firstName);
            ValidateLastName(lastName);
        }

        public PersonalInformation UpdateFirstName(string firstName)
        {
            if (firstName != null)
            {
                this.ValidateFirstName(firstName);
                this.FirstName = firstName;
            }

            return this;
        }

        public PersonalInformation UpdateMiddleName(string middleName)
        {
            if (middleName != null)
            {
                this.ValidateMiddleName(middleName);
                this.MiddleName = middleName;
            }

            return this;
        }

        public PersonalInformation UpdateLastName(string lastName)
        {
            if (lastName != null)
            {
                this.ValidateLastName(lastName);
                this.LastName = lastName;
            }

            return this;
        }

        public PersonalInformation UpdateIdentificationNumber(string identificationNumber)
        {
            if (identificationNumber != null)
            {
                this.ValidateIdentificationNumber(identificationNumber);
                this.IdentificationNumber = identificationNumber;
            }

            return this;
        }

        private void ValidateFirstName(string firstName)
            => Guard.ForStringLength<InvalidPersonalInformationException>(
                firstName,
                MinNameLength,
                MaxNameLength,
                nameof(this.FirstName));

        private void ValidateMiddleName(string middleName)
           => Guard.ForStringLength<InvalidPersonalInformationException>(
               middleName,
               MinNameLength,
               MaxNameLength,
               nameof(this.MiddleName));

        private void ValidateLastName(string lastName)
           => Guard.ForStringLength<InvalidPersonalInformationException>(
               lastName,
               MinNameLength,
               MaxNameLength,
               nameof(this.LastName));

        private void ValidateIdentificationNumber(string identificationNumber)
            => Guard.ForCorrectEGN<InvalidPersonalInformationException>(
                identificationNumber,
                nameof(this.IdentificationNumber));
    }
}
