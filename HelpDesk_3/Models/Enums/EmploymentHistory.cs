﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum EmploymentHistory
    {
        NewEmployee,
        Rehired
    }
}
