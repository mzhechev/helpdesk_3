﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum ReportingLevel
    {
        N1,
        N2,
        N3,
        N4,
        N5
    }
}
