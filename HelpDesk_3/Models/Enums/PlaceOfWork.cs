﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum PlaceOfWork
    {
        FixedDesk,
        HotDesk,
        Remote
    }
}
