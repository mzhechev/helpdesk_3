﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum CentralCreditRecord
    {
        Good,
        Bad,
        NoLoanHistory,
        NoConnectionToCreditHistory
    }
}
