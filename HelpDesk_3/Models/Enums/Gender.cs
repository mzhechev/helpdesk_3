﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum Gender
    {
        Male,
        Female,
        Indefinite
    }
}
