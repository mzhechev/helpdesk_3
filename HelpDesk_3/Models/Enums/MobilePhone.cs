﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum MobilePhone
    {
        IssueExistingMobileNumber,
        IssueNewMobileNumber,
        NoMobilePhoneNeeded
    }
}
