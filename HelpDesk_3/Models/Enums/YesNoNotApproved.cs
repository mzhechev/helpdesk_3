﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum YesNoNotApproved
    {
        Yes,
        No,
        NotApproved
    }
}
