﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum PrefferedEquipment
    {
        Desktop,
        Laptop,
        Both,
        None
    }
}
