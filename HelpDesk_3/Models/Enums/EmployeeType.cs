﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum EmployeeType
    {
        Employee,
        ForeignEmployee,
        Intern,
        ForeignIntern,
        Consultant
    }
}
