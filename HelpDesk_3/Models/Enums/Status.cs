﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum Status
    {
        NotSet,
        New,
        Started,
        InProcess,
        Completed,
        Closed,
        Deleted,
        Returned
    }
}
