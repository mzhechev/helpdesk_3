﻿namespace HelpDesk_3.Domain.Models.Enums
{
    public enum BonusEligibility
    {
        AnnualBonus,
        PerformanceBonus
    }
}
