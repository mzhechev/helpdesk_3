﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Application.Features.Identity;
using HelpDesk_3.Application.Features.Identity.Commands.LoginUser;

namespace HelpDesk_3.Application.Contracts
{
    public interface IIdentity
    {
        Task<Result<IUser>> Register(UserInputModel userInput);

        Task<Result<LoginOutputModel>> Login(UserInputModel userInput);
    }
}
