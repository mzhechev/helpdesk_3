﻿using HelpDesk_3.Domain.Common;

namespace HelpDesk_3.Application.Contracts
{
    public interface IRepository<in TEntity>
        where TEntity : IAggregateRoot
    {
        Task Save(TEntity entity, CancellationToken cancellationToken = default);
    }
}
