﻿namespace HelpDesk_3.Application.Contracts
{
    public interface ICurrentUser
    {
        string UserId { get; }

        List<string> UserRoles { get; }
    }
}
