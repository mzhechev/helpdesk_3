﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Employees;
using HelpDesk_3.Domain.Factories.Employees;
using MediatR;

namespace HelpDesk_3.Application.Features.Identity.Commands.CreateUser
{
    public class CreateUserCommand : UserInputModel, IRequest<Result>
    {
        public CreateUserCommand(
            string email, 
            string password,
            string firstName,
            string middleName,
            string lastName,
            string identificationNumber) 
            : base(email, password)
        {
            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.LastName = lastName;
            this.IdentificationNumber = identificationNumber;
        }

        public string FirstName { get; }
        public string MiddleName { get; }
        public string LastName { get; }
        public string IdentificationNumber { get; }

        public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Result>
        {
            private readonly IIdentity identity;
            private readonly IEmployeeFactory employeeFactory;
            private readonly IEmployeeRepository employeeRepository;

            public CreateUserCommandHandler(
                IIdentity identity,
                IEmployeeFactory employeeFactory,
                IEmployeeRepository employeeRepository)
            {
                this.identity = identity;
                this.employeeFactory = employeeFactory;
                this.employeeRepository = employeeRepository;
            }

            public async Task<Result> Handle(
                CreateUserCommand request, 
                CancellationToken cancellationToken)
            {
                var result = await this.identity.Register(request);

                if (!result.Succeeded)
                {
                    return result;
                }

                var user = result.Data;

                var employee = this.employeeFactory
                    .WithPersonalInformation(
                        request.FirstName, 
                        request.MiddleName, 
                        request.LastName, 
                        request.IdentificationNumber)
                    .Build();

                user.BecomeEmployee(employee);

                await this.employeeRepository.Save(employee, cancellationToken);

                return result;
            }
        }
    }
}
