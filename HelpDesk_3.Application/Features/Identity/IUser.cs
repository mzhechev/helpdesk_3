﻿using HelpDesk_3.Domain.Models.Employees;

namespace HelpDesk_3.Application.Features.Identity
{
    public interface IUser
    {
        void BecomeEmployee(Employee employee);
    }
}
