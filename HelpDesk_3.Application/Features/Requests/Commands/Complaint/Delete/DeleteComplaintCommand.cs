﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Commands.Complaint.Delete
{
    public class DeleteComplaintCommand : EntityCommand<Guid>, IRequest<Result>
    {
        public string Category { get; set; } = default!;

        public class DeleteComplaintCommandHandler : IRequestHandler<DeleteComplaintCommand, Result>
        {
            private readonly IRequestRepository requestRepository;

            public DeleteComplaintCommandHandler(IRequestRepository requestRepository)
                => this.requestRepository = requestRepository;

            public async Task<Result> Handle(
                DeleteComplaintCommand request, 
                CancellationToken cancellationToken)
            {
                var complaintRequest = await this.requestRepository
                    .Find(request.Id, request.Category, cancellationToken);

                if (complaintRequest == null) 
                {
                    throw new InvalidRequestException("There is no Complaint Request with the given Id.");
                }

                complaintRequest.UpdateStatus(Status.Deleted);

                await this.requestRepository.Save(complaintRequest, cancellationToken);

                return Result.Success;
            }
        }
    }
}
