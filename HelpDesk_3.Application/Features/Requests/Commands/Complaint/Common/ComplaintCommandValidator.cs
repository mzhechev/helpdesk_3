﻿using FluentValidation;

using static HelpDesk_3.Domain.Models.ModelConstants.Categories;
using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Application.Features.Requests.Commands.Complaint.Common
{
    public class ComplaintCommandValidator<TCommand> : AbstractValidator<ComplaintCommand<TCommand>>
        where TCommand : EntityCommand<Guid>
    {
        public ComplaintCommandValidator()
        {
            this.RuleFor(c => c.Category)
                .MinimumLength(MinCategoriesLength)
                .MaximumLength(MaxCategoriesLength);

            this.RuleFor(c => c.CategoryGroup)
                .MinimumLength(MinCategoriesLength)
                .MaximumLength(MaxCategoriesLength);

            this.RuleFor(c => c.FirstName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(c => c.MiddleName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(c => c.LastName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(c => c.IdentificationNumber)
                .Length(EGNLength);

            this.RuleFor(c => c.PhoneNumber)
                .Length(PhoneLength);

            this.RuleFor(c => c.Email)
                .MinimumLength(MinEmailLength)
                .MaximumLength(MaxEmailLength);

            this.RuleFor(c => c.Content)
                .MinimumLength(MinDescriptionLength)
                .MaximumLength(MaxDescriptionLength);
        }
    }
}
