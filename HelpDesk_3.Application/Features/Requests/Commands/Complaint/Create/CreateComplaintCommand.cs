﻿using HelpDesk_3.Application.Features.Customers;
using HelpDesk_3.Application.Features.Requests.Commands.Complaint.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Factories.Requests.Complaint;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Commands.Complaint.Create
{
    public class CreateComplaintCommand : ComplaintCommand<CreateComplaintCommand>, IRequest<CreateComplaintOutputModel>
    {
        public class CreateComplaintCommandHandler : IRequestHandler<CreateComplaintCommand, CreateComplaintOutputModel>
        {
            private readonly IComplaintRequestFactory complaintRequestFactory;
            private readonly IRequestRepository requestRepository;
            private readonly ICustomerRepository customerRepository;

            public CreateComplaintCommandHandler(
                IComplaintRequestFactory complaintRequestFactory,
                IRequestRepository requestRepository,
                ICustomerRepository customerRepository)
            {
                this.complaintRequestFactory = complaintRequestFactory;

                this.requestRepository = requestRepository;
                this.customerRepository = customerRepository;
            }

            public async Task<CreateComplaintOutputModel> Handle(
                CreateComplaintCommand request, 
                CancellationToken cancellationToken)
            {
                Enum.TryParse(request.Status, out Domain.Models.Enums.Status parsedStatus);

                var customer = await this.customerRepository
                    .Find(request.CustomerId, cancellationToken);

                if (customer == null)
                {
                    throw new InvalidRequestException("Cannot link the Request to a Customer because no such Customer exists.");
                }

                var complaintRequest = this.complaintRequestFactory
                    .WithRequestData(
                        request.Category,
                        request.CategoryGroup,
                        parsedStatus)
                    .WithPersonalInformation(
                        request.FirstName,
                        request.MiddleName,
                        request.LastName,
                        request.IdentificationNumber)
                    .WithCustomer(customer)
                    .WithContactInformation(
                        request.PhoneNumber,
                        request.Email)
                    .WithContent(request.Content)
                    .WithComplaintDate(DateTime.Now)
                    .Build();

                await this.requestRepository.Save(complaintRequest, cancellationToken);

                return new CreateComplaintOutputModel(
                    complaintRequest.Status.ToString(),
                    complaintRequest.Id);
            }
        }
    }
}
