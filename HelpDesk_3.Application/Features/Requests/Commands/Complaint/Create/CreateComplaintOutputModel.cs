﻿using HelpDesk_3.Domain.Models.Enums;

namespace HelpDesk_3.Application.Features.Requests.Commands.Complaint.Create
{
    public class CreateComplaintOutputModel
    {
        public CreateComplaintOutputModel(
            string status,
            Guid complaintId)
        {
            this.Status = status;
            this.ComplaintRequestId = complaintId;
        }

        public string Status { get; set; }

        public Guid ComplaintRequestId { get; set; }
    }
}
