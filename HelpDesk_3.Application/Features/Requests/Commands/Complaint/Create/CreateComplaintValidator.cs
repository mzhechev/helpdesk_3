﻿using FluentValidation;
using HelpDesk_3.Application.Features.Requests.Commands.Complaint.Common;

namespace HelpDesk_3.Application.Features.Requests.Commands.Complaint.Create
{
    public class CreateComplaintValidator : AbstractValidator<CreateComplaintCommand>
    {
        public CreateComplaintValidator()
            => this.Include(new ComplaintCommandValidator<CreateComplaintCommand>());
    }
}
