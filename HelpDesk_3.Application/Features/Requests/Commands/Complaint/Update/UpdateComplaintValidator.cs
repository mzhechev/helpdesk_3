﻿using FluentValidation;
using HelpDesk_3.Application.Features.Requests.Commands.Complaint.Common;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;
using static HelpDesk_3.Domain.Models.ModelConstants.ComplaintRequest;
using static HelpDesk_3.Domain.Models.ModelConstants.ComplaintInformation;

namespace HelpDesk_3.Application.Features.Requests.Commands.Complaint.Update
{
    public class UpdateComplaintValidator : AbstractValidator<UpdateComplaintCommand>
    {
        public UpdateComplaintValidator()
        {
            this.Include(new ComplaintCommandValidator<UpdateComplaintCommand>());

            this.RuleFor(c => c.ContractNumber)
                .Length(ContractNumberLength);

            this.RuleFor(c => c.Merchant)
                .MinimumLength(MinMerchantNameLength)
                .MaximumLength(MaxMerchantNameLength);

            this.RuleFor(c => c.NotificationType)
                .MinimumLength(MinNotificationTypeLength)
                .MaximumLength(MaxNotificationTypeLength);

            this.RuleFor(c => c.SourceOfComplaint)
                .MinimumLength(MinComplaintInformationLength)
                .MaximumLength(MaxComplaintInformationLength);

            this.RuleFor(c => c.MethodOfReceivingComplaint)
                .MinimumLength(MinComplaintInformationLength)
                .MaximumLength(MaxComplaintInformationLength);

            this.RuleFor(c => c.SubjectOfComplaint)
                .MinimumLength(MinComplaintInformationLength)
                .MaximumLength(MaxComplaintInformationLength);

            this.RuleFor(c => c.DetailsOfComplaint)
                .MinimumLength(MinComplaintInformationLength)
                .MaximumLength(MaxComplaintInformationLength);

            this.RuleFor(c => c.CommunicationHistory)
                .MinimumLength(MinCommunicationHistoryLength)
                .MaximumLength(MaxCommunicationHistoryLength);

            this.RuleFor(c => c.NumberOfClientContracts)
                .MinimumLength(MinNumberOfClientContractsLength)
                .MaximumLength(MaxNameOfClientContractsLength);

            this.RuleFor(c => c.NameOfClientContracts)
                .MinimumLength(MinNameOfClientContractsLength)
                .MaximumLength(MaxNameOfClientContractsLength);

            this.RuleFor(c => c.SolvedInFavor)
                .MinimumLength(MinSolvedInFavorLength)
                .MaximumLength(MaxSolvedInFavorLength);

            this.RuleFor(c => c.AmountPVCCRON)
                .MinimumLength(MinAmountPVCCRONLength)
                .MaximumLength(MaxAmountPVCCRONLength);

            this.RuleFor(c => c.RegistrationNumber)
                .Length(RegistrationNumberLength);

            this.RuleFor(c => c.Remarks)
                .MinimumLength(MinDescriptionLength)
                .MaximumLength(MaxDescriptionLength);
        }
    }
}
