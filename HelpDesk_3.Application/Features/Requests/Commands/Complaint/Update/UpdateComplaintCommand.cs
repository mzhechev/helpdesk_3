﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Customers;
using HelpDesk_3.Application.Features.Requests.Commands.Complaint.Common;
using HelpDesk_3.Domain.Models.Requests;
using HelpDesk_3.Domain.Models.Requests.Request.Complaint;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Commands.Complaint.Update
{
    public class UpdateComplaintCommand : ComplaintCommand<UpdateComplaintCommand>, IRequest<Result>
    {
        public string ContractNumber { get; set; } = default!;
        public string ContractStatus { get; set; } = default!;
        public string TypeOfLoan { get; set; } = default!;
        public string Merchant { get; set; } = default!;
        public string NotificationType { get; set; } = default!;
        public string ComplaintDate { get; set; } = default!;
        public string ResponseDate { get; set; } = default!;
        public string SourceOfComplaint { get; set; } = default!;
        public string MethodOfReceivingComplaint { get; set; } = default!;
        public string SubjectOfComplaint { get; set; } = default!;
        public string DetailsOfComplaint { get; set; } = default!;
        public string CommunicationHistory { get; set; } = default!;
        public string NumberOfClientContracts { get; set; } = default!;
        public string NameOfClientContracts { get; set; } = default!;
        public bool ReturnToPreviousClaim { get; set; }
        public string SolvedInFavor { get; set; } = default!;
        public bool PVCC { get; set; }
        public string AmountPVCCRON { get; set; } = default!;
        public string RegistrationNumber { get; set; } = default!;
        public string Remarks { get; set; } = default!;

        public class UpdateComplaintCommandHandler : IRequestHandler<UpdateComplaintCommand, Result>
        {
            private readonly IRequestRepository requestRepository;
            private readonly ICustomerRepository customerRepository;
            private readonly ICurrentUser currentUser;

            public UpdateComplaintCommandHandler(
                IRequestRepository requestRepository,
                ICustomerRepository customerRepository,
                ICurrentUser currentUser)
            {
                this.requestRepository = requestRepository;
                this.customerRepository = customerRepository;
                this.currentUser = currentUser;
            }

            public async Task<Result> Handle(
                UpdateComplaintCommand request, 
                CancellationToken cancellationToken)
            {
                var complaintRequest = (ComplaintRequest)(await this.requestRepository
                    .Find(request.Id, request.Category, cancellationToken));

                if (complaintRequest == null)
                {
                    return false;
                }

                var userRole = this.currentUser.UserRoles;
                var step = await this.requestRepository.SearchForStep(complaintRequest.Category, complaintRequest.Status, cancellationToken);

                if (userRole.Contains(step.CurrentRole)){
                    Enum.TryParse(request.ContractStatus, out Domain.Models.Enums.ContractStatus parsedContractStatus);
                    Enum.TryParse(request.TypeOfLoan, out Domain.Models.Enums.TypeOfLoan parsedTypeOfLoan);

                    DateTime parsedComplaintDate = DateTime.MinValue.AddDays(1);
                    DateTime parsedResponseDate = DateTime.MinValue;

                    if (!string.IsNullOrEmpty(request.ComplaintDate) && !string.IsNullOrEmpty(request.ResponseDate))
                    {
                        parsedComplaintDate = DateTime.Parse(request.ComplaintDate);
                        parsedResponseDate = DateTime.Parse(request.ResponseDate);
                    }

                    var responseDays = int.Parse(
                        parsedComplaintDate.Subtract(parsedResponseDate).Days.ToString());
                
                    var customer = await this.customerRepository
                        .Find(request.CustomerId, cancellationToken);

                    if (customer == null)
                    {
                        return false;
                    }

                    complaintRequest
                        .UpdatePersonalInformation(
                            request.FirstName,
                            request.MiddleName,
                            request.LastName,
                            request.IdentificationNumber)
                        .UpdateContractNumber(request.ContractNumber)
                        .UpdateCustomer(customer)
                        .UpdateContactInformation(
                            request.PhoneNumber,
                            request.Email)
                        .UpdateContractStatus(parsedContractStatus)
                        .UpdateTypeOfLoan(parsedTypeOfLoan)
                        .UpdateMerchant(request.Merchant)
                        .UpdateNotificationType(request.NotificationType)
                        .UpdateContent(request.Content)
                        .UpdateComplaintDate(parsedComplaintDate)
                        .UpdateResponseDate(parsedResponseDate)
                        .UpdateComplaintInformation(
                            request.SourceOfComplaint,
                            request.MethodOfReceivingComplaint,
                            request.SubjectOfComplaint,
                            request.DetailsOfComplaint)
                        .UpdateCommunicationHistory(request.CommunicationHistory)
                        .UpdateNumberOfClientContracts(request.NumberOfClientContracts)
                        .UpdateNameOfClientContracts(request.NameOfClientContracts)
                        .UpdateReturnToPreviousClaim(request.ReturnToPreviousClaim)
                        .UpdateSolvedInFavor(request.SolvedInFavor)
                        .UpdatePVCC(request.PVCC)
                        .UpdateAmountPVCCRON(request.AmountPVCCRON)
                        .UpdateRegistrationNumber(request.RegistrationNumber)
                        .UpdateResponseDays(responseDays)
                        .UpdateRemarks(request.Remarks)
                        .UpdateStatus(step.NextStatus);

                    await this.requestRepository.Save(complaintRequest, cancellationToken);
                } else
                {
                    //TODO find better way to return fail
                    return Result.Failure(new List<string>() { $"User must be in {step.CurrentRole} role, to edit this request." });
                }

                return Result.Success;
            }
        }
    }
}
