﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class AddressInputModel
    {
        public string Country { get; set; } = default!;
        public string City { get; set; } = default!;
        public string Street { get; set; } = default!;
        public int StreetNumber { get; set; }
        public int Appartment { get; set; }
        public int Unit { get; set; }
        public int Floor { get; set; }
    }
}
