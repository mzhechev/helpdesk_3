﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class FraudCheckInputModel
    {
        public bool CriminalRecord { get; set; }
        public string CentralCreditRecord { get; set; } = default!;
        public bool BlackList { get; set; }
        public bool ValidateInformation { get; set; }
        public string SmartInfo { get; set; } = default!;
    }
}
