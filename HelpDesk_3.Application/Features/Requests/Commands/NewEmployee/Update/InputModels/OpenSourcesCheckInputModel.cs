﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class OpenSourcesCheckInputModel
    {
        public bool Posts { get; set; }
        public bool PictureSearch { get; set; }
        public bool GoogleCheck { get; set; }
        public bool HateGroups { get; set; }
        public bool AccessCard { get; set; }
        public bool InternalSecurity { get; set; }
    }
}
