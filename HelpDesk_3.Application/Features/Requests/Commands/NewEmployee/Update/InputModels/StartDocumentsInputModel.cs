﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class StartDocumentsInputModel
    {
        public string ReportingLevel { get; set; } = default!;
        public bool ProbationPeriodValidation { get; set; }
        public bool DeclarationGDPR { get; set; }
        public bool LabourBook { get; set; }
        public bool MedicalCertificate { get; set; }
        public bool Diploma { get; set; }
        public bool SignedJobOffer { get; set; }
        public bool JobDescription { get; set; }
    }
}
