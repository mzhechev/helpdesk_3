﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class OrganizationDataInputModel 
    {
        public string Division { get; set; } = default!;
        public string Department { get; set; } = default!;
        public string Unit { get; set; } = default!;
        public string Location { get; set; } = default!;
        public string CostProfitArea { get; set; } = default!;
        public string CostCenter { get; set; } = default!;
        public string OfficialPosition { get; set; } = default!;
        public string InternalPosition { get; set; } = default!;
        public string DirectManagerAdminReporting { get; set; } = default!;
        public string DirectManagerFunctionalReporting { get; set; } = default!;
        public string Bonus { get; set; } = default!;
        public string AdditionalComments { get; set; } = default!;
        public string Photo { get; set; } = default!;
        public string CiriumVitae { get; set; } = default!;
    }
}
