﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class StartDocumentsSignedInputModel
    {
        public bool SignedJobDescription { get; set; }
        public bool SignedLabourContract { get; set; }
        public bool PCCCheck { get; set; }
        public string PCCCheckDate { get; set; } = default!;
        public bool InstructionNoteHS { get; set; }
        public bool NRA { get; set; }
        public bool VideoSurveillance { get; set; }
        public bool GDPR { get; set; }
        public bool GDPRPhotosVideo { get; set; }
    }
}
