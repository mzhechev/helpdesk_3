﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class AccessToSystemsDataOutputModel
    {
        public string ADUser { get; set; } = default!;
        public string ADPass { get; set; } = default!;
        public string VCSUser { get; set; } = default!;
        public string VCSPass { get; set; } = default!;
        public string SmartInfoUser { get; set; } = default!;
        public string SmartInfoPass { get; set; } = default!;
        public string SCardUser { get; set; } = default!;
        public string SCardPass { get; set; } = default!;
        public string FOSUser { get; set; } = default!;
        public string FOSPass { get; set; } = default!;
        public string SyronUser { get; set; } = default!;
        public string SyronPass { get; set; } = default!;
        public string ICollectUser { get; set; } = default!;
        public string ICollectPass { get; set; } = default!;
        public string UserEmail { get; set; } = default!;
        public bool CustomerView { get; set; }
        public string EquipmentOnFirstDay { get; set; } = default!;
        public bool AdditionalEquipmentApproved { get; set; }
        public bool AccessToSharedFolders { get; set; }
        public bool AddedToGroupEmails { get; set; }
        public string _UpdateAdditionalAccessesRequested { get; set; } = default!;
    }
}
