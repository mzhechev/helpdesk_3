﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class RequestAccessToSystemsInputModel
    {
        public bool OperationalRisk { get; set; }
        public bool SmartInfo { get; set; }
        public string SmartInfoRoles { get; set; } = default!;
        public bool SCard { get; set; }
        public string SCardRoles { get; set; } = default!;
        public bool FOS { get; set; }
        public string FOSRoles { get; set; } = default!;
        public bool CeGate { get; set; }
        public bool ICollect { get; set; }
        public string ICollectRoles { get; set; } = default!;
        public bool Syron { get; set; }
        public string SyronRoles { get; set; } = default!;
        public string AccessLikeUser { get; set; } = default!;
        public bool VCS { get; set; }
        public string VCSRoles { get; set; } = default!;
        public string PhoneRecievingVCS { get; set; } = default!;
        public bool CustomerView { get; set; }
        public string AccessToOtherSystems { get; set; } = default!;
        public bool AccessToEmailByMobile { get; set; }
        public string EmailGroups { get; set; } = default!;
        public string SharedFolders { get; set; } = default!;
    }
}
