﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels
{
    public class EquipmentAndAccessInputModel
    {
        public string WorkingPlace { get; set; } = default!;
        public bool StationaryPhone { get; set; }
        public string WorkingPlaceLocation { get; set; } = default!;
        public string LeavingEmployee { get; set; } = default!;
        public string MobilePhone { get; set; } = default!;
        public bool Uniform { get; set; }
        public bool CompanyCar { get; set; }
        public string PrefferedEquipment { get; set; } = default!;
        public bool VPN { get; set; }
        public string AdditionalEquipment { get; set; } = default!;
    }
}
