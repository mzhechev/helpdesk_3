﻿using FluentValidation;
using HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Common;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;
using static HelpDesk_3.Domain.Models.ModelConstants.Address;
using static HelpDesk_3.Domain.Models.ModelConstants.OrganizationData;
using static HelpDesk_3.Domain.Models.ModelConstants.FraudCheck;
using static HelpDesk_3.Domain.Models.ModelConstants.EquipmentAndAccess;
using static HelpDesk_3.Domain.Models.ModelConstants.RequestAccessToSystems;

namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update
{
    public class UpdateNewEmployeeValidator : AbstractValidator<UpdateNewEmployeeCommand>
    {
        public UpdateNewEmployeeValidator()
        {
            this.Include(new NewEmployeeCommandValidator<UpdateNewEmployeeCommand>());

            this.RuleFor(n => n.ProbationPeriod)
                .GreaterThanOrEqualTo(Zero);

            this.RuleFor(n => n.NoticePeriod)
                .GreaterThanOrEqualTo(Zero);

            this.RuleFor(n => n.Address.Country)
                .MinimumLength(MinCountryLength)
                .MaximumLength(MaxCountryLength);

            this.RuleFor(n => n.Address.City)
                .MinimumLength(MinCityLength)
                .MaximumLength(MaxCityLength);

            this.RuleFor(n => n.Address.Street)
                .MinimumLength(MinStreetLength)
                .MaximumLength(MaxStreetLength);

            this.RuleFor(n => n.Address.StreetNumber)
                .GreaterThanOrEqualTo(Zero);

            this.RuleFor(n => n.Address.Appartment)
                .GreaterThanOrEqualTo(Zero);

            this.RuleFor(n => n.Address.Unit)
                .GreaterThanOrEqualTo(Zero);

            this.RuleFor(n => n.Address.Floor)
                .GreaterThanOrEqualTo(Zero);

            this.RuleFor(n => n.OrganizationData.Division)
                .MinimumLength(MinDivisionLength)
                .MaximumLength(MaxDivisionLength);

            this.RuleFor(n => n.OrganizationData.Department)
                .MinimumLength(MinDepartmentLength)
                .MaximumLength(MaxDepartmentLength);

            this.RuleFor(n => n.OrganizationData.Unit)
                .MinimumLength(Domain.Models.ModelConstants.OrganizationData.MinUnitLength)
                .MaximumLength(Domain.Models.ModelConstants.OrganizationData.MaxUnitLength);

            this.RuleFor(n => n.OrganizationData.Location)
                .MinimumLength(MinLocationLength)
                .MaximumLength(MaxLocationLength);

            this.RuleFor(n => n.OrganizationData.CostProfitArea)
                .MinimumLength(MinCostProfitAreaLength)
                .MaximumLength(MaxCostProfitAreaLength);

            this.RuleFor(n => n.OrganizationData.CostCenter)
                .MinimumLength(MinCostCenterLength)
                .MaximumLength(MaxCostCenterLength);

            this.RuleFor(n => n.OrganizationData.OfficialPosition)
                .MinimumLength(MinOfficialPositionLength)
                .MaximumLength(MaxOfficialPositionLength);

            this.RuleFor(n => n.OrganizationData.InternalPosition)
                .MinimumLength(MinInternalPositionLength)
                .MaximumLength(MaxInternalPositionLength);

            this.RuleFor(n => n.OrganizationData.DirectManagerAdminReporting)
                .MinimumLength(MinDirectManagerAdminReportingLength)
                .MaximumLength(MaxDirectManagerAdminReportingLength);

            this.RuleFor(n => n.OrganizationData.DirectManagerFunctionalReporting)
                .MinimumLength(MinDirectManagerFunctionalReportingLength)
                .MaximumLength(MaxDirectManagerFunctionalReportingLength);

            this.RuleFor(n => n.OrganizationData.AdditionalComments)
                .MinimumLength(MinCommentLength)
                .MaximumLength(MaxCommentLength);

            this.RuleFor(n => n.OrganizationData.Photo)
                .Must(url => url == null || Uri.IsWellFormedUriString(url, UriKind.Absolute))
                .WithMessage("'{PropertyName}' must be a valid url.");

            this.RuleFor(n => n.OrganizationData.CiriumVitae)
               .Must(url => url == null || Uri.IsWellFormedUriString(url, UriKind.Absolute))
               .WithMessage("'{PropertyName}' must be a valid url.");

            this.RuleFor(n => n.FraudCheck.SmartInfo)
                .MinimumLength(MinSmartInfoLength)
                .MaximumLength(MaxSmartInfoLength);

            this.RuleFor(n => n.InternalSecurityComment)
                .MinimumLength(MinCommentLength)
                .MaximumLength(MaxCommentLength);

            this.RuleFor(n => n.EquipmentAndAccess.WorkingPlaceLocation)
                .MinimumLength(MinWorkingPlaceLocationLength)
                .MaximumLength(MaxWorkingPlaceLocationLength);

            this.RuleFor(n => n.EquipmentAndAccess.LeavingEmployee)
                .MinimumLength(MinLeavingEmployeeLength)
                .MaximumLength(MaxLeavingEmployeeLength);

            this.RuleFor(n => n.EquipmentAndAccess.LeavingEmployee)
                .MinimumLength(MinLeavingEmployeeLength)
                .MaximumLength(MaxLeavingEmployeeLength);

            this.RuleFor(n => n.EquipmentAndAccess.AdditionalEquipment)
                .MinimumLength(MinAdditionalEquipmentLength)
                .MinimumLength(MinAdditionalEquipmentLength);

            this.RuleFor(n => n.RequestAccessToSystems.SmartInfoRoles)
                .MinimumLength(MinRolesLength)
                .MaximumLength(MaxRolesLength);

            this.RuleFor(n => n.RequestAccessToSystems.SCardRoles)
                .MinimumLength(MinRolesLength)
                .MaximumLength(MaxRolesLength);

            this.RuleFor(n => n.RequestAccessToSystems.FOSRoles)
                .MinimumLength(MinRolesLength)
                .MaximumLength(MaxRolesLength);

            this.RuleFor(n => n.RequestAccessToSystems.ICollectRoles)
                .MinimumLength(MinRolesLength)
                .MaximumLength(MaxRolesLength);

            this.RuleFor(n => n.RequestAccessToSystems.SyronRoles)
                .MinimumLength(MinRolesLength)
                .MaximumLength(MaxRolesLength);

            this.RuleFor(n => n.RequestAccessToSystems.AccessLikeUser)
                .MinimumLength(MinRolesLength)
                .MaximumLength(MaxRolesLength);

            this.RuleFor(n => n.RequestAccessToSystems.VCSRoles)
                .MinimumLength(MinRolesLength)
                .MaximumLength(MaxRolesLength);

            this.RuleFor(n => n.RequestAccessToSystems.AccessToOtherSystems)
                .MinimumLength(MinAccessToOtherSystemsLength)
                .MaximumLength(MaxAccessToOtherSystemsLength);

            this.RuleFor(n => n.RequestAccessToSystems.EmailGroups)
                .MinimumLength(MinEmailGroupsLength)
                .MaximumLength(MaxEmailGroupsLength);

            this.RuleFor(n => n.RequestAccessToSystems.SharedFolders)
                .MinimumLength(MinSharedFoldersLength)
                .MaximumLength(MaxSharedFoldersLength);

            this.RuleFor(n => n.AccessToSystemsData.ADUser)
                .MinimumLength(MinUsernameLength)
                .MaximumLength(MaxUsernameLength);

            this.RuleFor(n => n.AccessToSystemsData.ADPass)
                .MinimumLength(MinPasswordLength)
                .MaximumLength(MaxPasswordLength);

            this.RuleFor(n => n.AccessToSystemsData.VCSUser)
                .MinimumLength(MinUsernameLength)
                .MaximumLength(MaxUsernameLength);

            this.RuleFor(n => n.AccessToSystemsData.VCSPass)
                .MinimumLength(MinPasswordLength)
                .MaximumLength(MaxPasswordLength);

            this.RuleFor(n => n.AccessToSystemsData.SmartInfoUser)
                .MinimumLength(MinUsernameLength)
                .MaximumLength(MaxUsernameLength);

            this.RuleFor(n => n.AccessToSystemsData.SmartInfoPass)
                .MinimumLength(MinPasswordLength)
                .MaximumLength(MaxPasswordLength);

            this.RuleFor(n => n.AccessToSystemsData.SCardUser)
                .MinimumLength(MinUsernameLength)
                .MaximumLength(MaxUsernameLength);

            this.RuleFor(n => n.AccessToSystemsData.SCardPass)
                .MinimumLength(MinPasswordLength)
                .MaximumLength(MaxPasswordLength);

            this.RuleFor(n => n.AccessToSystemsData.FOSUser)
                .MinimumLength(MinUsernameLength)
                .MaximumLength(MaxUsernameLength);

            this.RuleFor(n => n.AccessToSystemsData.FOSPass)
                .MinimumLength(MinPasswordLength)
                .MaximumLength(MaxPasswordLength);

            this.RuleFor(n => n.AccessToSystemsData.SyronUser)
                .MinimumLength(MinUsernameLength)
                .MaximumLength(MaxUsernameLength);

            this.RuleFor(n => n.AccessToSystemsData.SyronPass)
                .MinimumLength(MinPasswordLength)
                .MaximumLength(MaxPasswordLength);

            this.RuleFor(n => n.AccessToSystemsData.ICollectUser)
                .MinimumLength(MinUsernameLength)
                .MaximumLength(MaxUsernameLength);

            this.RuleFor(n => n.AccessToSystemsData.ICollectPass)
                .MinimumLength(MinPasswordLength)
                .MaximumLength(MaxPasswordLength);

            this.RuleFor(n => n.AccessToSystemsData.UserEmail)
                .MinimumLength(MinEmailLength)
                .MaximumLength(MaxEmailLength);

            this.RuleFor(n => n.AccessToSystemsData._UpdateAdditionalAccessesRequested)
                .MinimumLength(MinCommentLength)
                .MaximumLength(MaxCommentLength);

            this.RuleFor(n => n.IBAN)
                .Length(IBANLength);
            //(Plamena)
        }
    }
}
