﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Common;
using HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update.InputModels;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Update
{
    public class UpdateNewEmployeeCommand : NewEmployeeCommand<UpdateNewEmployeeCommand>, IRequest<Result>
    {
        public int ProbationPeriod { get; set; }
        public int NoticePeriod { get; set; }
        public bool OrgAnnouncement { get; set; }
        public string Gender { get; set; } = default!;
        public string DateOfBirth { get; set; } = default!;
        public string EmploymentHistory { get; set; } = default!;
        public AddressInputModel Address { get; set; } = default!;
        public StartDocumentsInputModel StartDocuments { get; set; } = default!;
        public OrganizationDataInputModel OrganizationData { get; set; } = default!;
        public FraudCheckInputModel FraudCheck { get; set; } = default!;
        public OpenSourcesCheckInputModel OpenSourcesCheck { get; set; } = default!;
        public string InternalSecurityComment { get; set; } = default!;
        public EquipmentAndAccessInputModel EquipmentAndAccess { get; set; } = default!;
        public RequestAccessToSystemsInputModel RequestAccessToSystems { get; set; } = default!;
        public AccessToSystemsDataOutputModel AccessToSystemsData { get; set; } = default!;
        public string IBAN { get; set; } = default!;
        public bool AllPaymentDocumentsRecieved { get; set; }
        public StartDocumentsSignedInputModel StartDocumentsSigned { get; set; } = default!;

        public class UpdateNewEmployeeCommandHandler : IRequestHandler<UpdateNewEmployeeCommand, Result>
        {
            private readonly IRequestRepository requestRepository;
            private readonly ICurrentUser currentUser;

            public UpdateNewEmployeeCommandHandler(
                IRequestRepository requestRepository,
                ICurrentUser currentUser)
            {
                this.requestRepository = requestRepository;
                this.currentUser = currentUser;
            }

            public async Task<Result> Handle(
                UpdateNewEmployeeCommand request,
                CancellationToken cancellationToken)
            {
                var newEmployeeRequest = (NewEmployeeRequest)(await this.requestRepository
                    .Find(request.Id, request.Category, cancellationToken));

                if (newEmployeeRequest == null)
                {
                    return false;
                }
                var userRole = this.currentUser.UserRoles;
                var step = await this.requestRepository.SearchForStep(newEmployeeRequest.Category, newEmployeeRequest.Status, cancellationToken);

                if (userRole.Contains(step.CurrentRole))
                {
                    Enum.TryParse(request.Status, out Domain.Models.Enums.Status parsedStatus);
                    Enum.TryParse(request.PlaceOfWork, out Domain.Models.Enums.PlaceOfWork parsedPlaceOfWork);
                    Enum.TryParse(request.EmployeeType, out Domain.Models.Enums.EmployeeType parsedEmployeeType);
                    Enum.TryParse(request.Gender, out Domain.Models.Enums.Gender parsedGender);
                    Enum.TryParse(request.EmploymentHistory, out Domain.Models.Enums.EmploymentHistory parsedEmploymentHistory);

                    Enum.TryParse(request.StartDocuments.ReportingLevel, out Domain.Models.Enums.ReportingLevel parsedRepotingLevel);

                    Enum.TryParse(request.OrganizationData.Bonus, out Domain.Models.Enums.BonusEligibility parsedBonusEligibility);

                    Enum.TryParse(request.FraudCheck.CentralCreditRecord, out Domain.Models.Enums.CentralCreditRecord parsedCentralCreditRecord);

                    Enum.TryParse(request.EquipmentAndAccess.WorkingPlace, out Domain.Models.Enums.WorkingPlace parsedWorkingPlace);
                    Enum.TryParse(request.EquipmentAndAccess.MobilePhone, out Domain.Models.Enums.MobilePhone parsedMobilePhone);
                    Enum.TryParse(request.EquipmentAndAccess.PrefferedEquipment, out Domain.Models.Enums.PrefferedEquipment parsedPrefferedEquipment);

                    Enum.TryParse(request.RequestAccessToSystems.PhoneRecievingVCS, out Domain.Models.Enums.PhoneRecieving parsedPhoneRecievingVCS);

                    Enum.TryParse(request.AccessToSystemsData.EquipmentOnFirstDay, out Domain.Models.Enums.YesNoNotApproved parsedEquipmentOnFirstDay);

                    newEmployeeRequest
                    .UpdateAccessToSystemsData(
                        request.AccessToSystemsData.ADUser,
                        request.AccessToSystemsData.ADPass,
                        request.AccessToSystemsData.VCSUser,
                        request.AccessToSystemsData.VCSPass,
                        request.AccessToSystemsData.SmartInfoUser,
                        request.AccessToSystemsData.SmartInfoPass,
                        request.AccessToSystemsData.SCardUser,
                        request.AccessToSystemsData.SCardPass,
                        request.AccessToSystemsData.FOSUser,
                        request.AccessToSystemsData.FOSPass,
                        request.AccessToSystemsData.SyronUser,
                        request.AccessToSystemsData.SyronPass,
                        request.AccessToSystemsData.ICollectUser,
                        request.AccessToSystemsData.ICollectPass,
                        request.AccessToSystemsData.UserEmail,
                        request.AccessToSystemsData.CustomerView,
                        parsedEquipmentOnFirstDay,
                        request.AccessToSystemsData.AdditionalEquipmentApproved,
                        request.AccessToSystemsData.AccessToSharedFolders,
                        request.AccessToSystemsData.AddedToGroupEmails,
                        request.AccessToSystemsData._UpdateAdditionalAccessesRequested)
                    .UpdateAddress(
                        request.Address.Country,
                        request.Address.City,
                        request.Address.Street,
                        request.Address.StreetNumber,
                        request.Address.Appartment,
                        request.Address.Unit,
                        request.Address.Floor)
                    .UpdateAllPaymentDocumentsRecieved(
                        request.AllPaymentDocumentsRecieved)
                    .UpdateContactInformation(
                        request.PhoneNumber,
                        request.Email)
                    .UpdateDateOfBirth(
                        string.IsNullOrEmpty(request.DateOfBirth) ? DateTime.MinValue :
                                                                    DateTime.Parse(request.DateOfBirth))
                    .UpdateEmployeeType(parsedEmployeeType)
                    .UpdateEmploymentHistory(parsedEmploymentHistory)
                    .UpdateEquipmentAndAccess(
                        parsedMobilePhone,
                        request.EquipmentAndAccess.Uniform,
                        request.EquipmentAndAccess.CompanyCar,
                        parsedPrefferedEquipment,
                        request.EquipmentAndAccess.VPN,
                        request.EquipmentAndAccess.AdditionalEquipment,
                        parsedWorkingPlace,
                        request.EquipmentAndAccess.StationaryPhone,
                        request.EquipmentAndAccess.WorkingPlaceLocation,
                        request.EquipmentAndAccess.LeavingEmployee)
                    .UpdateFraudCheck(
                        request.FraudCheck.CriminalRecord,
                        parsedCentralCreditRecord,
                        request.FraudCheck.BlackList,
                        request.FraudCheck.ValidateInformation,
                        request.FraudCheck.SmartInfo)
                    .UpdateGender(parsedGender)
                    .UpdateIBAN(request.IBAN)
                    .UpdateInternalSecurityComment(request.InternalSecurityComment)
                    .UpdateNoticePeriod(request.NoticePeriod)
                    .UpdateOpenSourcesCheck(
                        request.OpenSourcesCheck.Posts,
                        request.OpenSourcesCheck.PictureSearch,
                        request.OpenSourcesCheck.GoogleCheck,
                        request.OpenSourcesCheck.HateGroups,
                        request.OpenSourcesCheck.AccessCard,
                        request.OpenSourcesCheck.InternalSecurity)
                    .UpdateOrganizationData(
                        request.OrganizationData.Division,
                        request.OrganizationData.Department,
                        request.OrganizationData.Unit,
                        request.OrganizationData.Location,
                        request.OrganizationData.CostProfitArea,
                        request.OrganizationData.CostCenter,
                        request.OrganizationData.OfficialPosition,
                        request.OrganizationData.InternalPosition,
                        request.OrganizationData.DirectManagerAdminReporting,
                        request.OrganizationData.DirectManagerFunctionalReporting,
                        parsedBonusEligibility,
                        request.OrganizationData.AdditionalComments,
                        request.OrganizationData.Photo,
                        request.OrganizationData.CiriumVitae)
                    .UpdateOrgAnnouncement(request.OrgAnnouncement)
                    .UpdatePersonalInformation(
                        request.FirstName,
                        request.MiddleName,
                        request.LastName,
                        request.IdentificationNumber)
                    .UpdatePlaceOfWork(parsedPlaceOfWork)
                    .UpdateProbationPeriod(request.ProbationPeriod)
                    .UpdateRequestAccessToSystems(
                        request.RequestAccessToSystems.OperationalRisk,
                        request.RequestAccessToSystems.SmartInfo,
                        request.RequestAccessToSystems.SmartInfoRoles,
                        request.RequestAccessToSystems.SCard,
                        request.RequestAccessToSystems.SCardRoles,
                        request.RequestAccessToSystems.FOS,
                        request.RequestAccessToSystems.FOSRoles,
                        request.RequestAccessToSystems.CeGate,
                        request.RequestAccessToSystems.ICollect,
                        request.RequestAccessToSystems.ICollectRoles,
                        request.RequestAccessToSystems.Syron,
                        request.RequestAccessToSystems.SyronRoles,
                        request.RequestAccessToSystems.AccessLikeUser,
                        request.RequestAccessToSystems.VCS,
                        request.RequestAccessToSystems.VCSRoles,
                        parsedPhoneRecievingVCS,
                        request.RequestAccessToSystems.CustomerView,
                        request.RequestAccessToSystems.AccessToOtherSystems,
                        request.RequestAccessToSystems.AccessToEmailByMobile,
                        request.RequestAccessToSystems.EmailGroups,
                        request.RequestAccessToSystems.SharedFolders)
                    .UpdateStartDocuments(
                        parsedRepotingLevel,
                        request.StartDocuments.ProbationPeriodValidation,
                        request.StartDocuments.DeclarationGDPR,
                        request.StartDocuments.LabourBook,
                        request.StartDocuments.MedicalCertificate,
                        request.StartDocuments.Diploma,
                        request.StartDocuments.SignedJobOffer,
                        request.StartDocuments.JobDescription)
                    .UpdateStartDocumentsSigned(
                        request.StartDocumentsSigned.SignedJobDescription,
                        request.StartDocumentsSigned.SignedLabourContract,
                        request.StartDocumentsSigned.PCCCheck,
                        string.IsNullOrEmpty(request.StartDocumentsSigned.PCCCheckDate) ? DateTime.MinValue :
                                                                                          DateTime.Parse(request.StartDocumentsSigned.PCCCheckDate),
                        request.StartDocumentsSigned.InstructionNoteHS,
                        request.StartDocumentsSigned.NRA,
                        request.StartDocumentsSigned.VideoSurveillance,
                        request.StartDocumentsSigned.GDPR,
                        request.StartDocumentsSigned.GDPRPhotosVideo)
                    .UpdateStatus(parsedStatus)
                    .UpdateCategory(request.Category)
                    .UpdateCategoryGroup(request.CategoryGroup);

                    await this.requestRepository.Save(newEmployeeRequest, cancellationToken);
                }
                else
                {
                    return Result.Failure(new List<string>() { $"User must be in {step.CurrentRole} role, to edit this request." });
                }

                return Result.Success;
            }
        }
    }
}
