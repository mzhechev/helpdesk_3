﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Domain.Exceptions;
using HelpDesk_3.Domain.Models.Enums;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Delete
{
    public class DeleteNewEmployeeCommand : EntityCommand<Guid>, IRequest<Result>
    {
        public string Category { get; set; } = default!;

        public class DeleteNewEmployeeCommandHandler : IRequestHandler<DeleteNewEmployeeCommand, Result>
        {
            private readonly IRequestRepository requestRepository;

            public DeleteNewEmployeeCommandHandler(IRequestRepository requestRepository)
                => this.requestRepository = requestRepository;

            public async Task<Result> Handle(
                DeleteNewEmployeeCommand request,
                CancellationToken cancellationToken)
            {
                var newEmployeeRequest = await this.requestRepository
                    .Find(request.Id, request.Category, cancellationToken);

                if (newEmployeeRequest == null)
                {
                    throw new InvalidRequestException("There is no NewEmployee Request with the given Id.");
                }

                newEmployeeRequest.UpdateStatus(Status.Deleted);

                await this.requestRepository.Save(newEmployeeRequest, cancellationToken);

                return Result.Success;
            }
        }
    }
}
