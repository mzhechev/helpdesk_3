﻿using HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Common;
using HelpDesk_3.Domain.Factories.Requests.NewEmployee;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Create
{
    public class CreateNewEmployeeCommand : NewEmployeeCommand<CreateNewEmployeeCommand>, IRequest<CreateNewEmployeeOutputModel>
    {
        public class CreateNewEmployeeCommandHandler : IRequestHandler<CreateNewEmployeeCommand, CreateNewEmployeeOutputModel>
        {
            private readonly INewEmployeeRequestFactory newEmployeeRequestFactory;
            private readonly IRequestRepository requestRepository;

            public CreateNewEmployeeCommandHandler(
                INewEmployeeRequestFactory newEmployeeRequestFactory,
                IRequestRepository requestRepository)
            {
                this.newEmployeeRequestFactory = newEmployeeRequestFactory;
                this.requestRepository = requestRepository;
            }

            public async Task<CreateNewEmployeeOutputModel> Handle(
                CreateNewEmployeeCommand request, 
                CancellationToken cancellationToken)
            {
                Enum.TryParse(request.Status, out Domain.Models.Enums.Status parsedStatus);
                Enum.TryParse(request.EmployeeType, out Domain.Models.Enums.EmployeeType parsedEmployeeType);
                Enum.TryParse(request.PlaceOfWork, out Domain.Models.Enums.PlaceOfWork parsedPlaceOfWork);

                var newEmployeeRequest = this.newEmployeeRequestFactory
                    .WithRequestData(
                        request.Category,
                        request.CategoryGroup,
                        parsedStatus)
                    .WithEmployeeType(parsedEmployeeType)
                    .WithPlaceOfWork(parsedPlaceOfWork)
                    .WithPersonalInformation(
                        request.FirstName)
                    .WithContractInformation(
                        request.PhoneNumber,
                        request.Email)
                    .Build();

                await this.requestRepository.Save(newEmployeeRequest, cancellationToken);

                return new CreateNewEmployeeOutputModel(
                    newEmployeeRequest.Status.ToString(),
                    newEmployeeRequest.Id);
            }
        }
    }
}
