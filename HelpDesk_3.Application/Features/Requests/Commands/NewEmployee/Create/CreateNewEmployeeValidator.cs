﻿using FluentValidation;
using HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Common;

namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Create
{
    internal class CreateNewEmployeeValidator : AbstractValidator<CreateNewEmployeeCommand>
    {
        public CreateNewEmployeeValidator()
            => this.Include(new NewEmployeeCommandValidator<CreateNewEmployeeCommand>());
    }
}