﻿namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Create
{
    public class CreateNewEmployeeOutputModel
    {
        public CreateNewEmployeeOutputModel(
            string status,
            Guid newEmployeeRequestId)
        {
            this.Status = status;
            this.NewEmployeeRequestId = newEmployeeRequestId;
        }

        public string Status { get; set; }
        public Guid NewEmployeeRequestId { get; set; }
    }
}
