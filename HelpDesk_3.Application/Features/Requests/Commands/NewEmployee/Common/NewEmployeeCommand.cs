﻿using HelpDesk_3.Domain.Models.Enums;

namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Common
{
    public class NewEmployeeCommand<TCommand> : EntityCommand<Guid>
        where TCommand : EntityCommand<Guid>
    {
        public string Category { get; set; } = default!;
        public string CategoryGroup { get; set; } = default!;
        public string Status { get; set; } = default!;
        public string FirstName { get; set; } = default!;
        public string MiddleName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string IdentificationNumber { get; set; } = default!;
        public string PhoneNumber { get; set; } = default!;
        public string Email { get; set; } = default!;
        public string EmployeeType { get; set; } = default!;
        public string PlaceOfWork { get; set; } = default!;
    }
}
