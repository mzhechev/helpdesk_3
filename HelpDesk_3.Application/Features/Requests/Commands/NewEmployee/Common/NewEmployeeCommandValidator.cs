﻿using FluentValidation;

using static HelpDesk_3.Domain.Models.ModelConstants.Categories;
using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Application.Features.Requests.Commands.NewEmployee.Common
{
    internal class NewEmployeeCommandValidator<TCommand> : AbstractValidator<NewEmployeeCommand<TCommand>>
        where TCommand : EntityCommand<Guid>
    {
        public NewEmployeeCommandValidator()
        {
            this.RuleFor(n => n.Category)
                .MinimumLength(MinCategoriesLength)
                .MaximumLength(MaxCategoriesLength);

            this.RuleFor(n => n.CategoryGroup)
                .MinimumLength(MinCategoriesLength)
                .MaximumLength(MaxCategoriesLength);

            this.RuleFor(c => c.FirstName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(c => c.MiddleName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(c => c.LastName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(c => c.IdentificationNumber)
                .Length(EGNLength);

            this.RuleFor(c => c.PhoneNumber)
                .Length(PhoneLength);

            this.RuleFor(c => c.Email)
                .MinimumLength(MinEmailLength)
                .MaximumLength(MaxEmailLength);
        }
    }
}
