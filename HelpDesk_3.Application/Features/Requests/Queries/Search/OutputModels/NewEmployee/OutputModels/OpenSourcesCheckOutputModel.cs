﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class OpenSourcesCheckOutputModel : IMapFrom<OpenSourcesCheck>
    {
        public bool Posts { get; private set; }
        public bool PictureSearch { get; private set; }
        public bool GoogleCheck { get; private set; }
        public bool HateGroups { get; private set; }
        public bool AccessCard { get; private set; }
        public bool InternalSecurity { get; private set; }
    }
}
