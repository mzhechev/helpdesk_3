﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class FraudCheckOutputModel : IMapFrom<FraudCheck>
    {
        public bool CriminalRecord { get; private set; }
        public string CentralCreditRecord { get; private set; } = default!;
        public bool BlackList { get; private set; }
        public bool ValidateInformation { get; private set; }
        public string SmartInfo { get; private set; } = default!;
    }
}
