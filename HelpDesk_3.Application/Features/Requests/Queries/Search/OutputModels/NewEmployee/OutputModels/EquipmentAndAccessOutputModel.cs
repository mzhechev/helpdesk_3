﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class EquipmentAndAccessOutputModel : IMapFrom<EquipmentAndAccess>
    {
        public string WorkingPlace { get; private set; } = default!;
        public bool StationaryPhone { get; private set; }
        public string WorkingPlaceLocation { get; private set; } = default!;
        public string LeavingEmployee { get; private set; } = default!;
        public string MobilePhone { get; private set; } = default!;
        public bool Uniform { get; private set; }
        public bool CompanyCar { get; private set; }
        public string PrefferedEquipment { get; private set; } = default!;
        public bool VPN { get; private set; }
        public string AdditionalEquipment { get; private set; } = default!;
    }
}
