﻿using AutoMapper;
using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class AddressOutputModel : IMapFrom<Address>
    {
        public string Country { get; private set; } = default!;
        public string City { get; private set; } = default!;
        public string Street { get; private set; } = default!;
        public int StreetNumber { get; private set; }
        public int Appartment { get; private set; }
        public int Unit { get; private set; }
        public int Floor { get; private set; }
    }
}
