﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class OrganizationDataOutputModel : IMapFrom<OrganizationData> 
    {
        public string Division { get; private set; } = default!;
        public string Department { get; private set; } = default!;
        public string Unit { get; private set; } = default!;
        public string Location { get; private set; } = default!;
        public string CostProfitArea { get; private set; } = default!;
        public string CostCenter { get; private set; } = default!;
        public string OfficialPosition { get; private set; } = default!;
        public string InternalPosition { get; private set; } = default!;
        public string DirectManagerAdminReporting { get; private set; } = default!;
        public string DirectManagerFunctionalReporting { get; private set; } = default!;
        public string Bonus { get; private set; } = default!;
        public string AdditionalComments { get; private set; } = default!;
        public string Photo { get; private set; } = default!;
        public string CiriumVitae { get; private set; } = default!;
    }
}
