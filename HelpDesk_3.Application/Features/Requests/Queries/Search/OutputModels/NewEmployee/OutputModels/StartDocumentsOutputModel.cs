﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class StartDocumentsOutputModel : IMapFrom<StartDocuments>
    {
        public string ReportingLevel { get; private set; } = default!;
        public bool ProbationPeriodValidation { get; private set; }
        public bool DeclarationGDPR { get; private set; }
        public bool LabourBook { get; private set; }
        public bool MedicalCertificate { get; private set; }
        public bool Diploma { get; private set; }
        public bool SignedJobOffer { get; private set; }
        public bool JobDescription { get; private set; }
    }
}
