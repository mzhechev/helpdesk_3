﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class StartDocumentsSignedOutputModel : IMapFrom<StartDocumentsSigned>
    {
        public bool SignedJobDescription { get; private set; }
        public bool SignedLabourContract { get; private set; }
        public bool PCCCheck { get; private set; }
        public string PCCCheckDate { get; private set; } = default!;
        public bool InstructionNoteHS { get; private set; }
        public bool NRA { get; private set; }
        public bool VideoSurveillance { get; private set; }
        public bool GDPR { get; private set; }
        public bool GDPRPhotosVideo { get; private set; }
    }
}
