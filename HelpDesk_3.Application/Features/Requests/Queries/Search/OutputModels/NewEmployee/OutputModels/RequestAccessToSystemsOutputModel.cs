﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class RequestAccessToSystemsOutputModel : IMapFrom<RequestAccessToSystems>
    {
        public bool OperationalRisk { get; private set; }
        public bool SmartInfo { get; private set; }
        public string SmartInfoRoles { get; private set; } = default!;
        public bool SCard { get; private set; }
        public string SCardRoles { get; private set; } = default!;
        public bool FOS { get; private set; }
        public string FOSRoles { get; private set; } = default!;
        public bool CeGate { get; private set; }
        public bool ICollect { get; private set; }
        public string ICollectRoles { get; private set; } = default!;
        public bool Syron { get; private set; }
        public string SyronRoles { get; private set; } = default!;
        public string AccessLikeUser { get; private set; } = default!;
        public bool VCS { get; private set; }
        public string VCSRoles { get; private set; } = default!;
        public string PhoneRecievingVCS { get; private set; } = default!;
        public bool CustomerView { get; private set; }
        public string AccessToOtherSystems { get; private set; } = default!;
        public bool AccessToEmailByMobile { get; private set; }
        public string EmailGroups { get; private set; } = default!;
        public string SharedFolders { get; private set; } = default!;
    }
}
