﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class AccessToSystemsDataOutputModel : IMapFrom<AccessToSystemsData>
    {
        public string ADUser { get; private set; } = default!;
        public string ADPass { get; private set; } = default!;
        public string VCSUser { get; private set; } = default!;
        public string VCSPass { get; private set; } = default!;
        public string SmartInfoUser { get; private set; } = default!;
        public string SmartInfoPass { get; private set; } = default!;
        public string SCardUser { get; private set; } = default!;
        public string SCardPass { get; private set; } = default!;
        public string FOSUser { get; private set; } = default!;
        public string FOSPass { get; private set; } = default!;
        public string SyronUser { get; private set; } = default!;
        public string SyronPass { get; private set; } = default!;
        public string ICollectUser { get; private set; } = default!;
        public string ICollectPass { get; private set; } = default!;
        public string UserEmail { get; private set; } = default!;
        public bool CustomerView { get; private set; }
        public YesNoNotApproved EquipmentOnFirstDay { get; private set; } = default!;
        public bool AdditionalEquipmentApproved { get; private set; }
        public bool AccessToSharedFolders { get; private set; }
        public bool AddedToGroupEmails { get; private set; }
        public string _UpdateAdditionalAccessesRequested { get; private set; } = default!;
    }
}
