﻿using AutoMapper;
using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Requests.Request.NewEmployee;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels.NewEmployee
{
    public class NewEmployeeRequestOutputModel : RequestOutputModel, IMapFrom<NewEmployeeRequest>
    {
        public string FirstName { get; private set; } = default!;
        public string MiddleName { get; private set; } = default!;
        public string LastName { get; private set; } = default!;
        public string IdentificationNumber { get; private set; } = default!;
        public string PhoneNumber { get; private set; } = default!;
        public string Email { get; private set; } = default!;
        public string EmployeeType { get; private set; } = default!;
        public string PlaceOfWork { get; private set; } = default!;
        public int ProbationPeriod { get; set; }
        public int NoticePeriod { get; set; }
        public bool OrgAnnouncement { get; set; }
        public string Gender { get; set; } = default!;
        public string DateOfBirth { get; set; } = default!;
        public string EmploymentHistory { get; set; } = default!;
        public AddressOutputModel Address { get; set; } = default!;
        public StartDocumentsOutputModel StartDocuments { get; set; } = default!;
        public OrganizationDataOutputModel OrganizationData { get; set; } = default!;
        public FraudCheckOutputModel FraudCheck { get; set; } = default!;
        public OpenSourcesCheckOutputModel OpenSourcesCheck { get; set; } = default!;
        public string InternalSecurityComment { get; set; } = default!;
        public EquipmentAndAccessOutputModel EquipmentAndAccess { get; set; } = default!;
        public RequestAccessToSystemsOutputModel RequestAccessToSystems { get; set; } = default!;
        public AccessToSystemsDataOutputModel AccessToSystemsData { get; set; } = default!;
        public string IBAN { get; set; } = default!;
        public bool AllPaymentDocumentsRecieved { get; set; }
        public StartDocumentsSignedOutputModel StartDocumentsSigned { get; set; } = default!;

        public virtual void Mapping(Profile mapper)
          => mapper
              .CreateMap<NewEmployeeRequest, NewEmployeeRequestOutputModel>()
              .ForMember(f => f.FirstName, cfg => cfg
                  .MapFrom(f => f.PersonalInformation.FirstName))
              .ForMember(m => m.MiddleName, cfg => cfg
                  .MapFrom(m => m.PersonalInformation.MiddleName))
              .ForMember(l => l.LastName, cfg => cfg
                  .MapFrom(l => l.PersonalInformation.LastName))
              .ForMember(i => i.IdentificationNumber, cfg => cfg
                  .MapFrom(i => i.PersonalInformation.IdentificationNumber))
              .ForMember(p => p.PhoneNumber, cfg => cfg
                  .MapFrom(c => c.ContactInformation.PhoneNumber))
              .ForMember(e => e.Email, cfg => cfg
                  .MapFrom(c => c.ContactInformation.Email));
    }
}
