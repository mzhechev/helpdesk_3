﻿using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels
{
    public class RequestOutputModel : IMapFrom<RequestData>
    {
        public Guid Id { get; private set; } = default!;
        public string Category { get; private set; } = default!;
        public string CategoryGroup { get; private set; } = default!;
        public Status Status { get; private set; } = default!;
    }
}
