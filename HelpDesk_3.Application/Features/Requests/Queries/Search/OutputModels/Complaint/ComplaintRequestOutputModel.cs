﻿using AutoMapper;
using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests.Request.Complaint;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels.Complaint
{
    public class ComplaintRequestOutputModel : RequestOutputModel
    {
        public string FirstName { get; private set; } = default!;
        public string MiddleName { get; private set; } = default!;
        public string LastName { get; private set; } = default!;
        public string IdentificationNumber { get; private set; } = default!;
        public string ContractNumber { get; private set; } = default!;
        public string CustomerId { get; private set; } = default!;
        public string PhoneNumber { get; private set; } = default!;
        public string Email { get; private set; } = default!;
        public DateTime ContractIssuingDate { get; private set; } = default!;
        public ContractStatus ContractStatus { get; private set; } = default!;
        public TypeOfLoan TypeOfLoan { get; private set; } = default!;
        public string Merchant { get; private set; } = default!;
        public string NotificationType { get; private set; } = default!;
        public string Content { get; set; } = default!;
        public DateTime ComplaintDate { get; private set; } = default!;
        public DateTime ResponseDate { get; private set; } = default!;
        public string SourceOfComplaint { get; private set; } = default!;
        public string MethodOfReceivingComplaint { get; private set; } = default!;
        public string SubjectOfComplaint { get; private set; } = default!;
        public string DetailsOfComplaint { get; private set; } = default!;
        public string CommunicationHistory { get; private set; } = default!;
        public string NumberOfClientContracts { get; private set; } = default!;
        public string NameOfClientContracts { get; private set; } = default!;
        public bool ReturnToPreviousClaim { get; private set; } = default!;
        public string SolvedInFavor { get; private set; } = default!;
        public bool PVCC { get; private set; } = default!;
        public string AmountPVCCRON { get; private set; } = default!;
        public string RegistrationNumber { get; private set; } = default!;
        public int ResponseDays { get; private set; } = default!;
        public string Remarks { get; private set; } = default!;

        public virtual void Mapping(Profile mapper)
          => mapper
              .CreateMap<ComplaintRequest, ComplaintRequestOutputModel>()
              .ForMember(f => f.FirstName, cfg => cfg
                  .MapFrom(f => f.PersonalInformation.FirstName))
              .ForMember(m => m.MiddleName, cfg => cfg
                  .MapFrom(m => m.PersonalInformation.MiddleName))
              .ForMember(l => l.LastName, cfg => cfg
                  .MapFrom(l => l.PersonalInformation.LastName))
              .ForMember(i => i.IdentificationNumber, cfg => cfg
                  .MapFrom(i => i.PersonalInformation.IdentificationNumber))
              .ForMember(p => p.PhoneNumber, cfg => cfg
                  .MapFrom(c => c.ContactInformation.PhoneNumber))
              .ForMember(e => e.Email, cfg => cfg
                  .MapFrom(c => c.ContactInformation.Email))
              .ForMember(s => s.SourceOfComplaint, cfg => cfg
                  .MapFrom(c => c.ComplaintInformation.SourceOfComplaint))
              .ForMember(m => m.MethodOfReceivingComplaint, cfg => cfg
                  .MapFrom(c => c.ComplaintInformation.MethodOfReceivingComplaint))
              .ForMember(s => s.SubjectOfComplaint, cfg => cfg
                  .MapFrom(c => c.ComplaintInformation.SubjectOfComplaint))
              .ForMember(d => d.DetailsOfComplaint, cfg => cfg
                  .MapFrom(c => c.ComplaintInformation.DetailsOfComplaint));
    }
}
