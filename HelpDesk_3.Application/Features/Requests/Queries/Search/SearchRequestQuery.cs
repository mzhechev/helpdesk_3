﻿using HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Queries.Search
{
    public class SearchRequestQuery : IRequest<RequestOutputModel>
    {
        public Guid Id { get; set; }

        public string Category { get; set; } = default!;

        public class SearchRequestQueryHandler : IRequestHandler<SearchRequestQuery, RequestOutputModel>
        {
            private readonly IRequestRepository requestRepository;

            public SearchRequestQueryHandler(IRequestRepository requestRepository)
                => this.requestRepository = requestRepository;

            public async Task<RequestOutputModel> Handle(
                SearchRequestQuery request, 
                CancellationToken cancellationToken)
            {
                return await this.requestRepository.SearchById(
                    request.Id,
                    request.Category,
                    cancellationToken);
            }
        }
    }
}
