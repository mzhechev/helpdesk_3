﻿namespace HelpDesk_3.Application.Features.Requests.Queries.SearchStep.OutputModels
{
    public class StepOutputModel
    {
        public string Category { get; private set; } = default!;

        public string CurrentRole { get; private set; } = default!;

        public string NextRole { get; private set; } = default!;

        public bool CanStart { get; private set; }

        public bool CanFinish { get; private set; }

        public bool CanEdit { get; private set; }

        public string Currentstring { get; private set; } = default!;

        public string NextStatus { get; private set; } = default!;

        public string Button { get; private set; } = default!;

        public bool ButtonVisible { get; private set; }
    }
}
