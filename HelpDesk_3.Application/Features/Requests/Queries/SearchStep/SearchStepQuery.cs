﻿using HelpDesk_3.Application.Features.Requests.Queries.SearchStep.OutputModels;
using MediatR;

namespace HelpDesk_3.Application.Features.Requests.Queries.SearchStep
{
    public class SearchStepQuery : IRequest<StepOutputModel>
    {
        public string Category { get; set; } = default!;

        public string CurrentStep { get; set; } = default!;

        public class SearchStepQueryHandler : IRequestHandler<SearchStepQuery, StepOutputModel>
        {
            public Task<StepOutputModel> Handle(
                SearchStepQuery request, 
                CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }
        }
    }
}
