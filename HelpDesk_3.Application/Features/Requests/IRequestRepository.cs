﻿using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Requests.Queries.Search.OutputModels;
using HelpDesk_3.Domain.Models.Enums;
using HelpDesk_3.Domain.Models.Requests;
using HelpDesk_3.Domain.Models.Requests.RequestLogistic;

namespace HelpDesk_3.Application.Features.Requests
{
    public interface IRequestRepository : IRepository<RequestData>
    {
        Task<RequestData> Find(Guid id, string category, CancellationToken cancellationToken);

        Task<RequestOutputModel> SearchById(Guid id, string category, CancellationToken cancellationToken);

        Task<Step> SearchForStep(string category, Status currentStatus, CancellationToken cancellationToken);
    }
}
