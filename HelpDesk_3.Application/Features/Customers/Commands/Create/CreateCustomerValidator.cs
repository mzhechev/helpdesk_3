﻿using FluentValidation;
using HelpDesk_3.Application.Features.Customers.Commands.Common;

namespace HelpDesk_3.Application.Features.Customers.Commands.Create
{
    internal class CreateCustomerValidator : AbstractValidator<CreateCustomerCommand>
    {
        public CreateCustomerValidator()
            => this.Include(new CustomerCommandValidator<CreateCustomerCommand>());
    }
}
