﻿using HelpDesk_3.Application.Features.Customers.Commands.Common;
using HelpDesk_3.Domain.Factories.Customers;
using MediatR;

namespace HelpDesk_3.Application.Features.Customers.Commands.Create
{
    public class CreateCustomerCommand : CustomerCommand<CreateCustomerCommand>, IRequest<CreateCustomerOutputModel>
    {
        public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, CreateCustomerOutputModel>
        {
            private readonly ICustomerFactory customerFactory;
            private readonly ICustomerRepository customerRepository;

            public CreateCustomerCommandHandler(
                ICustomerFactory customerFactory,
                ICustomerRepository customerRepository)
            {
                this.customerFactory = customerFactory;
                this.customerRepository = customerRepository;
            }

            public async Task<CreateCustomerOutputModel> Handle(
                CreateCustomerCommand request, 
                CancellationToken cancellationToken)
            {
                var customer = this.customerFactory
                    .WithPersonalInformation(
                        request.FirstName,
                        request.MiddleName,
                        request.LastName,
                        request.IdentificationNumber)
                    .Build();

                await this.customerRepository.Save(customer, cancellationToken);

                return new CreateCustomerOutputModel(customer.Id);
            }
        }
    }
}
