﻿namespace HelpDesk_3.Application.Features.Customers.Commands.Create
{
    public class CreateCustomerOutputModel
    {
        public CreateCustomerOutputModel(Guid customerId)
            => this.CustomerId = customerId;
        public Guid CustomerId { get; set; }
    }
}
