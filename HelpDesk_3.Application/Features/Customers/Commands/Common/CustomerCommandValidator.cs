﻿using FluentValidation;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Application.Features.Customers.Commands.Common
{
    public class CustomerCommandValidator<TCommand> : AbstractValidator<CustomerCommand<TCommand>>
        where TCommand : EntityCommand<Guid>
    {
        public CustomerCommandValidator()
        {
            this.RuleFor(f => f.FirstName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(m => m.MiddleName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(l => l.LastName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(i => i.IdentificationNumber);
        }
    }
}
