﻿namespace HelpDesk_3.Application.Features.Customers.Commands.Common
{
    public class CustomerCommand<TCommand> : EntityCommand<Guid>
        where TCommand : EntityCommand<Guid>
    {
        public string FirstName { get; set; } = default!;

        public string MiddleName { get; set; } = default!;

        public string LastName { get; set; } = default!;

        public string IdentificationNumber { get; set; } = default!;
    }
}
