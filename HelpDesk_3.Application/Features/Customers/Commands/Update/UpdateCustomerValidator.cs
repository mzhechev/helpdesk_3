﻿using FluentValidation;
using HelpDesk_3.Application.Features.Customers.Command.Update;
using HelpDesk_3.Application.Features.Customers.Commands.Common;

namespace HelpDesk_3.Application.Features.Customers.Commands.Update
{
    internal class UpdateCustomerValidator : AbstractValidator<UpdateCustomerCommand>
    {
        public UpdateCustomerValidator()
            => this.Include(new CustomerCommandValidator<UpdateCustomerCommand>());
    }
}
