﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Application.Features.Customers.Commands.Common;
using MediatR;

namespace HelpDesk_3.Application.Features.Customers.Command.Update
{
    public class UpdateCustomerCommand : CustomerCommand<UpdateCustomerCommand>, IRequest<Result>
    {
        public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, Result>
        {
            private readonly ICustomerRepository customerRepository;

            public UpdateCustomerCommandHandler(
                ICustomerRepository customerRepository)
                    => this.customerRepository = customerRepository;

            public async Task<Result> Handle(
                UpdateCustomerCommand request, 
                CancellationToken cancellationToken)
            {
                var customer = await this.customerRepository
                    .Find(request.Id, cancellationToken);

            customer.UpdatePersonalInformation(
                request.FirstName,
                request.MiddleName,
                request.LastName,
                request.IdentificationNumber);

                await this.customerRepository.Save(customer, cancellationToken);

                return Result.Success;
            }
        }
    }
}
