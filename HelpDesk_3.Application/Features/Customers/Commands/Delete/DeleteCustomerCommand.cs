﻿using HelpDesk_3.Application.Common;
using MediatR;

namespace HelpDesk_3.Application.Features.Customers.Commands.Delete
{
    public class DeleteCustomerCommand : EntityCommand<Guid>, IRequest<Result>
    {
        public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, Result>
        {
            private readonly ICustomerRepository customerRepository;

            public DeleteCustomerCommandHandler(ICustomerRepository customerRepository)
                => this.customerRepository = customerRepository;

            public async Task<Result> Handle(
                DeleteCustomerCommand request,
                CancellationToken cancellationToken)
                    => await this.customerRepository
                       .Delete(request.Id, cancellationToken);
        }
    }
}
