﻿using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Customers.Queries.Search;
using HelpDesk_3.Domain.Models.Customers;

namespace HelpDesk_3.Application.Features.Customers
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Customer> Find(Guid id, CancellationToken cancellationToken);

        Task<bool> Delete(Guid id, CancellationToken cancellationToken);

        Task<CustomerOutputModel> GetCustomerById(
            Guid? id,
            CancellationToken cancellationToken = default);

        Task<CustomerOutputModel> GetCustomerByIdentification(
            string? identificationNumber,
            CancellationToken cancellationToken = default);

        Task<CustomerOutputModel> GetCustomerByRequestId(
            Guid? requestId,
            CancellationToken cancellationToken = default);
    }
}
