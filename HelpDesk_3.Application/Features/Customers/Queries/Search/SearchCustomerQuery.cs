﻿using MediatR;

namespace HelpDesk_3.Application.Features.Customers.Queries.Search
{
    public class SearchCustomerQuery : IRequest<CustomerOutputModel>
    {
        public Guid? Id { get; set; }
        public string? IdentificationNumber { get; set; } = default!;
        public Guid? RequestId { get; set; }

        public class SearchCustomerQueryHandler : IRequestHandler<SearchCustomerQuery, CustomerOutputModel>
        {
            private readonly ICustomerRepository customerRepository;

            public SearchCustomerQueryHandler(ICustomerRepository customerRepository)
                => this.customerRepository = customerRepository;

            public async Task<CustomerOutputModel> Handle(
                SearchCustomerQuery request,
                CancellationToken cancellationToken)
            {
                if (request.Id != null)
                    return await customerRepository
                        .GetCustomerById(
                            request.Id,
                            cancellationToken);

                if (request.IdentificationNumber != null)
                    return await customerRepository
                        .GetCustomerByIdentification(
                            request.IdentificationNumber,
                            cancellationToken);

                if (request.RequestId != null)
                    return await customerRepository
                        .GetCustomerByRequestId(
                            request.RequestId,
                            cancellationToken);

                throw new ArgumentException("Atleast one search requirement must not be null!");
            }
        }
    }
}
