﻿using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Notifications.Queries.Search;
using HelpDesk_3.Domain.Models.Notifications;

namespace HelpDesk_3.Application.Features.Notifications
{
    public interface INotificationRepository : IRepository<Email>
    {
        Task<bool> Delete(Guid id, CancellationToken cancellationToken = default);

        Task<SearchEmailOutputModel> GetEmailById(Guid id, CancellationToken cancellationToken = default);
    }
}
