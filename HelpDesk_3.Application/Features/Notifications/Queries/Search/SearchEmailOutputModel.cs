﻿using AutoMapper;
using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Notifications;

namespace HelpDesk_3.Application.Features.Notifications.Queries.Search
{
    public class SearchEmailOutputModel : IMapFrom<Email>
    {
        public string Name { get; private set; } = default!;

        public string Reciever { get; private set; } = default!;

        public string Description { get; private set; } = default!;

        public DateTime TimeSent { get; private set; }
    }
}
