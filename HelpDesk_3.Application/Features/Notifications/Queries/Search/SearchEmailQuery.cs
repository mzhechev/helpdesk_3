﻿using MediatR;

namespace HelpDesk_3.Application.Features.Notifications.Queries.Search
{
    public class SearchEmailQuery : IRequest<SearchEmailOutputModel>
    {
        public Guid Id { get; set; }

        public class SearchEmailQueryHandler : IRequestHandler<SearchEmailQuery, SearchEmailOutputModel>
        {
            private readonly INotificationRepository notificationRepository;

            public SearchEmailQueryHandler(INotificationRepository notificationRepository)
                => this.notificationRepository = notificationRepository;

            public async Task<SearchEmailOutputModel> Handle(
                SearchEmailQuery request, 
                CancellationToken cancellationToken)
            {
                return await notificationRepository
                    .GetEmailById(request.Id, cancellationToken);
            }
        }
    }
}
