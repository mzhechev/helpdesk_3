﻿using HelpDesk_3.Application.Common;
using MediatR;

namespace HelpDesk_3.Application.Features.Notifications.Commands.Delete
{
    public class DeleteEmailCommand : EntityCommand<Guid>, IRequest<Result>
    {
        public class DeleteEmailCommandHandler : IRequestHandler<DeleteEmailCommand, Result>
        {
            private readonly INotificationRepository notificationRepository;

            public DeleteEmailCommandHandler(INotificationRepository notificationRepository)
                => this.notificationRepository = notificationRepository;

            public async Task<Result> Handle(DeleteEmailCommand request, CancellationToken cancellationToken)
            {
                return await this.notificationRepository
                    .Delete(request.Id, cancellationToken);
            }
        }
    }
}
