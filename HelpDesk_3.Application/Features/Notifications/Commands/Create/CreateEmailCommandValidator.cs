﻿using FluentValidation;
using static HelpDesk_3.Domain.Models.ModelConstants.Email;

namespace HelpDesk_3.Application.Features.Notifications.Commands.Create
{
    public class CreateEmailCommandValidator : AbstractValidator<CreateEmailCommand>
    {
        public CreateEmailCommandValidator()
        {
            this.RuleFor(e => e.Name)
                .MinimumLength(MinEmailNameLength)
                .MaximumLength(MaxEmailNameLength);

            this.RuleFor(e => e.Reciever)
                .MinimumLength(MinRecieverLength)
                .MaximumLength(MaxRecieverLength);

            this.RuleFor(e => e.Description)
                .MinimumLength(MinDescriptionLength)
                .MaximumLength(MaxDescriptionLength);

            this.RuleFor(e => e.TimeSent)
                .Must(z => z.Date >= DateTime.Now);
        }
    }
}
