﻿namespace HelpDesk_3.Application.Features.Notifications.Commands.Create
{
    public class CreateEmailOutputModel
    {
        public CreateEmailOutputModel(Guid emailId)
            => this.EmailId = emailId;

        public Guid EmailId { get; }
    }
}
