﻿using HelpDesk_3.Domain.Factories.Notifications;
using MediatR;

namespace HelpDesk_3.Application.Features.Notifications.Commands.Create
{
    public class CreateEmailCommand : EmailCommand, IRequest<CreateEmailOutputModel>
    {
        public class CreateEmailCommandHandler : IRequestHandler<CreateEmailCommand, CreateEmailOutputModel>
        {
            private readonly INotificationFactory notificationFactory;
            private readonly INotificationRepository notificationRepository;

            public CreateEmailCommandHandler(
                INotificationFactory notificationFactory,
                INotificationRepository notificationRepository)
            {
                this.notificationFactory = notificationFactory;
                this.notificationRepository = notificationRepository;
            
            }

            public async Task<CreateEmailOutputModel> Handle(
                CreateEmailCommand request, 
                CancellationToken cancellationToken)
            {
                var email = this.notificationFactory
                    .WithName(request.Name)
                    .WithReciever(request.Reciever)
                    .WithDescription(request.Description)
                    .WithTimeSent(request.TimeSent)
                    .Build();

                await this.notificationRepository.Save(email, cancellationToken);

                return new CreateEmailOutputModel(email.Id);
            }
        }
    }
}
