﻿using MediatR;

namespace HelpDesk_3.Application.Features.Notifications.Commands.Create
{
    public class EmailCommand
    {
        public string Name { get; set; } = default!;
        public string Reciever { get; set; } = default!;
        public string Description { get; set; } = default!;
        public DateTime TimeSent { get; set; } = default!;
    }
}
