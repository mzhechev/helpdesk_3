﻿namespace HelpDesk_3.Application.Features.Employees.Commands.Common
{
    public class EmployeeCommand<TCommand> : EntityCommand<int>
        where TCommand : EntityCommand<int>
    {
        public string FirstName { get; set; } = default!;
        public string MiddleName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string IdentificationNumber { get; set; } = default!;
    }
}
