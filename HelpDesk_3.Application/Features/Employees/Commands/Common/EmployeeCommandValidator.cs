﻿using FluentValidation;

using static HelpDesk_3.Domain.Models.ModelConstants.Common;

namespace HelpDesk_3.Application.Features.Employees.Commands.Common
{
    public class EmployeeCommandValidator<TCommand> : AbstractValidator<EmployeeCommand<TCommand>>
        where TCommand : EntityCommand<int>
    {
        public EmployeeCommandValidator()
        {
            this.RuleFor(f => f.FirstName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(m => m.MiddleName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(l => l.LastName)
                .MinimumLength(MinNameLength)
                .MaximumLength(MaxNameLength);

            this.RuleFor(i => i.IdentificationNumber);
        }
    }
}
