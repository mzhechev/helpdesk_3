﻿using HelpDesk_3.Application.Common;
using MediatR;

namespace HelpDesk_3.Application.Features.Employees.Commands.Delete
{
    public class DeleteEmployeeCommand : EntityCommand<int>, IRequest<Result>
    {
        public class DeleteEmployeeCommandHandler : IRequestHandler<DeleteEmployeeCommand, Result>
        {
            private readonly IEmployeeRepository employeeRepository;

            public DeleteEmployeeCommandHandler(IEmployeeRepository employeeRepository)
                => this.employeeRepository = employeeRepository;

            public async Task<Result> Handle(
                DeleteEmployeeCommand request, 
                CancellationToken cancellationToken)
                    => await this.employeeRepository
                        .Delete(request.Id, cancellationToken);
        }
    }
}
