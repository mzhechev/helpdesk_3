﻿using FluentValidation;
using HelpDesk_3.Application.Features.Employees.Commands.Common;
using HelpDesk_3.Application.Features.Employees.Commands.Update;

namespace HelpDesk_3.Application.Features.Employees.Commands.Update
{
    public class UpdateEmployeeCommandValidator : AbstractValidator<UpdateEmployeeCommand>
    {
        public UpdateEmployeeCommandValidator()
            => this.Include(new EmployeeCommandValidator<UpdateEmployeeCommand>());
    }
}
