﻿using HelpDesk_3.Application.Common;
using HelpDesk_3.Application.Features.Employees.Commands.Common;
using MediatR;

namespace HelpDesk_3.Application.Features.Employees.Commands.Update
{
    public class UpdateEmployeeCommand : EmployeeCommand<UpdateEmployeeCommand>, IRequest<Result>
    {
        public class UpdateEmployeeCommandHandler : IRequestHandler<UpdateEmployeeCommand, Result>
        {
            private readonly IEmployeeRepository employeeRepository;

            public UpdateEmployeeCommandHandler(
                IEmployeeRepository employeeRepository)
            {
                this.employeeRepository = employeeRepository;
            }

            public async Task<Result> Handle(
                UpdateEmployeeCommand request, 
                CancellationToken cancellationToken)
            {
                var employee = await this.employeeRepository
                    .GetEmployee(request.Id, cancellationToken);

                employee.PersonalInformation
                    .UpdateFirstName(request.FirstName)
                    .UpdateMiddleName(request.MiddleName)
                    .UpdateLastName(request.LastName)
                    .UpdateIdentificationNumber(request.IdentificationNumber);

                await this.employeeRepository.Save(employee, cancellationToken);

                return Result.Success;
            }
        }
    }
}
