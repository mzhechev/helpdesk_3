﻿using MediatR;

namespace HelpDesk_3.Application.Features.Employees.Queries.Search
{
    public class SearchEmployeeQuery : IRequest<EmployeeOutputModel>
    {
        public int? Id { get; set; }
        public string? IdentificationNumber { get; set; } = default!;

        public class SearchEmployeeQueryHandler : IRequestHandler<SearchEmployeeQuery, EmployeeOutputModel>
        {
            private readonly IEmployeeRepository employeeRepository;

            public SearchEmployeeQueryHandler(IEmployeeRepository employeeRepository)
                => this.employeeRepository = employeeRepository;

            public async Task<EmployeeOutputModel> Handle(
                SearchEmployeeQuery request, 
                CancellationToken cancellationToken)
            {
                if (request.Id != null)
                    return await employeeRepository
                        .GetEmployee(
                            request.Id,
                            cancellationToken);

                if (request.IdentificationNumber != null)
                    return await employeeRepository
                        .GetEmployee(
                            request.IdentificationNumber,
                            cancellationToken);

                throw new ArgumentException("Atleast one search requirement must not be null!");
            }
        }
    }
}
