﻿using AutoMapper;
using HelpDesk_3.Application.Mapping;
using HelpDesk_3.Domain.Models.Employees;

namespace HelpDesk_3.Application.Features.Employees.Queries.Search
{
    public class EmployeeOutputModel : IMapFrom<Employee>
    {
        public int Id { get; private set; }

        public string FirstName { get; private set; } = default!;

        public string MiddleName { get; private set; } = default!;

        public string LastName { get; private set; } = default!;

        public string IdentificationNumber { get; private set; } = default!;

        public virtual void Mapping(Profile mapper)
           => mapper
               .CreateMap<Employee, EmployeeOutputModel>()
               .ForMember(f => f.FirstName, cfg => cfg
                   .MapFrom(f => f.PersonalInformation.FirstName))
               .ForMember(m => m.MiddleName, cfg => cfg
                   .MapFrom(m => m.PersonalInformation.MiddleName))
               .ForMember(l => l.LastName, cfg => cfg
                   .MapFrom(l => l.PersonalInformation.LastName))
               .ForMember(i => i.IdentificationNumber, cfg => cfg
                   .MapFrom(i => i.PersonalInformation.IdentificationNumber));
    }
}
