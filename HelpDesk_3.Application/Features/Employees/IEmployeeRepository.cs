﻿using HelpDesk_3.Application.Contracts;
using HelpDesk_3.Application.Features.Employees.Queries.Search;
using HelpDesk_3.Domain.Models.Employees;

namespace HelpDesk_3.Application.Features.Employees
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Task<Employee> GetEmployee(
            int id,
            CancellationToken cancellationToken = default);

        Task<EmployeeOutputModel> GetEmployee(
            int? id,
            CancellationToken cancellationToken = default);

        Task<EmployeeOutputModel> GetEmployee(
            string? identificationNumber,
            CancellationToken cancellationToken = default);

        Task<bool> Delete(
            int id,
            CancellationToken cancellationToken = default);
    }
}
